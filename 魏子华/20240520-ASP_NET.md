### 一、ASP.NET Core简介
ASP.NET Core是一个开源、跨平台的高性能Web框架，用于构建现代、云端优化的Web应用和服务。具有以下特点：
- 跨平台
- 开源
- 高性能
- 模块化和灵活性：采用模块化设计，开发者可以根据需要选择和配置中间件组件，构建高度定制化的请求处理管道- 统一的编程模型
- 依赖注入：内置了依赖注入（DI）框架，简化了应用的配置和管理，提高了代码的可维护性和可测试性
- 现代开发工具
- 云优化
- 兼容性和迁移
### 二、设置开发环境
使用.NETCoreCLI可以创建一个.NETCore应用
~~~js 
-o //用于放置生成的输出的位置。 默认为当前目录。
dotnet new console -o HelloConsole //控制台应用程序
dotnet new webapi -o HelloApi //ASP.NET Core Web API
dotnet new classlib -o HelloClassLibrary //类库
// -o 表示要创建的位置
~~~
除此之外，.NETCoreCLI还有其他命令：

### 三、创建第一个API项目
#### 1.使用vscode
~~~js
// 恢复项目中的NuGet包依赖项：
dotnet restore

// 编译项目：
dotnet build

// 运行项目：
dotnet run

// 测试项目：
dotnet test

// 发布项目：
dotnet publish -c Release -o <output-folder>
-c Release指定发布配置为Release模式，-o指定输出文件夹

// 添加包：
dotnet add package <package-name>

// 添加项目引用：
dotnet add reference <project-path>

// 移除引用：
dotnet remove

// 清理项目：
dotnet clean
~~~

#### 2.使用VisualStudio
1. 选择“ 文件 ”→“ 新建 ”→“ 项目 ”
2. 在弹出的新建项目对话框中选择左侧的“VisualC#”，继续选择“.NET Core”；在右侧选择“ASP.NET Core Web应用程序”，输入新项目名称
3. 选择“API”模板，并且选择“不进行身份验证”