#### 引入 ant-design-vue
```
1.新建项目
2.使用组件
1. 安装
npm i --save ant-design-vue@4.x
2. 注册
全局完整注册
  import { createApp } from 'vue';
  import App from './App';
  // 以下完整引入antdv
  import Antd from 'ant-design-vue';
  import 'ant-design-vue/dist/reset.css';
  
  let app = createApp(App);
  
  app.use(Antd).mount('#app');
  // 样式文件需要单独引入
全局部分注册
  import { createApp } from 'vue';
  import { Button, message } from 'ant-design-vue';
  import App from './App';  

  let app = createApp(App);  

  /* 会自动注册 Button 下的子组件, 例如 Button.Group */
  app.use(Button).mount('#app');  

  app.config.globalProperties.$message = message;
局部组件 需要分别注册组件子组件，如 Button、ButtonGroup，并且注册后仅在当前组件中有效
  // 选项式写法
  <template>
  <a-button>Add</a-button>
  </template>
  <script>
  import { Button } from 'ant-design-vue';
  const ButtonGroup = Button.Group;

  export default {
    components: {
      AButton: Button,
      AButtonGroup: ButtonGroup,
    },
  };
  </script>
  // 组合式写法
  <template>
    <a-button>Add</a-button>
  </template>

  <script setup>
  import { Button } from 'ant-design-vue';
  const { Button: AButton, ButtonGroup: AButtonGroup } = Button;
  </script>
```