创建项目

打开集成终端，写入以下代码

创建项目 dotnet new webapi -o/-n 项目名称 cd 项目名称
启动项目 dotnet run/dotnet watch --project .\项目名称\
打开Program.cs

// 定义一个名为TodoA的命名空间
namespace TodoA
{
    // 定义一个公共静态类，用于应用程序的入口点
    public static class Program
    {
        // 主函数，应用程序的入口点，接收命令行参数
        public static void Main(string[] args)
        {
            // 创建一个HostBuilder实例，用于配置和构建应用程序
            IHostBuilder hostBuilder = CreateHostBuilder(args);

            // 使用Build方法构建和启动应用程序
            hostBuilder.Build().Run();
        }

        // 创建HostBuilder的静态方法，用于配置WebHost的默认设置
        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            // 使用Host类的CreateDefaultBuilder方法创建一个新的Builder实例，传入命令行参数
            return Host.CreateDefaultBuilder(args);

            // 添加WebHost的默认配置，即使用Startup类作为应用程序的启动点
            .ConfigureWebHostDefaults(builder =>
            {
                builder.UseStartup<Startup>();
            });
        }
    }
}
在同层创建一个Startup.cs文件，写入以下代码

// 定义一个名为TodoA的命名空间
namespace TodoA
    // 定义一个公共类，用于应用程序的启动和配置
    public class Startup
    {
        // 配置服务容器的方法，用于注册依赖注入的服务
        public void ConfigureServices(IServiceCollection services)
        {
            // 注册控制器服务
            services.AddControllers();
        }

        // 配置中间件管道的方法，用于设置HTTP请求处理的流程
        public void Configure(IApplicationBuilder app)
        {
            // 使用Routing中间件，用于路由匹配
            app.UseRouting();

            // 使用Endpoints中间件，用于管理终结点
            app.UseEndpoints(endPoints =>
            {
                // 将控制器终结点映射到应用程序中
                endPoints.MapControllers();
            });
        }
    }
在Controller文件夹中添加BooksController.cs文件，写图书的信息，实现crud

using Microsoft.AspNetCore.Mvc; // 引入ASP.NET Core MVC的命名空间，用于控制器和动作方法的创建

namespace TodoA; // 定义命名空间，用于组织代码

[ApiController] // 标记该类为API控制器，启用一些API控制器的特性
[Route("/api/[controller]")] // 定义控制器的路由模板，[controller]将替换为控制器名称

public class BooksController : ControllerBase // 定义一个公共类BooksController，继承自ControllerBase
{
    // 查询操作，通过HTTP GET请求获取特定ID的书籍信息
    [HttpGet("{id}")] // 使用HTTP GET方法，并定义路径参数{id}
    public IActionResult Get(int id, BooksDto booksDto) // 定义Get方法，接收一个整数id和一个BooksDto对象
    {
        return Ok(new { id, booksDto }); // 返回包含id和booksDto的Ok结果
    }

    // 增加操作，通过HTTP POST请求添加新的书籍信息
    [HttpPost] // 使用HTTP POST方法
    public IActionResult Post(BooksDto booksDto) // 定义Post方法，接收一个BooksDto对象
    {
        return Ok(new { booksDto }); // 返回包含booksDto的Ok结果
    }

    // 修改操作，通过HTTP PUT请求更新特定ID的书籍信息
    [HttpPut("{id}")] // 使用HTTP PUT方法，并定义路径参数{id}
    public IActionResult Put(int id, BooksDto booksDto) // 定义Put方法，接收一个整数id和一个BooksDto对象
    {
        return Ok(new { id, booksDto }); // 返回包含id和booksDto的Ok结果
    }

    // 删除操作，通过HTTP DELETE请求删除特定ID的书籍信息
    [HttpDelete("{id}")] // 使用HTTP DELETE方法，并定义路径参数{id}
    public IActionResult Delete(int id, BooksDto booksDto) // 定义Delete方法，接收一个整数id和一个BooksDto对象
    {
        return Ok(); // 返回一个简单的Ok结果，表示操作成功
    }
};