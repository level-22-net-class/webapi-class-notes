1. 入口程序
~~~c#
 using Microsoft.AspNetCore;
 // 1. 命名空间声明
 namespace Api
 {
     // 2. 类声明
     // Program类是应用程序的入口点，应用程序通常从Main方法开始执行
     public class Program
     {
         public static void Main(string[] args)
         {
             CreateWebHostBuilder(args).Build().Run();
             // CreateWebHostBuilder(args)方法被调用来创建一个IWebHostBuilder对象
             // Build()方法被调用在IWebHostBuilder对象上，生成一个IWebHost对象
             // Run()方法启动Web主机，使应用程序开始监听Web请求
         }
         public static IWebHostBuilder CreateWebHostBuilder(string[] args)
         {
             return WebHost.CreateDefaultBuilder(args).UseStartup<Startup>();
             // CreateWebHostBuilder方法用于配置和创建一个Web主机生成器
             // UseStartup<Startup>()方法指定Startup类作为应用程序的启动类
         }
     }
 }
~~~
2. 启动类
~~~c#
 // Startup类是ASP.NET Core应用程序启动时调用的类，用于配置应用程序服务和HTTP请求管道
 public class Startup
 {
     // 用于添加和注册中间件
     public void Configure(IApplicationBuilder app)
     {
         app.UseRouting(); //添加路由中间件
         // 配置终结点路由
         app.UseEndpoints(endpoints => {
             endpoints.MapControllers(); 
             // 添加控制器终结点到请求管道中，这使得控制器能够处理HTTP请求
         }); 
     }
     // 注册准备依赖注入的服务
     // ConfigureServices方法用于配置依赖注入容器，添加应用程序所需的服务
     public void ConfigureServices(IServiceCollection services)
     {
         services.AddControllers(); //添加MVC控制器相关的服务到依赖注入容器中
     }
 }
~~~
3. 控制器
~~~c#
 namespace Api.Controllers;
 
 [Route("[controller]")] //定义控制器的路由前缀，即/blogs
 // BlogsController类继承自ControllerBase，它是一个用于构建API的基类（不包含视图支持）
 public class BlogsController : ControllerBase
 {
     // 处理对/blogs的GET请求
     public IActionResult Index(){
         return Ok("999"); //返回一个包含字符串"999"的200 OK响应
     }
     
     [Route("{id}")] //定义Single方法的路由模板
     // 处理对/blogs/{id}的GET请求,接受一个整型参数id
     public IActionResult Single(int id)
     {
         return Ok(id); //返回一个包含id值的200 OK响应
     }
 }
~~~
