## 一、基础概念
### 1.认证
验证当前用户的身份，互联网中的认证有：
- 用名密码登录
- 邮箱发送登录链接
- 手机号接收验证码等等

### 2.授权
1. 用户授予第三方应用访问该用户某些资源的权限
2. 实现授权的方式有：cookie、session、token、OAuth

### 3.凭证
- 实现认证和授权的前提是需要一种媒介（证书）来标记访问者的身份
- 一般网站（如掘金）会有两种模式，游客模式和登录模式。游客模式下，可以正常浏览网站上面的文章，一旦想要点赞/收藏/分享文章，就需要登录或者注册账号。当用户登录成功后，服务器会给该用户使用的浏览器颁发一个令牌（token），这个令牌用来表明你的身份，每次浏览器发送请求时会带上这个令牌，就可以使用游客模式下无法使用的功能

## 二、Cookie
### 1.了解
#### 1.基本概念
- 存储在客户端：cookie 是服务器发送到用户浏览器并保存在本地的一小块数据，它会在浏览器下次向同一服务器再发起请求时被携带并发送到服务器上。因此，服务端脚本就可以读、写存储在客户端的cookie的值
- 不可跨站：每个 cookie 都绑定在特定的域名下（绑定域名下的子域都是有效的），无法在别的域名下获取使用，同域名不同端口也允许共享

*可以在浏览器控制台的 Application 面板查看Cookie*
*Cookie都是name=value的结构，具体格式如下：*
~~~js
Set-Cookie: "name=value;domain=.domain.com;path=/;expires=Sat, 11 Jun 2016 11:29:42 GMT;HttpOnly;secure"
~~~

#### 2.Cookie的数据流转
1. 在首次访问网站时，浏览器发送请求中并未携带Cookie
2. 浏览器看到请求中未携带Cookie，在HTTP的响应头中加入Set-Cookie
3. 浏览器收到Set-Cookie后，会将Cookie保存下来
4. 下次再访问该网站时，HTTP请求头就会携带Cookie

### 2.检测cookie是否启用
有些用户为了避免隐私泄露会在它们的浏览器中禁用cookie。因此，在js代码使用cookie前，首先要确保cookie是启用的。可以用navigator.cookieEnabled属性来判断，如果值为true，则当前cookie是启用的；反之则是禁用的
~~~js
 if (navigator.cookieEnabled) {
     // 浏览器启用了 Cookie
     console.log("Cookies are enabled in this browser.");
 } else {
     // 浏览器禁用了 Cookie
     console.log("Cookies are not enabled in this browser.");
 }
~~~
==注意：==
1. navigator.cookieEnabled 只能告诉你浏览器的 Cookie 设置，但不能告诉你具体的 Cookie 是否被服务器接受
2. 某些浏览器或浏览器插件可能会篡改 navigator.cookieEnabled 的值，所以在处理敏感信息时，最好仍然验证实际的 Cookie 是否按预期工作

### 3.Cookie属性
#### 有效期和作用域
- 默认的有效期很短暂，它只能维持在Web浏览器的会话期间，一旦用户关闭浏览器，cookie保存的数据就丢失了
- ookie的作用域不是局限在浏览器的单个窗口中，它的有效期和整个浏览器进程而不是单个浏览器窗口的有效期一致。如果想要延长cookie的有效期，可以通过设置max-age属性
#### 常见属性
1. name=valu（键值对）：设置 Cookie 的名称及相对应的值，都必须是字符串类型
2. domain（生效的域名）：即Cookie在哪个网站生效。默认当前访问域名
3. path：希望Cookie仅仅在部分路径下生效，就可以使用Path进行限制。默认的path=/，即在所有路径下生效。 如果设置了path=/abc，则只在/abc路径下生效
4. expires（过期时间）：当浏览器端本地的当前时间超过这个时间时，Cookie便会失效,格式【Expires=Wed, 21 Oct 2015 07:28:00 GMT】
5. Max-age（存活时间）如果为正数，则该 cookie 在 maxAge 秒后失效。如果为负数，该 cookie 为临时 cookie ，关闭浏览器即失效，浏览器也不会以任何形式保存该 cookie 。如果为 0，表示删除该 cookie 。默认为 -1。==优先级高于expire==
6. HttpOnly：设置了 httpOnly 属性，则无法通过js读写该 cookie 的信息，但还是能通过 Application 中手动修改 cookie
7. secure：该cookie 是否仅被使用安全协议传输，默认为false；当 secure 值为 true 时，cookie 在 HTTP 中是无效的
8. SameSite：是否允许跨站请求时发送Cookie
9. Priority：当Cookie的数量超过限制时，路蓝旗会清除一部分Cookie，Priority属性用来定义Cookie的优先级，低优先级的Cookie会优先被清除。优先级有 Low, Medium, High
  
*cookie集合中的每个cookie都拥有这些属性，而且每个cookie的这些属性都是独立分开的，各自控制各自的cookie*

### 4.对Cookie的存取
#### 1.读取Cookie
~~~js
document.cookie;
// "name1=value1; name2=value2"
~~~
#### 2.设置Cookie
~~~js
document.cookie = `name=${encodeURIComponent(name)}; max-age=1000;`;
// name这个cookie会被添加到现有的cookie集合中。
// 由于cookie的键/值中的值是不允许包含分号、逗号和空白符，因此，在存储前一般可以采用 encodeURIComponent() 函数对值进行编码
// 读取cookie值的时候要用 decodeURIComponent() 函数解码
~~~
#### 3.更新Cookie
要改变cookie的值，需要使用相同的名字、路径和域，但是新的值重新设置cookie的值
#### 4.删除Cookie
要删除一个cookie，需要使用相同的名字、路径和域，然后指定一个任意（非空）的值，并且将 max-age 属性指定为0，再次设置cookie

*其他详细见：https://blog.csdn.net/huangpb123/article/details/109107461*

## 三、Session
### 1.了解
会话用于跟踪用户在多个页面请求期间的状态。它们通常存储在服务器端，并且与唯一的会话标识符（通常是会话ID）相关联，会话ID作为Cookie发送给客户端。会话允许服务器在用户访问期间记住有关用户的信息
![Session-2024-7-910:11:38.jpeg](https://gitee.com/huangxuefang0929/xiu_img/raw/master/Session-2024-7-910:11:38.jpeg)
### 2.session认证流程
1. 用户第一次请求服务器的时候，服务器根据用户提交的相关信息，创建对应的 Session
2. 请求返回时将此 Session 的唯一标识 SessionID 返回给浏览器
3. 浏览器接收到服务器返回的 SessionID 后，会将此信息存入到 Cookie 中，同时 Cookie 记录此 SessionID 属于哪个域名
4. 当用户第二次访问服务器的时候，请求会自动把此域名下的 Cookie 信息也发送给服务端，服务端会从 Cookie 中获取 SessionID，再根据 SessionID 查找对应的 Session 信息，如果没有找到说明用户没有登录或者登录失效，如果找到 Session 证明用户已经登录可执行后面操作
### 3.Cookie 和 Session 的区别
- 安全性： Session 比 Cookie 安全，Session 是存储在服务器端的，Cookie 是存储在客户端的
- 存取值的类型不同：Cookie 只支持存字符串数据，Session 可以存任意数据类型
- 有效期不同： Cookie 可设置为长时间保持，比如我们经常使用的默认登录功能，Session 一般失效时间较短，客户端关闭（默认情况下）或者 Session 超时都会失效
- 存储大小不同：单个 Cookie 保存的数据不能超过 4K，Session 可存储数据远高于 Cookie，但是当访问量过多，会占用过多的服务器资源

## 四、Token
### 1.了解
Token 是访问接口（API）时所需要的资源凭证
### 2.简单的token组成
1. uid：用户唯一的身份标识
2. time：当前时间的时间戳
3. sign：签名，token 的前几位以哈希算法压缩成的一定长度的十六进制字符串
### 3.Access Token
AccessToken身份验证流程如下：
![AccessToken-2024-7-910:16:23.jpeg](https://gitee.com/huangxuefang0929/xiu_img/raw/master/AccessToken-2024-7-910:16:23.jpeg)

1. 客户端使用用户名跟密码请求登录
2. 服务端收到请求，去验证用户名与密码
3. 验证成功后，服务端会签发一个 token 并把这个 token 发送给客户端
4. 客户端收到 token 以后，会把它存储起来，比如放在 localStorage 里
5. 户端每次发起请求的时候需要把 token 放到请求的 Header 里传给服务端
6. 服务端收到请求，然后去验证客户端请求里面带着的 token ，如果验证成功，就向客户端返回请求的数据
   
### 4.Refresh Token
- 专用于刷新 access token 的 token。如果没有 refresh token，也可以刷新 access token，但每次刷新都要用户输入登录用户名与密码，会很麻烦。有了 refresh token，可以减少这个麻烦，客户端直接用 refresh token 去更新 access token，无需用户进行额外的操作。
- Access Token 的有效期比较短，当 Acesss Token 由于过期而失效时，使用 Refresh Token 就可以获取到新的 Token，如果 Refresh Token 也失效了，用户就只能重新登录了。
- Refresh Token 及过期时间是存储在服务器的数据库中，只有在申请新的 Acesss Token 时才会验证，不会对业务接口响应时间造成影响，也不需要向 Session 一样一直保持在内存中以应对大量的请求

### 5.Toke和Session的区别
- Session 是一种记录服务器和客户端会话状态的机制，使服务端有状态化，可以记录会话信息。而 Token 是令牌，访问资源接口（API）时所需要的资源凭证。Token 使服务端无状态化，不会存储会话信息
- Session 和 Token 并不矛盾，作为身份认证 Token 安全性比 Session 好，因为每一个请求都有签名还能防止监听以及重复攻击，而 Session 就必须依赖链路层来保障通讯安全了。如果你需要实现有状态的会话，仍然可以增加 Session 来在服务器端保存一些状态
- 如果你的用户数据可能需要和第三方共享，或者允许第三方调用 API 接口，用 Token 。如果永远只是自己的网站，自己的 App，用什么就无所谓了
  

## 五、JWT
### 1.了解
- JSON Web Token（简称 JWT）是目前最流行的跨域认证解决方案
- JWT 的声明一般被用来在身份提供者和服务提供者间传递被认证的用户身份信息，以便于从资源服务器获取资源。比如用在用户登录上

### 2.原理
服务器认证以后，生成一个 JSON 对象，返回给用户，就像下面这样
~~~js
{
  "姓名": "张三",
  "角色": "管理员",
  "到期时间": "2018年7月1日0点0分"
}
~~~
服务器完全只靠这个对象认定用户身份。为了防止用户篡改数据，服务器在生成这个对象的时候，会加上签名

### 3.JWT认证流程
![JWT-2024-7-910:25:30.jpeg](https://gitee.com/huangxuefang0929/xiu_img/raw/master/JWT-2024-7-910:25:30.jpeg)

*当用户希望访问一个受保护的路由或者资源的时候，需要请求头的 Authorization 字段中使用Bearer 模式添加 JWT，其内容看起来是下面这样*
~~~js
Authorization: Bearer <token>
~~~
- 服务端的保护路由将会检查请求头 Authorization 中的 JWT 信息，如果合法，则允许用户的行为
- 因为 JWT 是自包含的（内部包含了一些会话信息），因此减少了需要查询数据库的需要
- 因为 JWT 并不使用 Cookie 的，所以你可以使用任何域名提供你的 API 服务而不因为用户的状态不再存储在服务端的内存中，所以这是一种无状态的认证机制需要担心跨域问题

### 4.数据结构

- Header头部信息：
  - Json对象，描述JWT的元数据，通常是以下样子
    ~~~js
     {
       "alg": "HS256", // 签名的默认算法
       "typ": "JWT" // token的类型
     }
    ~~~ 
- Payload负载：
  - Json对象，用来存放实际需要传递的数据
  - JWT 默认是不加密的，任何人都可以读到，所以不要把秘密信息放在这个部分
  - 七个官方字段如下：
    ~~~js
     iss(issuer)：签发人
     exp(expiration time)：过期时间
     aud (audience)：受众
     nbf (Not Before)：生效时间
     iat (Issued At)：签发时间
     jti (JWT ID)：编号

     <!-- 此外还可以定义私有字段 -->
     {
       "sub": "1234567890",
       "name": "John Doe",
       "admin": true
     }
    ~~~ 
- Signature签名：
  - 是对前两部分的签名，防止数据篡改
  - 可以按照下面的公式产生签名
    ~~~js
     HMACSHA256(
      base64UrlEncode(header) + "." +
      base64UrlEncode(payload),
      secret)
    ~~~ 

### 5.Token和JWT的区别
**相同：**
1. 都是访问资源的令牌
2. 都可以记录用户的信息
3. 都是使服务端无状态化
4. 都是只有验证成功后，客户端才能访问服务端上受保护的资源

**区别：**
1. Token：服务端验证客户端发送过来的 Token 时，还需要查询数据库获取用户信息，然后验证 Token 是否有效
2. JWT： 将 Token 和 Payload 加密后存储于客户端，服务端只需要使用密钥解密进行校验（校验也是 JWT 自己实现的）即可，不需要查询或者减少查询数据库，因为 JWT 自包含了用户信息和加密的数据


## 六、四者的区别
![cstj四者的区别-2024-7-914:00:32.png](https://gitee.com/huangxuefang0929/xiu_img/raw/master/cstj%E5%9B%9B%E8%80%85%E7%9A%84%E5%8C%BA%E5%88%AB-2024-7-914:00:32.png)

## 七、四者的优缺点
![cstj四者的优缺点-2024-7-914:00:22.png](https://gitee.com/huangxuefang0929/xiu_img/raw/master/cstj%E5%9B%9B%E8%80%85%E7%9A%84%E4%BC%98%E7%BC%BA%E7%82%B9-2024-7-914:00:22.png)