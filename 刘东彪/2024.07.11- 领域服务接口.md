# 领域服务接口
using Admin2024.Domain.Entity.System;
using Admin2024.Domain.ObjectValue;

namespace Admin2024.Domain.DomainService.System;

// 资源访问控制服务
public interface IResourceAccessControlService
{
    Task<DomainResult<string>> CanAccessResource(Guid appUserId, Guid appResourceId);

}
using Admin2024.Domain.Entity.System;
using Admin2024.Domain.ObjectValue;

namespace Admin2024.Domain.DomainService.System;

// 用户认证服务接口
public interface IAuthenticationService
{
    Task<DomainResult<string>> Authenticate(string username, string password);

    Task<DomainResult<string>> RefreshToken(string token);
}
using Admin2024.Domain.Entity.System;
using Admin2024.Domain.ObjectValue;

namespace Admin2024.Domain.DomainService.System;

// 用户认证服务接口
public interface IAuthenticationService
{
    Task<DomainResult<string>> Authenticate(string username, string password);

    Task<DomainResult<string>> RefreshToken(string token);
}
using Admin2024.Domain.Entity.System;
using Admin2024.Domain.ObjectValue;

namespace Admin2024.Domain.DomainService.System;

// 用户认证服务接口
public interface IAuthorizationService
{
    Task<DomainResult<string>> HasRole(Guid appUserId, string roleName);

    Task<DomainResult<string>> HasPermission(Guid appUserId,string permission);
}
using Admin2024.Domain.Entity.System;
using Admin2024.Domain.ObjectValue;

namespace Admin2024.Domain.DomainService.System;

public interface IPasswordManagementService
{
    Task<DomainResult<string>> ChangePassword(Guid appUserId, string password);

    Task<DomainResult<string>> ResetPassword(Guid appUserId,string newPassword);
}

using Admin2024.Domain.Entity.System;
using Admin2024.Domain.ObjectValue;

namespace Admin2024.Domain.DomainService.System;

public interface IPermissionManagementService
{
    Task<DomainResult<string>> CreatePermission(Guid username, string password);

    Task<DomainResult<string>> DeletePermission(string token);
}
using Admin2024.Domain.Entity.System;
using Admin2024.Domain.ObjectValue;

namespace Admin2024.Domain.DomainService.System;

// 角色管理服务接口
public interface IRoleManagementService
{
    Task<DomainResult<AppRole>> CreateRole(string roleName);

    Task<DomainResult<AppRole>> DeleteRole(string token);
    Task<DomainResult<AppRole>> UpdateRoleName(Guid appRoleId, string roleName);
    Task<DomainResult<AppRole>> AssignPermissionToRole(Guid appRoleId, Guid appPermissionId);
    Task<DomainResult<AppRole>> RemovePermissionFromRole(Guid appRoleId, Guid appPermissionId);
}
using Admin2024.Domain.Entity.System;
using Admin2024.Domain.ObjectValue;

namespace Admin2024.Domain.DomainService.System;

public interface IUserInformationManagementService
{
    Task<DomainResult<string>> UpdateUserInfo(Guid appUserId, string password);

}
using Admin2024.Domain.Entity.System;
using Admin2024.Domain.ObjectValue;

namespace Admin2024.Domain.DomainService.System;

public interface IUserRoleManagementService
{
    Task<DomainResult<string>> AssignRoleToUser(Guid appUserId, Guid appRoleId);

    Task<DomainResult<string>> RemoveRoleFromUser(Guid appUserId, Guid appRoleId);
}