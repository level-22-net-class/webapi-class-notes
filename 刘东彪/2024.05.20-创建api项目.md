## 创建首个Api

1. 安装SDK

2. Web的命令：

- 新建一个WebApi项目：dotnet new webapi -n `命名`
- 新建一个解决方案：dotnet new sln -n `命名`
- 新建一个WebApi项目(--no -https参数表示不启用https)：dotnet new webapi --no -https -o dream.WebApi
- 创建类库：dotnet new classlib -o `命名（dream.Entity）`
- 运行(命令当中 “-p” 是指定要运行的项目)：dotnet run --project Dream.WebApi/Dream.WebApi.csproj -->
3. 运行这个项目
- dotnet run

4. 在WeatherForecast.js中复制类名至导航栏，可看到数据

> 导航栏中localhost:端口/`swagger`可进入api的页面(CRUD)