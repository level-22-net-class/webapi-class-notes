BaseEntity.cs:
```c#
namespace Admin2024.Domain.Entity;

public abstract class BaseEntity
{
    //主键Id
    public Guid Id { get; set; }
    //是否启用/激活
    public bool IsActived { get; set; }
    //是否删除
    public bool IsDeleted { get; set; }
    //创建时间
    public DateTime CreatedAt { get; set; }
    //创建人
    public Guid CreatedBy { get; set; }
    //最后更新时间
    public DateTime UpdatedAt { get; set; }
    //最后更新人
    public Guid UpdatedBy { get; set; }
    //显示顺序，数字越大越靠前
    public int DisplayOrder { get; set; }
    //备注
    public string? Remarks { get; set; }
}
```


AppUser.cs:
```c#
namespace Admin2024.Domain.Entity.System
{
    /// <summary>
    /// 应用用户实体类，继承自基础实体类BaseEntity，代表系统中的一个用户。
    /// </summary>
    public class AppUser : BaseEntity
    {
        // 用户的角色集合，用于存储用户所拥有的角色。
        private List<AppRole> _appRoles = new List<AppRole>();

        // 用户名，用于唯一标识一个用户。
        public string Username { get; set; } = null!;

        // 用户密码，用于验证用户身份。
        public string Password { get; set; } = null!;

        // 随机加密字符串，用于密码加密。
        public string Salt { get; set; } = null!;

        // 用户昵称，用于显示给其他用户看到的名称。
        public string Nickname { get; set; } = null!;

        // 用户头像地址，用于显示用户头像。
        public string Avatar { get; set; } = null!;

        // 用户手机号码，用于用户身份验证和联系用户。
        public string Telephone { get; set; } = null!;

        // 用户微信号，用于绑定微信登录。
        public string Weixin { get; set; } = null!;

        // 用户QQ号，用于绑定QQ登录。
        public string QQ { get; set; } = null!;

        // 设置用户密码
        public void SetPassword(string password)
        {
            this.Password = password;
        }

        // 为用户分配角色。
        //"appRole"待分配的角色
        public void AllocateRole(AppRole appRole)
        {
            _appRoles.Add(appRole);
        }

        // 移除用户的一个角色
        //"appRole"待移除的角色
        public void RemoveRole(AppRole appRole)
        {
            _appRoles.Remove(appRole);
        }

        // 通过角色名移除用户的一个角色
        //"roleName"待移除的角色名
        public void RemoveRole(string roleName)
        {
            var role = _appRoles.FirstOrDefault(x => x.RoleName == roleName);
            if (role != null)
            {
                _appRoles.Remove(role);
            }
        }

        // 检查用户是否具有权限
        //始终返回true，表示具有权限。实际应用中应根据角色权限进行具体实现
        public bool HasPermssion()
        {
            return true;
        }
    }
}
```

AppRole.cs:
```c#
namespace Admin2024.Domain.Entity.System
{
    /// <summary>
    /// 应用角色实体类，继承自基础实体类BaseEntity，定义系统中的角色及其权限。
    /// </summary>
    public class AppRole : BaseEntity
    {
        // 角色关联的权限列表。
        private List<AppPermission> _appPermission = new List<AppPermission>();

        // 角色名称，用于标识角色的唯一名称。
        public string RoleName { get; set; } = null!;

        // 角色描述，对角色的简短说明。
        public string? Description { get; set; }

        /// <summary>
        /// 角色构造函数，无参构造。
        /// </summary>
        public AppRole()
        {
        }

        /// <summary>
        /// 角色构造函数，带角色名和描述的构造。
        /// </summary>
        /// <param name="roleName">角色名称。</param>
        /// <param name="description">角色描述。</param>
        public AppRole(string roleName, string description)
        {
            RoleName = roleName;
            Description = description;
        }

        /// <summary>
        /// 分配权限给角色。
        /// </summary>
        /// <param name="appPermission">待分配的权限实例。</param>
        public void AllocatePermission(AppPermission appPermission)
        {
            _appPermission.Add(appPermission);
        }

        /// <summary>
        /// 从角色中移除权限。
        /// </summary>
        /// <param name="appPermission">待移除的权限实例。</param>
        public void RemovePermission(AppPermission appPermission)
        {
            _appPermission.Remove(appPermission);
        }

        /// <summary>
        /// 判断角色是否拥有指定资源和操作的权限。
        /// </summary>
        /// <param name="appResource">资源实例。</param>
        /// <param name="appOperation">操作实例。</param>
        /// <returns>如果拥有权限则返回true，否则返回false。</returns>
        public bool HasPermission(AppResource appResource, AppOperation appOperation)
        {
            return _appPermission.Any(x => x.AppResourceId == appResource.Id
            && x.AppOperationId == appOperation.Id);
        }

        /// <summary>
        /// 根据资源ID和操作ID判断角色是否拥有指定权限。
        /// </summary>
        /// <param name="resourceId">资源ID。</param>
        /// <param name="operationId">操作ID。</param>
        /// <returns>如果拥有权限则返回true，否则返回false。</returns>
        public bool HasPermission(Guid resourceId, Guid operationId)
        {
            return _appPermission.Any(x => x.AppResourceId == resourceId
            && x.AppOperationId == operationId);
        }
    }
}
```

AppUserRole.cs:
```c#
namespace Admin2024.Domain.Entity.System;

public class AppUserRole : BaseEntity
{
    public Guid AppUserId { get; set; }
    public Guid AppRoleId { get; set; }
}
```

AppPermission.cs:
```c#
namespace Admin2024.Domain.Entity.System
{
    /// <summary>
    /// 应用权限基类，继承自 <see cref="BaseEntity"/>，包含了应用资源标识和操作标识。
    /// </summary>
    public class AppPermission : BaseEntity
    {
        // 获取或设置应用资源的唯一标识。
        public Guid AppResourceId { get; set; }

        // 获取或设置应用操作的唯一标识。
        public Guid AppOperationId { get; set; }

        // 初始化一个 <see cref="AppPermission"/> 实例。
        public AppPermission()
        {
            // 默认构造函数，通常用于数据库映射或其他框架初始化。
        }

        /// <summary>
        /// 初始化一个 <see cref="AppPermission"/> 实例，指定应用资源标识和操作标识。
        /// </summary>
        /// <param name="appResourceId">应用资源的唯一标识。</param>
        /// <param name="appOperationId">应用操作的唯一标识。</param>
        public AppPermission(Guid appResourceId, Guid appOperationId)
        {
            AppResourceId = appResourceId;
            AppOperationId = appOperationId;
        }
    }
}
```

AppRolePermission.cs:
```c#
namespace Admin2024.Domain.Entity.System
{

    // "AppRolePermission" 类表示应用角色与权限之间的关联信息
    public class AppRolePermission : BaseEntity
    {
        /// <summary>
        /// 获取或设置应用角色的唯一标识。
        /// </summary>
        public Guid AppRoleId { get; set; }

        /// <summary>
        /// 获取或设置应用权限的唯一标识。
        /// </summary>
        public Guid AppPermissionId { get; set; }
    }
}
```

AppOperation.cs:
```c#
namespace Admin2024.Domain.Entity.System
{
    /// <summary>
    /// <see cref="AppOperation"/> 类表示应用程序中的一个操作项，
    /// 继承自 <see cref="BaseEntity"/>，包含操作名称和描述信息。
    /// </summary>
    public class AppOperation : BaseEntity
    {
        // 获取或设置操作的名称
        public string OperationName { get; set; } = null!;

        /// 获取或设置操作的描述信息
        public string? Descript { get; set; }
    }
}
```

AppResource.cs:
```c#
namespace Admin2024.Domain.Entity.System;

public class AppResource : BaseEntity
{
    //资源名称
    public string ResourceName { get; set; } = null!;
    //资源的访问URL
    public string Url { get; set; } = null!;
}
```

IRepository.cs:
```c#
using Admin2024.Domain.Entity;

namespace Admin2024.Domain.Interface;

/// <summary>
/// 定义了对实体类型T进行基本CRUD操作的接口。
/// T必须继承自BaseEntity以确保具有基础的实体属性与行为。
/// </summary>
/// <typeparam name="T">实体类型，继承自BaseEntity。</typeparam>
public interface IRepository<T> where T : BaseEntity
{
    // 异步根据ID获取单个实体。
    Task<T> GetByIdAsync(Guid id);

    // 异步获取所有实体的列表。
    Task<List<T>> GetListAsync();

    // 异步创建一个新的实体。
    // 返回已创建并可能包含新分配ID的实体对象。
    Task<T> CreateAsync(T entity);

    // 异步更新指定ID的实体。
    // 返回已更新的实体对象，如果更新成功。
    Task<T> UpdateAsync(Guid id, T entity);

    // 异步根据ID删除实体。
    // 如果删除成功则返回被删除的实体，否则返回null。
    Task<T?> DeleteAsync(Guid id);

    // 异步根据实体实例删除实体。
    // 如果删除成功则返回被删除的实体，否则返回null。
    Task<T?> DeleteAsync(T entity);
}
```