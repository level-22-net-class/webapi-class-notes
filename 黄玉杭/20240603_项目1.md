

```csharp
public class ProductEditDto
{
    public string Name { get; set; }
    public decimal Price { get; set; }
}
```

然后，在控制器中使用DTO来接收和验证编辑请求：

```csharp
[HttpPut("{id}")]
public async Task<IActionResult> UpdateProduct(int id, ProductEditDto productDto)
{
    var existingProduct = await _context.Products.FindAsync(id);

    if (existingProduct == null)
    {
        return NotFound();
    }

    existingProduct.Name = productDto.Name;
    existingProduct.Price = productDto.Price;

    _context.Products.Update(existingProduct);
    await _context.SaveChangesAsync();

    return NoContent();
}
```

