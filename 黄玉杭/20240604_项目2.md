
```csharp
[HttpDelete("{id}")]
public async Task<IActionResult> DeleteProduct(int id)
{
    var existingProduct = await _context.Products.FindAsync(id);

    if (existingProduct == null)
    {
        return NotFound();
    }

    _context.Products.Remove(existingProduct);
    await _context.SaveChangesAsync();

    return NoContent();
}
```
