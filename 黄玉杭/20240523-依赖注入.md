依赖注入（Dependency Injection，简称DI）是一种设计模式，用于解耦组件之间的依赖关系。在ASP.NET中，依赖注入是一种常见的实践，用于将依赖项提供给应用程序的各个组件。

使用依赖注入


```csharp
public void ConfigureServices(IServiceCollection services)
{
    // 注册依赖项
    services.AddScoped<IMyService, MyService>();
    services.AddSingleton<ILogger, Logger>();
    // ...
}
```

```csharp
public class MyController : ControllerBase
{
    private readonly IMyService _myService;

    public MyController(IMyService myService)
    {
        _myService = myService;
    }

    // ...
}
```

```csharp
public class MyController : ControllerBase
{
    private readonly IMyService _myService;

    public MyController(IMyService myService)
    {
        _myService = myService;
    }

    public IActionResult Index()
    {
        var data = _myService.GetData();
        // 处理数据并返回结果
        return Ok(data);
    }
}
```

