AutoMapper是一个用于.NET应用程序的对象-对象映射库，它可以帮助简化不同类型对象之间的映射过程。以下是AutoMapper的基本使用步骤：

1. **安装**：使用`dotnet add`或NuGet包管理器安装AutoMapper。

2. **配置**：定义一个继承自`Profile`的配置类，在构造函数中配置对象之间的映射关系。例如：
   ```cs
   public class EmployeeProfile : Profile
   {
       public EmployeeProfile()
       {
           CreateMap<Employee, EmployeeDto>();
       }
   }

   var config = new MapperConfiguration(cfg =>
   {
       cfg.AddProfile<EmployeeProfile>();
   });
   ```

3. **注册**：将AutoMapper服务注册到应用程序的依赖注入容器中。

4. **注入**：在控制器或其他服务中，通过构造函数注入`IMapper`接口，以便在代码中使用AutoMapper进行对象映射。

Profile类是一种组织映射关系的方式，配置在Profile内部的映射仅适用于该Profile内部。而应用于根配置的映射配置适用于所有创建的映射。