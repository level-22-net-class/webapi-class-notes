```cs blogController.cs
using Microsoft.AspNetCore.Mvc;
using BlogApi.Domain;
using BlogApi.interfaces;

[Route("api/[controller]")]
[ApiController]
public class BlogController : ControllerBase
{
    private readonly IRepository<Blogs> _blogRepository;
    public BlogController(IRepository<Blogs> blogRepository)
    {
        _blogRepository = blogRepository;
    }
    [HttpGet]
    public IActionResult Get(string? keyword)
    {
        Console.Write("钥匙在这里" + keyword);
        var blogs = _blogRepository.GetList(keyword);
        return Ok(blogs);
    }
    [HttpGet("{id}")]
    public IActionResult GetById(int id)
    {
        var blog = _blogRepository.GetById(id);
        if(blog == null)
        {
            return NotFound();
        }
        return Ok(blog);
    }
    [HttpPost]
    public IActionResult Post(Blogs blog)
    {
        _blogRepository.Add(blog);
        return Ok(new{
            id=blog.Id,
            data=blog
        });
    }
    [HttpPut("{id}")]
    public IActionResult Put(int id, Blogs blog)
    {
        if(id != blog.Id)
        {
            return BadRequest();
        }
        _blogRepository.Update(blog);
        return Ok("修改成功");
    }
    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        var blog = _blogRepository.GetById(id);
        if(blog == null)
        {
            return NotFound();
        }
        _blogRepository.Delete(blog);
        return Ok("删除成功");
    }
}
```