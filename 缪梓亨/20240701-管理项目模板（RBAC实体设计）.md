## 项目构建及说明

### *.Api 向外提供接口服务的层
 - dotnet new webapi -n Admin2024.Api

### *.Application  应用层/服务层
 - dotnet new classlib -n Admin2024.Application
领域层金额应用层不知道项目中使用的ORM和Database Provider。只依赖于仓储接口
 - dotnet new classlib -n Admin2024.Application.Contracts

### *.Application.Shared  定义应用接口及Dto
 - dotnet new classlib -n Admin2024.Application.Shared

### *.Domain  领域层 核心层
 - dotnet new classlib -n Admin2024.Domain

### *.EntityFrameworkCore ORM工具之一
 - dotnet new classlib -n Admin2024.EntityFrameworkCore

### *.Infrastructure  基础设施层
 -  dotnet new classlib -n Admin2024.Infrastructure

## 解决方案
 -  dotnet new sln -n Admin2024
### 将项目添加到解决方案
 - dotnet sln add .\Admin2024.Api\ .\Admin2024.Application\

## RBAC实体设计
```


namespace Admin2024.Domain.Entity;

public abstract class BaseEntity
{
    public Guid Id { get; set; }//主键id
    public bool IsActived { get; set; }//是否启用/激活
    public bool IsDeleted { get; set; }//是否删除
    public DateTime CreatedAt { get; set; }//创建时间
    public Guid CreatedBy { get; set; }//创建人
    public DateTime UpdatedAt { get; set; }//最后更新时间
    public Guid UpdatedBy { get; set; }//最后更新人
    public int DisplayOrder { get; set; }//显示顺序，数字越大越靠前
    public string? Remarks { get; set; }//备注
}
```

```
namespace Admin2024.Domain.Entity.System;

public class AppUser  : BaseEntity
{
    public string UserName { get; set;}=null!; //用户名
    public string Password { get; set;}=null!; //密码
    public string Salt { get; set;}=null!; //盐//随机加密字符串
    public string Nickname { get; set;}=null!;  //昵称

    public string Auatar { get; set;}=null!;  //头像
    public string Telephone { get; set;}=null!;  //手机号
    public string Weixin { get; set;}=null!;  //微信号
    public string QQ { get; set;}=null!;  //QQ号
}

```
```
namespace Admin2024.Domain.Entity.System;

public class AppRole : BaseEntity
{
    public string RoleName { get; set;}=null!;
    public string? Descript{ get; set;}

}
```
```
namespace Admin2024.Domain.Entity.System;

public class AppPermission : BaseEntity
{
    public Guid AppResourceId { get; set;}
    public Guid  AppOperationId { get; set;}
    //public virtual AppResource? AppResource{ get; set;}
   // public virtual AppResource? AppOperation{ get; set;}
    
}
```
```
namespace Admin2024.Domain.Entity.System;

public class AppOperation : BaseEntity
{
    //操作名称，比如有读取（查找、过滤）、新增、修改、导入、导出、打印、放大、最大化
    public string OperationName { get; set;}=null!;
    public Guid ? Descript { get; set;}
    
}
```
```
namespace Admin2024.Domain.Entity.System;

public class AppResource : BaseEntity
{
    public string ResourceName { get; set;}=null!;
    public string Url { get; set;}=null!;
    
}

```

```
namespace Admin2024.Domain.Entity.System;

public class AppUserRole : BaseEntity
{
    public Guid AppUserId { get; set; }
    public Guid AppRoleId { get; set;}
}
```