## 用户实体、服务层接口及Dto、实现
```cs
namespace Admin2024.Application.Contracts.AppUser;

public interface IAppUserAppService
{
    Task<AppUserDto?> Registry(CreateAppUserDto createAppUserDto);

    void Login(string username, string password);

    void Logout(string token);

    void DisableUser();

    void Delete();

    void Update(UpdateAppUserDto updateAppUserDto);

    void ModifyPassword(string password);

    void AllocateRoleToUser();


}
```
```cs
namespace Admin2024.Application.Contracts.AppUser;

public class AppUserDto
{
    public Guid Id{get;set;}
    public string Username{get;set;}=null!;
    public string Password{get;set;}=null!;
}
```
```cs
namespace Admin2024.Application.Contracts.AppUser;

public class CreateAppUserDto
{
    public string Username { get; set; } = null!;
    public string Password { get; set; } = null!;
    public string ConfirmPassword { get; set; } = null!;
}
```
```cs
namespace Admin2024.Application.Contracts.AppUser;

public class UpdateAppUserDto
{
    
}
```