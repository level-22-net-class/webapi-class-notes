### 一、解决方案和项目之间的关系
1. 编译或打包解决方案时，会同时编译或打包其下所有项目
2. 如果运行解决方案，会按编排的（指定的启动项目），分别编译该项目
3. 在根目录下编译项目会一起编译解决方案，而在项目目录下编译却只会编译该项目

### 二、在项目中引用另一个项目
~~~js
dotnet add.\MyApi\ reference .\ApiDemo\
// 在MyApi项目中引用ApiDemo项目
// 则在MyApi.csproj文件中会出现以下代码
<ItemGroup>
    <ProjectReference Include="..\ApiDemo\ApiDemo.csproj" />
</ItemGroup>
~~~

### 三、添加依赖包
~~~js
dotnet add .\MyApi\ package package_name
//  则在MyApi.csproj文件中会出现以下代码：
 <PackageReference Include="****" Version="****" />

// 也可以根据MyApi.csproj文件还原包
dotnet restore
~~~

### 四、编译一个简单的api请求
1. 编译代码
~~~js
// 在program.cs中
// 使用了 MapGet 方法来指定了一个 GET 请求的路由路径为 "/api/users"
app.MapGet("/api/users",()=>{
    // 创建了一个 List<dynamic> 类型的列表对象，用于存储动态类型的元素
    var list = new List<dynamic>();
    list.Add(new{
        Id=1,
        Username="猪猪侠",
        Password="123",
        Skill="变身"
    });
    list.Add(new{
        Id=2,
        Username="小呆呆",
        Password="123",
        Skill="流鼻涕"
    });
    return list;
});
~~~
2. 启动项目
~~~js
// 在根目录启动命令：
dotnet run --project .\MyApi\
// 也可以直接在项目目录启动：
dotnet run project
// 监控修改（热重载）：无需每次修改后do重新运行
dotnet watch project
// 清理缓存
dotnet clea
~~~
3. 在api.http中测试api
![api测试-2024-5-2217:44:38.png](https://gitee.com/huangxuefang0929/xiu_img/raw/master/api%E6%B5%8B%E8%AF%95-2024-5-2217:44:38.png)

### 五、中间件
1. 添加中间件
~~~js
// 接受 HttpContext 对象 ctx 和一个 next 委托作为参数
app.Use(async (ctx,next)=>{
    // 创建变量forecast，其中存储包含1到10的整数序列
    // Enumerable.Range 方法用于生成指定范围内的整数序列
    var forecast = Enumerable.Range(1,10);
    // 调用下一个中间件或终结点，使得控制权传递给下一个中间件或请求处理终结点
    await next();
    // 调用下一个中间件或终结点，使得控制权传递给下一个中间件或请求处理终结点
    ctx.Response.WriteAsync("C929星球");
});
~~~
2. 效果如下：
![添加中间件-2024-5-2217:44:48.png](https://gitee.com/huangxuefang0929/xiu_img/raw/master/%E6%B7%BB%E5%8A%A0%E4%B8%AD%E9%97%B4%E4%BB%B6-2024-5-2217:44:48.png)

### 六、编写一个简单的api（完整）
代码如下：
![简单的api-2024-5-2217:52:36.png](https://gitee.com/huangxuefang0929/xiu_img/raw/master/%E7%AE%80%E5%8D%95%E7%9A%84api-2024-5-2217:52:36.png)
修改端口号：Properties/launchsetting.json
![修改端口号-2024-5-2217:44:56.png](https://gitee.com/huangxuefang0929/xiu_img/raw/master/%E4%BF%AE%E6%94%B9%E7%AB%AF%E5%8F%A3%E5%8F%B7-2024-5-2217:44:56.png)

# 

### 练习
#### 一、在最简单的形式下，转换为在单独的文件中写路径-抽离
路由部分代码：
![router-2024-5-2322:17:54.png](https://gitee.com/huangxuefang0929/xiu_img/raw/master/router-2024-5-2322:17:54.png)

#### 二、在最简形式下，转换为原来传统的控制器的模式-转换
控制器部分代码：
![控制器部分-2024-5-2217:52:53.png](https://gitee.com/huangxuefang0929/xiu_img/raw/master/%E6%8E%A7%E5%88%B6%E5%99%A8%E9%83%A8%E5%88%86-2024-5-2217:52:53.png)
最终效果：
![cb186568df1d51e3de82e02105da460-2024-5-2217:44:13.png](https://gitee.com/huangxuefang0929/xiu_img/raw/master/cb186568df1d51e3de82e02105da460-2024-5-2217:44:13.png)

