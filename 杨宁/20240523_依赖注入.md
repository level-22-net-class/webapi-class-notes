### 一、将简单的api转换为控制器模式的写法（Api写法）
1. program.cs部分代码
![pro-2024-5-2322:17:56.png](https://gitee.com/huangxuefang0929/xiu_img/raw/master/pro-2024-5-2322:17:56.png)
2. Startup.cs部分代码
![startup-2024-5-2322:17:48.png](https://gitee.com/huangxuefang0929/xiu_img/raw/master/startup-2024-5-2322:17:48.png)
3. HelloController.cs代码不变

### 二、依赖注入
#### 1.简介
- 组件与组件之间存在依赖关系，但是但一方不存在时，另一方就不能正常工作，这个时候就需要采用控制反转。
- 控制反转是面向对象编程中的一种设计原则，可以用来减低计算机代码之间的耦合度，其中最常见的方式叫做依赖注入，也可以说依赖注入和控制反转是一回事
- 依赖注入的方式主要有三种：
  - 构造函数注入
  - 属性注入：通过在需要注入的类中声明属性
  - 方法注入：将依赖项通过方法参数传递的方式进行依赖注入
#### 2.举例说明
~~~js
 // MyDependency.cs
 public class MyDependency
 {
    public void WriteMessage(string message)
    {
        Console.WriteLine($"调用的数据为{message}");
    }
 }
 
 // IndexModel.cs
 public class IndexModel 
 {
    // 假设在当前类中需要引用MyDependency类的实例
    private readonly MyDependency _dependency;
    public IndexModel(){
        // 创建一个MyDependency实例
        // MyDependency是一个依赖项，为IndexModel提供数据
        _dependency = new MyDependency();
    }
    public void DoSomething()
    {
        _dependency.WriteMessage("超级侦探");
    }
 }
 
 //Program.cs
  public static void Main(string[] args)
   {
        // 调用 CreateWebHostBuilder 方法来创建一个 IWebHostBuilder 实例
       var app = CreateWebHostBuilder(args).Build();
       // 在这里实例化 IndexModel 并调用方法进行测试
       var indexModel = new IndexModel();
       indexModel.DoSomething();
       // 运行应用程序
       app.Run();  //控制台输出：调用的数据为超级侦探
   }
~~~
从上面可知：
- 想要替换 MyDependency的结果，必须修改 IndexModel 类
- 很难进行单元测试
- 如果 MyDependency 具有依赖项，则必须由 IndexModel 类对其进行配置
依赖关系注入通过以下方式解决这些问题：
- 使用接口或基类将依赖关系实现抽象化
- 在服务容器中注册依赖关系。 ASP.NET Core 提供了一个内置的服务容器 IServiceProvider。 服务通常已在应用的 Program.cs 文件中注册
- 将服务注入到使用它的类的构造函数中。 框架负责创建依赖关系的实例，并在不再需要时将其释放
~~~js
 // 下面是构造函数注入
 // MyDependency.cs
 // IMyDependency 接口
 public interface IMyDependency
 {
    // 定义了一个方法 WriteMessage(string message)用于写入消息
    void WriteMessage(string message);
 }
 // MyDependency 类实现了 IMyDependency 接口
 // 提供了 WriteMessage 方法的具体实现
 public class MyDependency : IMyDependency
 {
    public void WriteMessage(string message)
    {
        Console.WriteLine($"调用的数据为{message}");
    }
 }
 
 // IndexModel.cs
 public class IndexModel 
 {
   // 构造_idependency属性，并在构造函数中注入（属性注入）
    private readonly IMyDependency _idependency;
    // 使用构造函数注入的方式来将 IMyDependency 的具体实现注入到 IndexModel 中
    public IndexModel(IMyDependency dependency){
        // 控制反转，不需要直接new实例化
        _idependency = dependency;
    }
    public void DoSomething()
    {
        _idependency.WriteMessage("超级侦探");
    }
 }
 
 // Startup.cs
 // 启动类，包含应用程序的配置信息
 public class Startup
 {
    public void Configure(IApplicationBuilder app)
    {
        // ......
    }
    public void ConfigureServices(IServiceCollection services)
    {
        // 将 MyDependency 类注册为单例服务
        // 注册 将IMyDependency实现类MyDependency实例化一个，放入容器
        services.AddSingleton<IMyDependency, MyDependency>();
        // 将 IndexModel 类注册为作用域服务
        services.AddScoped<IndexModel>();
    }
 }
 
 // program.cs
  public static void Main(string[] args)
 {
    var app = CreateWebHostBuilder(args).Build();
    // 在 using 语句中创建作用域，以便在作用域内获取服务
    using (var scope = app.Services.CreateScope())
    {
        // 获取服务提供者 services
        var services = scope.ServiceProvider;
        // 从服务提供者中获取 IndexModel 的实例
        var indexModel = services.GetService<IndexModel>();
        indexModel.DoSomething();  //控制台输出 调用的数据为超级侦探
    }
    app.Run();  
 }
~~~
#### 3.依赖注入容器
- 专门的类用来负责管理创建所需要的类，并创建它所有可能要用到的依赖，这个类就是依赖注入容器，也可以称之为控制反转容器（IoC容器）
- 可以把依赖注入容器看作一个用于创建对象的工厂，它负责向外提供被请求要创建的对象，当创建这个对象时，如果它又依赖了其他对象或者服务，那么容器会负责在其内部查找需要的依赖，并创建这些依赖，直至所有依赖项都创建完成后，最终返回被请求的对象
- 容器也负责管理所创建对象的生命周期

### 三、ASP.NET Core中的依赖注入
1. ASP.NET Core框架内部集成了自身的依赖注入容器
2. 所有被放入依赖注入容器的类型或组件称为服务，添加服务就使用Startup类的ConfigureServices方法
3. 服务的生命周期有以下三种：
   - Singleton：容器会创建并共享服务的单例，且一直存在于应用程序的整个生命周期里
   - TRansient：每次请求，总会创建新实例
   - Scope：在每次请求时会创建服务的新实例，并在这个请求内一直共享这个实例