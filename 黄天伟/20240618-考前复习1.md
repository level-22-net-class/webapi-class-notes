## 后端代码

### 配置应用程序和服务(Startup)
```c#
using Microsoft.EntityFrameworkCore;
using proj.Db;
using proj.Interfaces;
using proj.Services;

namespace proj;

public class Startup
{
    private readonly IConfiguration _configuration;
    public Startup(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    public void Configure(IApplicationBuilder app)
    {
        app.UseCors("Cors");
        app.UseRouting();
        app.UseEndpoints(endPoints =>
        {
            endPoints.MapControllers();
        });

    }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllers();
        services.AddDbContext<BlogDbContext>(opt =>
        {
            opt.UseSqlServer(_configuration.GetConnectionString("Mssql"));
        });
        services.AddCors(opt =>
        {
            opt.AddPolicy("Cors", buider =>
            {
                buider.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
            });
        });
        services.AddScoped(typeof(IRepositoryBase<>), typeof(RepositoryBase<>));
        services.AddAutoMapper(typeof(Startup));
    }
}
```

### 配置入口(Program)
```c#
namespace proj;
using Microsoft.AspNetCore;

public class Program
{
    public static void Main(string[] args)
    {
        CreateWebHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateWebHostBuilder(string[] args)
    {
        return Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webBuilder => {
            webBuilder.UseStartup<Startup>();
        });
    } 
}
```

### 定义实体类(Blog)
```c#
namespace BlogApi.Domain;

public class Blog
{
    public int BlogId { get; set; }
    public string Title { get; set; } = null!;
    public string Author { get; set; } = null!;
    public string? Tags { get; set; }
}

```

### 配置数据库上下文(BlogDbContext)
```c#
using Microsoft.EntityFrameworkCore;
using BlogApi.Domain;
namespace BlogApi.Db;

public class BlogDbContext : DbContext
{
    public BlogDbContext(DbContextOptions<BlogDbContext> options) : base(options)
    {

    }

    public DbSet<Blog> Blogs { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Blog>(entity =>
        {
            entity.HasKey(e => e.BlogId);
            entity.Property(e => e.BlogId).ValueGeneratedOnAdd();
            entity.HasData(
                new Blog
                {
                    BlogId=1,
                    Title = "111",
                    Author = "111",
                    Tags = "123"
                }// ...
            );
        });
    }
}
```

### 接口和实现
#### 接口
```c#
using BlogApi.Domain;

namespace BlogApi.Interface;

public interface IRepositoryBase<T> where T : class
{
    ICollection<T> GetAll();
    T? GetById(int id);
    void Delete(int id);
    void Insert(T entity);
    void Update(T entity);
}
```

#### 实现
```c#
using BlogApi.Db;
using BlogApi.Domain;
using BlogApi.Interface;
using Microsoft.EntityFrameworkCore;

namespace BlogApi.Services;

public class RepositoryBase<T> : IRepositoryBase<T> where T : class
{

    private readonly BlogDbContext _db;

    private readonly DbSet<T> _tb;

    public RepositoryBase (BlogDbContext db)
    {
        _db = db;
        _tb = db.Set<T>();
    }
    public void Delete(int id)
    {
        var entity = GetById(id);
        _tb.Remove(entity);
        _db.SaveChanges();
    }

    public ICollection<T> GetAll()
    {
        return _tb.ToList();
    }

    public T? GetById(int id)
    {
        return _tb.Find(id);
    }

    public void Insert(T entity)
    {
        _tb.Add(entity);
        _db.SaveChanges();
    }

    public void Update(T entity)
    {
        _tb.Update(entity);
        _db.SaveChanges();
    }
}
```

### 配置控制器处理HTTP请求
```c#
using AutoMapper;
using BlogApi.Domain;
using BlogApi.Dto;
using BlogApi.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BlogApi.Controller;

[Route("api/[controller]")]
[ApiController]
public class BlogController : ControllerBase
{
    private readonly IRepositoryBase<Blog> _repo;

    private readonly DbSet<Blog> _tb;

    private readonly IMapper _mapper;

    public BlogController(IRepositoryBase<Blog> repo, IMapper mapper)
    {
        _repo = repo;
        _mapper = mapper;
    }

    [HttpGet]
    public IActionResult Get(string? keyword = null)
    {
        if (!string.IsNullOrEmpty(keyword))
        {
            var blogs = _repo.GetAll().Where(item =>
                item.Title.Contains(keyword, StringComparison.OrdinalIgnoreCase) ||
                item.Author.Contains(keyword, StringComparison.OrdinalIgnoreCase) ||
                item.Tags.Contains(keyword, StringComparison.OrdinalIgnoreCase)
            );
            return Ok(blogs);
        }
        return Ok(_repo.GetAll());
    }

    [HttpGet("{id}")]
    public IActionResult GetBlogById(int id)
    {
        return Ok(_repo.GetById(id));
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        var blog = _repo.GetById(id);
        if (blog != null)
        {
            _repo.Delete(id);
            return Ok("删除成功");
        }
        else
        {
            return NotFound();
        }
    }

    [HttpPost]
    public ActionResult<Blog> Post(BlogInsertDto blogInsertDto)
    {
        if (string.IsNullOrEmpty(blogInsertDto.Title.Trim()))
        {
            return BadRequest("标题不能为空");
        }
        else if (string.IsNullOrEmpty(blogInsertDto.Author.Trim()))
        {
            return BadRequest("作者不能为空");
        }

        var blog = _mapper.Map<Blog>(blogInsertDto);
        return Ok("添加成功");
    }

    [HttpPut("{id}")]
    public ActionResult<Blog> Put(int id, BlogUpdatetDto blogUpdatetDto)
    {
        var blog = _repo.GetById(id);
        if (blog == null)
        {
            return NotFound();
        }
        if (string.IsNullOrEmpty(blogUpdatetDto.Title.Trim()))
        {
            return BadRequest("标题不能为空");
        }
        else if (string.IsNullOrEmpty(blogUpdatetDto.Author.Trim()))
        {
            return BadRequest("作者不能为空");
        }
        _mapper.Map(blogUpdatetDto, blog);
        return Ok("修改成功");

    }
}
```