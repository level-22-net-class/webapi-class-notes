### 路由是什么?
路由是指将用户请求映射到特定的处理程序（如控制器的操作方法）的过程。

### 作用
- 请求分发：路由决定了如何将传入的 HTTP 请求分发给相应的控制器和操作方法。

- URL 映射：路由将请求的 URL 映射到特定的控制器和操作方法，以便执行相应的业务逻辑。

- RESTful 设计：通过定义符合 RESTful 架构风格的路由模板，可以创建清晰、易懂的 API 接口。

- 参数传递：路由模板中的参数可以从 URL 中提取值，并将其传递给控制器操作方法进行处理。

- 路由优先级：当定义多个路由时，路由系统会根据路由的顺序选择最佳匹配，确保正确的路由被执行。

- 路由配置：在框架中通常有路由配置文件，您可以在其中定义路由模板、默认路由和约束等。

### 示例
```c#
[Route("/api/[controller]")] //定义控制器的路由前缀，
// 例如以下示例定义了BlogsController的控制器路由前缀为/api/blogs
public class BlogsController : ControllerBase
{
    // 处理对/blogs的GET请求
    public IActionResult Index(){
        return Ok("999"); //返回一个包含字符串"999"的200 OK响应
    }
    
    [Route("{id}")] //定义Single方法的路由模板
    // 处理对/blogs/{id}的GET请求,接受一个整型参数id
    public IActionResult Single(int id)
    {
        return Ok(id); //返回一个包含id值的200 OK响应
    }
}

// 上面两个函数可以合并写为以下形式：
    [Route("{id?}")]
    public IActionResult Index(){
        // 代码块    
    }
     public IActionResult Single(int id)
    {
         // 代码块 
    }
```

### Action特性
- [FromQuery]：从HTTP请求的查询字符串中获取参数的值
- [FromForm]：从表单中获取参数的值
- [FromHeader]：从HTTP 请求的头部信息中获取参数值
- [FromBody]：从请求的内容体获取参数值
- [FromServices]：从依赖注入容器中获取参数值
- [FromRoute]：从路由中获取参数值
- [BindRequiredAttribute]：如果没有值绑定到此参数，或绑定不成功，这个特性将添加一个ModelState错误
- [ApiController]：特性标记的控制器会自动验证请求体中的参数，并返回适当的 HTTP 响应