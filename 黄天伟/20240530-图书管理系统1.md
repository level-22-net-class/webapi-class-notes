# 一、项目设计
## 一、主体结构
1. Resultfull风格的WebApi
2. Asp.net core 8.0
3. 采用传统控制器模式

## 二、模型设计（例如：图书管理系统）
### 1.作者表Authors
1. 姓名 AuthorName
2. 性别 Gender
3. 出生年月 Birthday
### 2.图书表Books
1. 书名BookName
2. 出版社Publiser
3. 作者AuthorId
4. 价格Price

## 三、内存型数据库（自定义的一个集合类）
在应用程序中定义一个类，专门提供模拟数据

## 四、仓储系统（准备用来接入数据库的一套接口）
### 1.通用型仓储接口
定义了通用的数据存取操作方法，例如增删改查
### 2.个性化的仓储接口
针对特定业务需求而定义的数据存取接口，通常继承自通用型仓储接口，通过扩展通用接口来实现特定业务需求的操作

## 五、利用主体结构，具体实现Resultfull风格的关于两个表的CRUD
### 1.作者表
- 获取作者列表 get/api/authors
- 获取指定作者 get/api/authors/29
- 新增作者信息 post/api/authors
- 修改作者信息 put/api/authors/29
- 删除作者信息 delete/api/authors/29
### 2.图书表-部署于作者表底下
- 获取图书列表 get/api/authors/9/books
- 获取指定图书 get/api/authors/9/books/29
- 新增图书信息 post/api/authors/9/books
- 修改图书信息 put/api/authors/9/books/29
- 删除图书信息 delete/api/authors/9/books/29
