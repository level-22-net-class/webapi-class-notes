### 路由
1. 在控制器上写上两个特性 [ApiController].[Route("/api/..")]
2. 如果不符合restfull风格的路由的话，在action上单独写路由
[HttpGet("/api/...")]

### 关于匹配到函数的处理
- 入参：函数的参数，模型绑定
- 出参：返回的响应，1.状态码 2.带对象的状态码  3.重定向  4.内容：{Code:1000,data:[],msg:"请求成功"}

### 过滤器
过滤器和中间件相似，能够在某些功能前后执行，通常用于执行日志记录、身份验证、授权、异常处理和结果转换等操作

ASP.NET Core中提供了五种类型的过滤器：
- 授权过滤器（Authorization Filter）：最先执行，用于检查用户是否有权访问资源，并在未经授权时拦截请求，实现IAsyncAuthorizationFilter或IAuthorizationFilter接口
- 资源过滤器（Resource Filter）：在Authorization后执行，并在其他过滤器之前和之后执行，实现IAsyncResourceFilter或IResourceFilter接口
- 动作过滤器（Action Filter）：在执行Action之前或之后执行自定义逻辑，而且在模型绑定后执行，实现IAsyncActionFilter或IActionFilter接口
- 异常过滤器（Exception Filter）：用于捕获并处理动作方法中抛出的异常，实现IAsyncExceptionFilter或IExceptionFilter接口
- 结果过滤器（Result Filter）：在IActionResult执行的前后执行，能够控制Action的执行结果，比如格式化结果等。需要注意的是，它只有在Action方法成功执行完成后才会运行，实现IAsyncResultFilter或IResultFilter接口

```c#
 // 对于每次请求都需要设置return结果，这里可以使用Result过滤器格式化结果
 // 1. Dto/ApiResult.cs
   public class ApiResult
    {
        public int Code { get; set; }
        public string? Msg { get; set; }
        public object Data { get; set; }
    }
 // 2. Filters/ApiResultFilter.cs 实现接口
   using Microsoft.AspNetCore.Mvc;
   using Microsoft.AspNetCore.Mvc.Filters;
   using System.Threading.Tasks;
   namespace Api.Filters;
   public class ApiResultFilter:IAsyncResultFilter{
       public async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
           {
            // 判断返回结果是否为内容，如果是则给context.Result赋值一个新的实例对象ApiResult
               if (context.Result is ObjectResult objectResult)
               {
                   context.Result = new ObjectResult(new ApiResult
                   {
                       Code = 1000,
                       Msg = "请求成功",
                       Data = objectResult.Value
                   });
               }
               else
               {
                   context.Result = new ObjectResult(new ApiResult {    Code = 4000, Msg = "无效请求" });
               }
               // 必须记得调用，否则不能执行下一个Action  
               await next();  
           }
   } 
 //  3. Startup.cs 注册服务
  public void ConfigureServices(IServiceCollection services)
    {
        // 全局过滤
        services.AddControllers(options => {
            options.Filters.Add<ApiResultFilter>();
        }); 
    }
```


### 配置
访问json配置文件
```c#
// appsetting.json
    {
        "MySettings": {
        "Setting1": "Value1",
        "Setting2": "Value2"
      }
    }
 // startup.cs
   // IConfiguration 用于读取应用程序的配置
    private readonly IConfiguration _configuration;
    public Startup(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    public void Configure(IApplicationBuilder app)
    {
        app.UseRouting(); 
        app.UseEndpoints(endpoints => {
            endpoints.MapControllers(); 

        }); 
    }
    public void ConfigureServices(IServiceCollection services)
    {
        // 输出配置值并打印到控制台
        var setting1 = _configuration["MySettings:Setting1"];
        Console.WriteLine($"Setting1: {setting1}");
    }
```

### 日志
日志不会为应用程序增加实质性的功能，但是能够让开发人员跟踪程序的运行、调试程序以及记录错误信息

日志有两种类型：
- 系统日志：系统在运行时向外输出的记录信息
- 用户记录日志：开发人员在程序中适当的位置调用与日志功能相关的API输出

由以下接口组成：（都位于Microsoft.Extensions.Logging命名空间下
- Ilogger：实际执行记录日志操作的方法
- IloggerProvider：用于创建ILogger对象
- IloggerFactory：通过ILoggerProvider对象创建ILogger对象

日志级别：
- Trace:级别最低
- Debug：用于记录调试信息
- Information：用于记录应用程序的执行流程信息
- Warning：用于记录应用程序出现的轻微错误或其他不会导致程序停止的警告信息
- Critical：严重级别最高，用于记录引起应用程序崩溃、灾难性故障等信息