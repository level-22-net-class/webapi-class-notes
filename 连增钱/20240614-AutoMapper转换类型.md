## AutoMapper 转换类型

1. 安装 AutoMapper 依赖
   
   - dotnet add package AutoMapper
2. 配置 AutoMapper
   - 定义一个 Helper 类文件夹
     ```csharp
        using AutoMapper;
         public class AutoMapperHelper:Profile
         {
             public AutoMapperHelper ()
             {
                 CreateMap<AuthorDto，Authors>();
             }
         }
     ```
   - Startup 中
   ```csharp
       services.AddAutoMapper(typeof(AutoMapperHelper));
   ```
3. 使用
   - 控制器
   ```csharp
        using AutoMapper;
        private readonly IMapper _mapper;
        public AuthorsController(/*其他注入 */IMapper mapper)
        {
            /*其他注入 */
            _mapper=mapper;
        }
        //使用，后转前
        _mapper.Map<Authors>(AuthorDto)
   ```
