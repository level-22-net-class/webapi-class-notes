## 路由查询·
### 接口
```C# 
 using BookStore.Api.Dto;

namespace BookStore.Api.Interface;

public interface IAuthorRepository
{
    // 通过Id获取指定作者的方法
    AuthorDto? GetAuthorById(Guid id);

    // 获取所有作者的方法
    // 函数三要素：函数名称、函数参数、函数返回值
    ICollection<AuthorDto> GetAllAuthors();


    AuthorDto Insert(AuthorCreateDto authorCreateDto);
  
}
```
### 实现
```C#  
   public AuthorDto Insert(AuthorCreateDto authorCreateDto)
    {
        // 将传入的dto转换为保存到数据库需要的实体类型
        var author = new Authors
        {
            Id = Guid.NewGuid(),
            AuthorName = authorCreateDto.AuthorName,
            Gender = authorCreateDto.Gender,
            Birthday = authorCreateDto.Birthday,
        };
        // 插入数据库
        BookStoreDb.Instance.Authors.Add(author);
        // 将内存数据库获取的数据转换为AuthorDto的实例
        var authorDto = new AuthorDto { Id = author.Id, AuthorName = author.AuthorName, Gender = author.Gender };
        return authorDto;
    }

    public ICollection<AuthorDto> GetAllAuthors()
    {
        // 这个实现应该从持久化的数据源中获得：数据库、文件、各种异构数据、从各种api中获取的数据
        var list = BookStoreDb.Instance.Authors.ToList();
        var resultList = new List<AuthorDto>();// let resultList=[]
        list.ForEach(item =>
        {
            // 实例化一个对象这里有2种方式，一个是直接调用构造函数,形如：new AuthorDto()
            // 另一个是直接填充其属性,形如：new AuthorDto{}
            var tmp = new AuthorDto { Id = item.Id, AuthorName = item.AuthorName, Gender = item.Gender };
            resultList.Add(tmp);
        });
        return resultList;
    }

    public AuthorDto? GetAuthorById(Guid id)
    {
        var tmp = BookStoreDb.Instance.Authors.SingleOrDefault(item => item.Id == id);
        var tmpResult = tmp != null ? new AuthorDto { Id = tmp.Id, AuthorName = tmp.AuthorName, Gender = tmp.Gender } : null;
        // dynamic xResult=null;
        // if(tmp!=null){
        //     xResult=new AuthorDto { Id = tmp.Id, AuthorName = tmp.AuthorName, Gender = tmp.Gender };
        // }
        return tmpResult;
    }
```
### action
```C# 
   [HttpGet("{id?}")]
    public IActionResult Get(Guid id)
    {
        if (id.ToString() == "00000000-0000-0000-0000-000000000000")
        {
            return Ok(_authorRepository.GetAllAuthors());
        }
        else
        {
            var item = _authorRepository.GetAuthorById(id);
            return Ok(item);
        }
    }

    /// <summary>
    /// 新增作者
    /// </summary>
    /// <param name="authorCreateDto">接受的参数</param>
    /// <returns></returns>
    [HttpPost]
    public IActionResult Post(AuthorCreateDto authorCreateDto)
    {
        /*
            1.拿到AuthorCreateDto类型的实例化数据-模型绑定
            2.将相关数据保存到数据库
                1.转换AuthorCreateDto类型的数据为Authors
                2.调用数据库上下文，将数据插入

        */
        var result = _authorRepository.Insert(authorCreateDto);
        return Ok(result);
    }

```