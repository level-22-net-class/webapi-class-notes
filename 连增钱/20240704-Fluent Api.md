Fluent API（流畅 API）是 Entity Framework Core 提供的一种配置数据库模型的方法，它允许开发者通过链式调用方法来定义实体类与数据库之间的映射关系，而不需要使用传统的属性标记（attributes）或 XML 配置文件。这种方式使得配置更加直观和易于理解。

### Fluent API 的优势和用法：

1. **可读性和可维护性**：
   - Fluent API 使用链式调用来配置实体，使得配置代码更加紧凑和可读性更高，特别是在配置复杂关系或者索引时，相比使用属性标记或者 XML 配置，更易于维护和理解。

2. **灵活性**：
   - Fluent API 提供了丰富的方法来配置实体的各个方面，如表名、列名、主键、外键关系、索引、约束等，可以精确地控制实体在数据库中的映射。

3. **集中式配置**：
   - 使用 Fluent API 可以将所有与数据库映射相关的配置集中在一个类中，通常是一个继承自 `IEntityTypeConfiguration<TEntity>` 的配置类，这样有助于代码组织和维护。

4. **不侵入性**：
   - Fluent API 的配置不会侵入到实体类本身，这意味着你可以保持实体类的纯粹性，不需要引入和依赖于特定的 ORM 特性。

### 示例和常见用法：

下面是一些使用 Fluent API 配置实体映射的示例，假设我们有一个 `Product` 实体和一个 `Category` 实体，它们之间是一对多的关系：

```csharp
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class Product
{
    public int ProductId { get; set; }
    public string Name { get; set; }
    public decimal Price { get; set; }

    public int CategoryId { get; set; }
    public Category Category { get; set; }
}

public class Category
{
    public int CategoryId { get; set; }
    public string Name { get; set; }

    public ICollection<Product> Products { get; set; }
}

public class ProductConfiguration : IEntityTypeConfiguration<Product>
{
    public void Configure(EntityTypeBuilder<Product> builder)
    {
        // 表名
        builder.ToTable("Products");

        // 主键
        builder.HasKey(p => p.ProductId);

        // 列配置
        builder.Property(p => p.Name)
            .HasColumnName("ProductName")
            .HasMaxLength(100)
            .IsRequired();

        builder.Property(p => p.Price)
            .HasColumnType("decimal(18,2)")
            .IsRequired();

        // 外键关系
        builder.HasOne(p => p.Category)
            .WithMany(c => c.Products)
            .HasForeignKey(p => p.CategoryId);
    }
}

public class CategoryConfiguration : IEntityTypeConfiguration<Category>
{
    public void Configure(EntityTypeBuilder<Category> builder)
    {
        // 表名
        builder.ToTable("Categories");

        // 主键
        builder.HasKey(c => c.CategoryId);

        // 列配置
        builder.Property(c => c.Name)
            .HasMaxLength(50)
            .IsRequired();

        // 导航属性配置
        builder.HasMany(c => c.Products)
            .WithOne(p => p.Category)
            .HasForeignKey(p => p.CategoryId);
    }
}
```

在上面的示例中，我们展示了如何使用 Fluent API 配置 `Product` 和 `Category` 实体的映射关系和表结构：

- `ProductConfiguration` 和 `CategoryConfiguration` 类实现了 `IEntityTypeConfiguration<T>` 接口，并在 `Configure` 方法中使用 Fluent API 来配置实体的映射关系。
- 通过 `builder.ToTable` 方法指定了实体在数据库中对应的表名。
- 使用 `builder.HasKey` 方法配置了主键。
- 使用 `builder.Property` 方法配置了各个属性的列名、数据类型、最大长度等信息。
- 使用 `builder.HasOne`、`builder.WithMany` 和 `builder.HasForeignKey` 方法配置了实体之间的关系，如外键关系。
