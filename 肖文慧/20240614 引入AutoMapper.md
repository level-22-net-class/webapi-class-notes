## AutoMapper

```c#
下载相关包

命令：

dotnet add package AutoMapper.Extensions.Microsoft.DependencyInjection


新建helpr文件夹

代码：


using AutoMapper;
using BookStore.Api.Domain;
using BookStore.Api.Dto;

namespace BookStore.Api.Helper;

public class BookStoreAutorMapperProfile : Profile
{
    public BookStoreAutorMapperProfile()
    {
        CreateMap<Authors, AuthorDto>();
        CreateMap<Books, BookDto>();

        CreateMap<AuthorCreateDto, Authors>();
        CreateMap<BookCreateDto, Books>();

        CreateMap<AuthorUpdateDto, Authors>();
        CreateMap<BookUpdateDto, Books>();
    }
}
```