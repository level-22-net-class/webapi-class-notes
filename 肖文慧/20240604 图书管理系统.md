### 小知识and实现方式

1. ICollection 接口是 System.Collections 命名空间中类的基接口，ICollection 接口扩展 IEnumerable，IDictionary 和 IList 则是扩展 ICollection 的更为专用的接口。 如果 IDictionary 接口和 IList 接口都不能满足所需集合的要求，则从 ICollection 接口派生新集合类以提高灵活性。 ICollection是IEnumerable的加强型接口，它继承自IEnumerable接口，提供了同步处理、赋值及返回内含元素数目的功能。

2. SingleOrDefault():返回序列中的唯一元素；如果该序列为空，则返回默认值；如果该序列包含多个元素，此方法将引发异常

```c#
Controller

//为了在action中操作数据，要先从依赖注入容器中获取之前定义的仓储接口。
    //在Controller中，构造函数的注入是一种常见的注入方式
    private readonly IBlogRepository _blogRepository;

    public BlogController(IBlogRepository blogRepository)
    {
        _blogRepository=blogRepository;
    }

```