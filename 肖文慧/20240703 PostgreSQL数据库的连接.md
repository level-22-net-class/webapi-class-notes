## PostgreSql数据库的连接

### 如何在云服务器上配置数据库

https://developer.aliyun.com/article/745938

### 连接数据库

定义数据库上下文

将ORM工具包引用到应用层

迁移数据库

更新数据库

（以应用层为开始文件，ORM工具层为当前文件操作）


数据库自建用户
![图](./img/postgresql图1.png)

项目引用命令解释：
```c#
dotnet add 项目1 reference 项目二

将项目2添加到项目1中
```

### 命令
```c#
进入数据库命令

 su - postgres

卸载postgresql数据库

1. 先停止数据库服务
sudo service postgresql stop

2.卸载数据库软件包
sudo yum remove postgresql

3.删除数据库数据目录
sudo rm -rf /var/lib/postgresql

4.删除数据库配置文件和日志文件
sudo rm -rf /etc/postgresql
sudo rm -rf /var/log/postgresql


安装数据库
官网

如果显示active(running)就表示服务启动成功。

```