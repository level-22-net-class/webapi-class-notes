### AutoMapper使用
- 第一步：安装 dotnet add 
- 第二步：配置，定义Profile类型配置类
- 第三步：注册，autoMapper服务器到容器
- 第四步:在控制器或者其它接口服务中，通过构造函数注入

### Profile
- Profile 是组织映射的另一种方式。新建一个类，继承 Profile，并在构造函数中配置映射。
```js
public class EmployeeProfile : Profile
{
    public EmployeeProfile()
    {
        CreateMap<Employee, EmployeeDto>();
    }
}

var config = new MapperConfiguration(cfg =>
{
    cfg.AddProfile<EmployeeProfile>();
});
```

- Profile 内部的配置仅适用于 Profile 内部的映射。应用于根配置的配置适用于所有创建的映射。

- AutoMapper 也可以在指定的程序集中扫描从 Profile 继承的类，并将其添加到配置中。