# 依赖注入
依赖注入（Dependency Injection，DI）是.NET Core应用程序中常用的设计模式，用于管理和提供应用程序需要的依赖项。在API .NET Core中，依赖注入可用于将服务（services）注入到控制器、中间件、过滤器等组件中。以下是有关API .NET Core依赖注入的知识点和示例：

1. 依赖注入容器：在.NET Core中，依赖注入容器负责管理应用程序中的依赖关系，用于解析所需的服务实例。一个常用的依赖注入容器是.NET Core内置的IServiceProvider接口。

2. 注册服务：在Startup.cs文件的ConfigureServices方法中，可以注册应用程序需要使用的服务。服务可以通过AddTransient、AddScoped和AddSingleton方法注册，分别表示瞬时、作用域和单例生命周期。

3. 服务提供者：在需要使用服务的地方，可以通过依赖注入将服务注入到控制器或其他组件中。ASP.NET Core会自动创建服务提供者，开发人员可以在构造函数或方法参数中请求所需的服务。

以下是一个简单的示例，展示了如何在API .NET Core中使用依赖注入：

```csharp
// 服务接口
public interface IMyService
{
    void DoSomething();
}

// 服务实现
public class MyService : IMyService
{
    public void DoSomething()
    {
        Console.WriteLine("Doing something...");
    }
}

// 在Startup.cs中注册服务
public void ConfigureServices(IServiceCollection services)
{
    services.AddTransient<IMyService, MyService>();
}

// 控制器
[ApiController]
[Route("api/[controller]")]
public class MyController : ControllerBase
{
    private readonly IMyService _myService;

    public MyController(IMyService myService)
    {
        _myService = myService;
    }

    [HttpGet]
    public IActionResult Get()
    {
        _myService.DoSomething();
        return Ok();
    }
}
```

在上面的示例中，我们定义了一个名为IMyService的服务接口和一个实现该接口的MyService类。在Startup.cs中使用AddTransient方法注册该服务，然后在控制器中通过构造函数进行依赖注入，使控制器可以使用MyService服务。当调用控制器的Get方法时，会调用MyService的DoSomething方法。

这是一个简单的示例，实际中使用依赖注入通常会包含更多的服务和复杂的依赖关系。通过使用依赖注入，可以提高代码的可测试性、可维护性和可扩展性。
