## 在ASP.NET Core应用程序中配置CORS（跨域资源共享）策略
```
public void ConfigureServices(IServiceCollection services)
{
    // 添加跨域服务
    services.AddCors(options => {
        // "AllowOrigin"是策略的名称，你可以根据需要命名它
      options.AddPolicy("AllowOrigin",builder => {
        builder.AllowAnyOrigin()    //指定允许来自任何来源的请求，WithOrigins制定对某些域名开放
               .AllowAnyMethod()    //指定允许任何HTTP方法
               .AllowAnyHeader();   //指定允许任何请求头
      });
    });
}
public void Configure(IApplicationBuilder app,IHostEnvironment env)
{
    // 启用路由中间件后使用
    app.UseCors();
}
```