### 创建新的.NETCore项目
在VS Code中打开终端，执行以下命令创建一个新的.NETCore项目：
```bash
dotnet new console -n MyProject
```

### 创建新的ASP.NETCore项目
要创建ASP.NETCore Web应用程序，可以使用以下命令：
```
dotnet new webapi -n MyWebApi
```

### 调试.NETCore应用程序
在VS Code中打开项目文件夹，按下`F5`启动调试。你可以在`.vscode`文件夹中配置调试器设置。

### 编写控制器和路由
创建一个控制器类，并使用路由属性定义路由：
```
[Route("api/[controller]")]
public class MyController : ControllerBase
{
    [HttpGet]
    public ActionResult<string> Get()
    {
        return "Hello, World!";
    }
}
```

### 运行ASP.NETCore应用
使用以下命令运行ASP.NETCore应用程序：
```
dotnet run
```