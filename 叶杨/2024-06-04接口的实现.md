### 一、配置接口（IBookRepository）：
1. **GetBookById(Guid id)**：获取指定 id 的图书。
2. **GetAllBooks(Guid authorId)**：获取指定作者的全部图书。
3. **AddBook(Guid authorId, AddBookDto addBookDto)**：添加图书。
4. **UpdateBook(Guid bookId, BookDtoUpdate bookDtoUpdate)**：修改图书。
5. **DelBook(Guid bookId)**：删除图书。

### 二、实现接口：
1. **AddBook(Guid authorId, AddBookDto addBookDto)**：添加图书的具体实现，包括创建新的图书对象并将其添加到数据库。
2. **GetAllBooks(Guid authorId)**：获取指定作者的全部图书的具体实现。
3. **GetBookById(Guid id)**：获取指定 id 的图书的具体实现。
4. **UpdateBook(Guid bookId, BookDtoUpdate bookDtoUpdate)**：修改图书的具体实现，包括更新图书信息。
5. **DelBook(Guid id)**：删除图书的具体实现。

### 三、控制器操作：
1. **Get(Guid authorId, Guid? bookId)**：根据作者 ID 获取图书信息，可以根据图书 ID 获取特定图书信息。
2. **Post(Guid authorId, AddBookDto addBookDto)**：添加图书，需要提供作者 ID 和要添加的图书信息。
3. **Put(Guid authorId, Guid bookId, BookDtoUpdate bookDtoUpdate)**：修改图书信息，需要提供作者 ID、图书 ID 和要更新的图书信息。
4. **Del(Guid authorId, Guid bookId)**：删除图书，需要提供作者 ID 和要删除的图书 ID。

### 集合接口整理（PlantUML版）：
- 图书（Books）集合接口的结构图已经展示在您提供的 PlantUML 图中，但我无法在当前环境中显示该图片。您可以参考您提供的 PlantUML 图来查看集合接口的结构。

通过上述接口、实现和控制器操作，实现了对图书数据的增删改查功能，保持了良好的结构化和逻辑清晰，有助于代码的可维护性和可扩展性。