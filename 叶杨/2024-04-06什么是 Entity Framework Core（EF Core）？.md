## 什么是 Entity Framework Core（EF Core）？

Entity Framework Core（EF Core）是一个轻量级、可扩展且跨平台的对象关系映射（Object-Relational Mapping，ORM）框架，用于在.NET应用程序中实现数据访问。

## 主要特点

- **跨平台性**：EF Core支持在Windows、Linux和macOS上运行，可在不同的操作系统上开发和部署应用程序。
  
- **轻量级**：与以前的Entity Framework版本相比，EF Core更为轻量级，同时保持核心功能，使其更易于学习和使用。
  
- **可扩展性**：EF Core允许通过插件方式扩展功能，例如使用第三方提供的数据库提供程序或插件，以满足特定需求。

## 支持的数据库

EF Core支持多种数据库，包括但不限于：
- SQL Server
- MySQL
- PostgreSQL
- SQLite
- Oracle