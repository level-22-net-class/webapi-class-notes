- [小型项目](https://gitee.com/level-22-net-class/webapi-class-notes/blob/master/吴诗茵/20240530-8.小型项目.md#小型项目)

## 小型项目

在Program.cs中写：

```
namespace BookStore.Api;

public static class Program
{
    public static void Main(string[] args)
    {
        CreateWebHostBuilder(args).Build().Run();
    }
    public static IHostBuilder CreateWebHostBuilder(string[] args)
    {
        return Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webBuilder=>
        {
            webBuilder.UseStartup<Startup>();
        });
    }
}
```

在Startup.cs中写：

```
namespace BookStore.Api;

public class Startup
{
    public void Configure(IApplicationBuilder app)
    {
        app.UseRouting();
        app.UseEndpoints(endpoints=>
        {
            endpoints.MapControllers();
        });
    }
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllers();
    }
}
```

在AuthorsController.cs中写：

```
using BookStore.Api.Db;
using BookStore.Api.Domain;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.Api.Controller;

[ApiController]
[Route("api/[controller]")]
public class AuthorsController : ControllerBase
{
    [HttpGet("{id?}")]
    public IActionResult Get(int id)
    {
        var db=new BookStoreDb();
        db.Authors.Add(new Authors{AuthorName="思航航",Gender=1,Birth=Convert.ToDateTime("2000-8-8")});
        var list=db.Authors.ToList();
        return Ok(list);
    }
    [HttpPost("")]
    public IActionResult Post()
    {
        return Ok();
    }
    [HttpPut("{id?}")]
    public IActionResult Put(int id)
    {
        return Ok(id);
    }
    [HttpDelete("{id?}")]
    public IActionResult Delete(int id)
    {
        return Ok(id);
    }
}
```