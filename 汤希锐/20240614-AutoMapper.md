## AutoMapper
AutoMapper是一个**对象到对象**的**映射库**，它**用于在C#中简化将一个对象的属性值复制到另一个对象的过程**。这在数据传输对象（DTOs）和数据库模型之间转换时非常有用，尤其是在使用ORM（对象关系映射）框架如Entity Framework时

### 使用步骤
1. 安装依赖包
   ```c#
    dotnet add package AutoMapper
   ```
2. 配置，定义Profile类型配置类,用于在应用程序中自动映射对象之间的关系
   ```c#
    namespace Library.Helper;
    public class BookStoreAutoMapperProfile : Profile
    {
       public BookStoreAutoMapperProfile()
       {
          // 将 Author 对象映射到 AuthorGetDto 对象，这通常用于从数据库读取数据后返回给客户端
           CreateMap<Author , AuthorGetDto>();
           CreateMap<Books , BookGetDto>();
    
           CreateMap<AuthorCreateDto,Author>();
           CreateMap<BookCreateDto,Books>();
    
           CreateMap<AuthorPutDto , Author>();
           CreateMap<BookPutDto , Books>();
       }
    }

    CreateMap方法的两个泛型参数分别指明对象映射中的源和目标,可以理解第一个参数为源，第二个参数为目标
    - 从数据库中获取数据时，实体类为源，DTO为目标
    - 增改时，DTO为源，实体类为目标
   ```
3. 注册AutoMapper服务到容器
   ```c#
    services.AddAutoMapper(typeof(BookStoreAutoMapperProfile));
   ```
4. 使用
   ```c#
    -- controller --
    1> 通过构造函数注入
       private readonly IMapper _mapper;
       public BookController(IBookRepository bookRepository, IRepository<Books> brep, IRepository<Author> aurep, IMapper mapper)
       {
           _bookrep = bookRepository;
           _brep = brep;
           _aurep = aurep;
           _mapper = mapper;
       }
    2> 使用（两种转换方法）
       1》将实体类转换成DTO：
               单体形式(GetId)： var result = _mapper.Map<BookGetDto>(temp);
               集合形式(GetAll)： var result = _mapper.Map<IEnumerable<BookGetDto>>(temp);
       2》将DTO转换成实体类： var result = _mapper.Map(BookGetDto , Book); 
   ```