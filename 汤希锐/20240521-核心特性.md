#### 创建一个Api项目
```
cd src
dotnet new sln -n Admin2024
dotnet new webapi -n Admin2024.Api
dotnet new webapi -n Admin2024.Domain
```
```
解决方案和项目之间的联系：
1.编译或者打包解决方案的时候，会同时编译或者打包其下所有项目
2.如果运行解决方案的话，则会按照编排的（指定的启动项目），分别调用指定项目
```

```
将Admin2024.api放入解决方案中（编译）
dotnet sln add .\Admin2024.Api
dotnet build
dotnet sln add .\Admin2024.Domain 
dotnet build 
添加依赖包：dotnet add package Microsoft
dotnet restore
```

#### 核心特性
```
Admin2024.Api:
app.MapGet("api/users",()=>{
    var list=new list<dynamic>();
    list Add(new {
        Id=1,
        username="admin",
        nickname="超级管理员"
    });
      list Add(new {
        Id=2,
        username="admin",
        nickname="普通管理员"
    });
})
return list;

终端：dotnet run --project .\src\Admin2024.Api\
```

##### 中间件
```
programs.cs:
app.usecasync(ctx,next=>{
    var forecast=Enumerable.Range(1,10);
    ctx.Response.Writesync(forecast)
    app.Run();
})

终端：dotnet watch --project .\src\Admin2024.Api\
```

```
var builder = WebApplication.CreateBuilder(args);

var app = builder.Build();

app.MapGet("/",async ctx => {
    
    await ctx.Response.WriteAsync("8888");
});


app.MapGet("/leaf",async ctx => {

    await ctx.Response.WriteAsync("9999");
});

app.Run();

```

```
api.http:
@url=http://localhost:5000

###
GET {{url}}/api/users HTTP/1.1

### 
GET {{url}}/weatherforecast HTTP/1.1

###
GET {{URL}}/HTTP/1.1
```