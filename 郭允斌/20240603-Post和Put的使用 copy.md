- AuthorController
```csharp
  //获取所有
    [HttpGet("{id?}")]
    public IActionResult Get(Guid id)
    {
        //用我们注入的接口，使用其中我们定义的函数方法去实现功能
        var list = _authorRepository.GetAllAuthors(id);
        return Ok(list);
    }
    //保存
    [HttpPost]
    public ActionResult<Authors> Post(AuthorCreateDto AuthorCreateDto)
    {
        //用我们注入的接口，使用其中我们定义的函数方法去实现功能
        var list = _authorRepository.Create(AuthorCreateDto);
        return Ok(list);
    }
    //修改
    [HttpPut("{id}")]
    public ActionResult<Authors> Put(Guid id, AuthorEditDto authorEditDto)
    {
        //用我们注入的接口，使用其中我们定义的函数方法去实现功能
        var list = _authorRepository.Edit(id, authorEditDto);
        return Ok(list);
    }  
```
- AuthorStoreDb
```csharp
//初始化一个AuthorStoreDb对象
    public static AuthorStoreDb Index { get; set; } = new AuthorStoreDb();
    //初始化一个ICollection集合类型的对象,将它new成一个list的对象(Authors是你创建的Domain中的实例化对象)
    public ICollection<Authors> Authors { get; set; } = new List<Authors>();
    public ICollection<Books> Books { get; set; } = new List<Books>();
    //给内存型的数据库添加数据，List集合使用Add添加
    public AuthorStoreDb()
    {
        Authors.Add(new Authors
        {
            Id = Guid.NewGuid(),
            AuthorName = "aaaa",
            Gender = 1
        });
        Authors.Add(new Authors
        {
            Id = Guid.NewGuid(),
            AuthorName = "bbbb",
            Gender = 0
        });
        Authors.Add(new Authors
        {
            Id = Guid.NewGuid(),
            AuthorName = "cccc",
            Gender = 1
        });
        Books.Add(new Books
        {
            Id = Guid.NewGuid(),
            BookName = "aaa",
            AuthorId = Guid.NewGuid()
        });
        Books.Add(new Books
        {
            Id = Guid.NewGuid(),
            BookName = "bbb",
            AuthorId = Guid.NewGuid()
        });
        Books.Add(new Books
        {
            Id = Guid.NewGuid(),
            BookName = "cccc",
            AuthorId = Guid.NewGuid()
        });

    }
```
- Startup
```csharp
public void ConfigureServices(IServiceCollection service)
    {
        //注册控制器
        service.AddControllers();
        // 将仓储服务注册到容器
        service.AddScoped<IAuthorRepository, AuthorRepository>();
    }
```
- IAuthorRepository
```csharp
using Admin8.Domain;
using Admin8.Dto;

namespace Admin8.Interface;

public interface IAuthorRepository
{
    //定义接口中的函数，返回类型，形参
    //定义获取的方法
    ICollection<AuthorDto>? GetAllAuthors(Guid id);
    //定义增加的方法
    AuthorDto Create(AuthorCreateDto authorCreateDto);
    //定义编辑的方法
    AuthorDto? Edit(Guid id, AuthorEditDto authorEditDto);
    //定义删除的方法
    AuthorDto? Delete(Guid id);
}
```
- AuthorRepository
```csharp
//获取全部或者指定Guid的作者（AuthorDto中我的是将Password隐藏了的，如果不用的话，考试时可以直接改为Author）
    public ICollection<AuthorDto>? GetAllAuthors(Guid guid)
    {
        //判断传进来的Id是否存在，不存在的Guid默认值为00000000-0000-0000-0000-000000000000
        if (guid.ToString() == "00000000-0000-0000-0000-000000000000")
        {
            //在内存型数据库中获取到所有的数据
            var list = AuthorStoreDb.Index.Authors.ToList();
            //创建一个List集合保存需要返回的数据（我这里返回的数据中将Password隐藏了，如果不需要隐藏，以下省略）
            var result = new List<AuthorDto>();
            list.ForEach(item =>
            {
                var temp = new AuthorDto { Id = item.Id, AuthorName = item.AuthorName, Gender = item.Gender };
                result.Add(temp);
            });
            //将数据返回（我这里返回的数据中将Password隐藏了，如果不需要隐藏，以下返回改为list）
            return result;
        }
        else
        {
            //在内存型数据库中获取到指定Guid的数据
            var list = AuthorStoreDb.Index.Authors.FirstOrDefault(item => item.Id == guid);
            //没有则返回空，并结束函数
            if (list == null)
            {
                return null;
            }
            //创建一个List集合保存需要返回的数据（我这里返回的数据中将Password隐藏了，如果不需要隐藏，以下省略）
            var result = new List<AuthorDto>();
            var obj = new AuthorDto { Id = list.Id, AuthorName = list.AuthorName, Gender = list.Gender };
            result.Add(obj);
            //将数据返回（我这里返回的数据中将Password隐藏了，如果不需要隐藏，以下返回改为list）
            return result;
        }

    }
    //添加作者信息
    public AuthorDto Create(AuthorCreateDto authorCreateDto)
    {
        //创建一个新的Authors，其中将上面传进来的authorCreateDto数据保存进去
        var obj = new Authors
        {
            //Guid.NewGuid()这是让他自己创建一个新的Guid
            Id = Guid.NewGuid(),
            AuthorName = authorCreateDto.AuthorName,
            Gender = authorCreateDto.Gender,
            Password = authorCreateDto.Password
        };
        //将创建的数据保存进内存型数据库(List用Add添加)，AuthorStoreDb中看一下结构，逐级.下去的
        AuthorStoreDb.Index.Authors.Add(obj);
        var list = new AuthorDto { Id = obj.Id, AuthorName = obj.AuthorName, Gender = obj.Gender };
        //将数据返回（我这里返回的数据中将Password隐藏了，如果不需要隐藏，以下返回改为obj）
        return list;
    }
    //修改
    public AuthorDto? Edit(Guid guid, AuthorEditDto authorEditDto)
    {
        //先在内存型数据库中找到是否有这个guid
        var list = AuthorStoreDb.Index.Authors.FirstOrDefault(item => item.Id == guid);
        if (list == null)
        {
            return null;
        }
        //不为空就不会执行上面的return，继续下面的内容
        //给找到的去数据赋值，authorEditDto是传进来修改的内容
        list.AuthorName = authorEditDto.AuthorName;
        list.Gender = authorEditDto.Gender;
        list.Password = authorEditDto.Password;
        //创建一个AuthrDto保存需要返回的数据（我这里返回的数据中将Password隐藏了，如果不需要隐藏，以下省略）
        var result = new AuthorDto
        {
            Id = guid,
            AuthorName = list.AuthorName,
            Gender = list.Gender
        };
        //将数据返回（我这里返回的数据中将Password隐藏了，如果不需要隐藏，以下返回改为list）
        return result;
    }
```