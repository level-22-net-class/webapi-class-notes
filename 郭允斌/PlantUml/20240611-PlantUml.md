@startuml
left to right direction
skinparam actorBorderColor Black
skinparam actorFontColor Black
skinparam entityBorderColor Black
skinparam entityBackgroundColor #D6EAF8
skinparam entityFontColor Black

actor 开发者
entity "创建项目" as 创建项目
entity "定义实体类" as 定义实体类
entity "创建 DbContext" as 创建DbContext
entity "配置数据模型" as 配置数据模型
entity "应用迁移" as 应用迁移
entity "生成数据库" as 生成数据库

开发者 --> 创建项目
创建项目 --> 定义实体类
定义实体类 --> 创建DbContext
创建DbContext --> 配置数据模型
配置数据模型 --> 应用迁移
应用迁移 --> 生成数据库
@enduml

