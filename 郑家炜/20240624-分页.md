### 分页
```
Controllers-> BlogsController.cs


    [HttpGet(Name = "abc")]
    public ActionResult<BlogDto> GetAll([FromQuery] BaseParams baseParams)
    {   // 第一个，考虑如何从前端传递分页数据到接口
        // 第二个，后面到底是如何完成分页这个操作的
        var count = _blogDb.Table.Count();
        var totalPages = Math.Ceiling(count * 1.0 / baseParams.PageSize);
        var list = _blogDb.GetAll().Skip((baseParams.PageIndex - 1) * baseParams.PageSize).Take(baseParams.PageSize).ToList();

        /*
        返回给请求端的页信息如下（猜测）：
            总共多少条数据
            第几页数据
            每页条目数
            共多少页


        */
        // 分页的数据信息
        var pagination = new
        {
            pageIndex = baseParams.PageIndex,
            pageSize = baseParams.PageSize,
            totalPages,
            totalCount = count,
            previousPageUrl = baseParams.PageIndex == 1 ? null : Url.Link("abc",
            new { pageIndex = baseParams.PageIndex - 1, pageSize = baseParams.PageSize }),
            nextPageUrl = baseParams.PageIndex < totalPages ? Url.Link("abc",
            new { pageIndex = baseParams.PageIndex + 1, pageSize = baseParams.PageSize }) : null
        };

        // 将分页数据信息附加到响应头
        Response.Headers.Append("X-Pagination", JsonSerializer.Serialize(pagination));
        return Ok(new { baseParams, list, abc = 11 * 1.0 / 10, xyz = Math.Ceiling(11 / (10 * 1.0)) });
    }
```

```
Params-> BaseParams.cs:

namespace Blog.Api.Params;

public class BaseParams
{
    private int _pageIndex;

    private int _pageSize;

    public int PageIndex
    {
        get
        {
            return RegualIndex(_pageIndex);
        }
        set
        {
            _pageIndex = RegualIndex(value);
        }
    }
    public int PageSize
    {
        get
        {
            return RegualSize(_pageSize);
        }
        set
        {
            _pageSize = RegualSize(value);
        }
    }

    private static int RegualIndex(int index)
    {
        int minIndex = 1;// 页码最小值
        return index <= 0 ? minIndex : index;
    }

    private static int RegualSize(int size)
    {
        int minSize = 10;// 页大小最小值
        int maxSize = 50;// 页大小最大值
        return size <= 0 ? minSize : (size > maxSize ? maxSize : size);
    }
}
```