# AutoMapper的使用
AutoMapper 是一个用于在 .NET 应用程序中映射对象的库。它可以帮助您简化将对象从一个层传递到另一个层（例如，从数据访问层传递到业务逻辑层或表示层）所需的样板代码。

以下是 AutoMapper 的使用步骤：

1.  安装 AutoMapper 包。可以使用 NuGet 包管理器在 Visual Studio 中安装它，或者在命令行中运行以下命令：

    Install-Package AutoMapper
    

2.  创建映射配置。在您的应用程序中，创建一个新的类，并将 AutoMapper 配置为映射两个类型。例如，假设您有以下两个类：

    public class SourceClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    
    public class DestinationClass
    {
        public int Id { get; set; }
        public string FullName { get; set; }
    }
    

您可以使用 AutoMapper 配置来映射这两个类型：

    using AutoMapper;
    
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<SourceClass, DestinationClass>()
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.Name));
        }
    }
    

在这个配置中，我们告诉 AutoMapper 如何将 `SourceClass` 的 `Name` 属性映射到 `DestinationClass` 的 `FullName` 属性。

3.  在您的应用程序中创建一个实例化的 `Mapper` 对象，并使用它来执行映射。例如：

    using AutoMapper;
    
    public class ExampleClass
    {
        private readonly IMapper _mapper;
    
        public ExampleClass(IMapper mapper)
        {
            _mapper = mapper;
        }
    
        public DestinationClass MapSourceToDestination(SourceClass source)
        {
            return _mapper.Map<DestinationClass>(source);
        }
    }
    

在这个示例中，我们注入 `IMapper` 对象，并使用它来执行映射。在构造函数中，可以使用 `Mapper.Initialize` 方法来配置 AutoMapper：

    public ExampleClass()
    {
        var config = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<MappingProfile>();
        });
    
        _mapper = config.CreateMapper();
    }
    

这样，您就可以使用 AutoMapper 在您的 .NET 应用程序中映射对象了。请注意，这只是一个简单的示例，AutoMapper 具有更多高级功能，例如可以映射集合、支持自定义映射函数等等。