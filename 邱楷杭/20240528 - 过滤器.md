# 过滤器
在 ASP.NET Core 中，过滤器（Filters）是一种强大的机制，用于处理在执行 Web 请求时发生的各种事件。过滤器可以在请求处理管道中的特定阶段执行代码，例如在动作方法执行前、后或者发生异常时。以下是关于 ASP.NET Core 中过滤器的一些知识点和例子：

### 过滤器的类型

ASP.NET Core 支持以下几种类型的过滤器：

1.  **授权过滤器（Authorization Filters）**
    
    *   最先执行，用于确定用户是否被授权访问某个资源。
    *   示例：
        
            public class CustomAuthorizationFilter : IAuthorizationFilter
            {
                public void OnAuthorization(AuthorizationFilterContext context)
                {
                    // 实现自定义授权逻辑
                }
            }
            
        
2.  **资源过滤器（Resource Filters）**
    
    *   在授权过滤器之后执行，但在其他类型的过滤器之前。
    *   可以用于缓存、短路请求处理等。
    *   示例：
        
            public class CustomResourceFilter : IResourceFilter
            {
                public void OnResourceExecuting(ResourceExecutingContext context)
                {
                    // 在资源执行前执行
                }
            
                public void OnResourceExecuted(ResourceExecutedContext context)
                {
                    // 在资源执行后执行
                }
            }
            
        
3.  **动作过滤器（Action Filters）**
    
    *   在动作方法执行前后执行，可以用于修改动作方法的输入或输出。
    *   示例：
        
            public class CustomActionFilter : IActionFilter
            {
                public void OnActionExecuting(ActionExecutingContext context)
                {
                    // 在动作方法执行前执行
                }
            
                public void OnActionExecuted(ActionExecutedContext context)
                {
                    // 在动作方法执行后执行
                }
            }
            
        
4.  **异常过滤器（Exception Filters）**
    
    *   用于捕获和处理在请求处理过程中发生的异常。
    *   示例：
        
            public class CustomExceptionFilter : IExceptionFilter
            {
                public void OnException(ExceptionContext context)
                {
                    // 处理异常
                }
            }
            
        
5.  **结果过滤器（Result Filters）**
    
    *   在动作方法的结果执行前后执行，可以用于修改动作结果的输出。
    *   示例：
        
            public class CustomResultFilter : IResultFilter
            {
                public void OnResultExecuting(ResultExecutingContext context)
                {
                    // 在结果执行前执行
                }
            
                public void OnResultExecuted(ResultExecutedContext context)
                {
                    // 在结果执行后执行
                }
            }
            
        
6.  **终结点过滤器（Endpoint Filters）**
    
    *   在 ASP.NET Core 3.0 及更高版本中引入，用于在路由到终结点前后执行代码。
    *   示例：
        
            public class CustomEndpointFilter : IEndpointFilter
            {
                public ValueTask<object> InvokeAsync(EndpointFilterInvocationContext context, EndpointFilterDelegate next)
                {
                    // 在终结点执行前/后执行
                    return next(context);
                }
            }
            
        

### 过滤器的应用

过滤器可以通过多种方式应用到控制器或动作方法上：

*   **全局应用**：在 `Startup.cs` 的 `ConfigureServices` 方法中注册为全局过滤器。
    
        services.AddMvc(options =>
        {
            options.Filters.Add<CustomActionFilter>();
        });
        
    
*   **控制器级别应用**：在控制器类上使用 `[ServiceFilter]` 或 `[TypeFilter]` 特性。
    
        [ServiceFilter(typeof(CustomActionFilter))]
        public class MyController : Controller
        {
            // ...
        }
        
    
*   **动作方法级别应用**：在动作方法上使用 `[ServiceFilter]` 或 `[TypeFilter]` 特性。
    
        public class MyController : Controller
        {
            [TypeFilter(typeof(CustomActionFilter))]
            public IActionResult MyAction()
            {
                // ...
            }
        }
        
    

### 过滤器的生命周期

过滤器的生命周期由依赖注入（DI）管理。如果过滤器需要依赖其他服务，可以通过构造函数注入。过滤器实例的生命周期通常与请求的生命周期相同，除非它们被注册为作用域或单例服务。

### 结论

过滤器是 ASP.NET Core 中一个非常灵活和强大的功能，它允许开发者在请求处理的各个阶段插入自定义逻辑。通过合理使用过滤器，可以实现诸如授权、日志记录、异常处理、缓存等功能，从而提高应用程序的健壮性和可维护性。