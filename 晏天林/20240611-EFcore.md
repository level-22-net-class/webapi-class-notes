## EFCcore
- 第一步：安装EFCcore核心包
    - dotnet add package Microsoft.EntityFrameworkCore
- 第二步：安装数据库驱动包，支持主流的数据库如MsSql
    - dotnet add package Microsoft.EntityFrameworkCore.SqlServer
- 第三步：安装数据库迁移工具包
    - dotnet add package Microsoft.EntityFrameworkCore.Design
- 第四步：定义实体类型
- 第五步：定义数据库上下文
    - 定义对应的数据库表DbSet<Author> Author
- 第六步：安装 -ef 工具
    - dotnet  tool install --global dotnet-ef
- 第七步：生成数据库迁移文件
    - dotnet ef migrations add InitialCreate
- 第八步：更新数据库
    - dotnet ef database update

```
@startmindmap cx
    *[#409EFF] EFCcore
        **[#79bbff] 安装EFCcore核心包
        ***_ Microsoft.EntityFrameworkCore
        **[#79bbff] 安装数据库驱动包，支持主流的数据库如MsSql,Mysql
        ***_ Microsoft.EntityFrameworkCore.SqlServer
        **[#79bbff] 安装数据库迁移工具包
        ***_ Microsoft.EntityFrameworkCore.Tools
        **[#79bbff] 定义数据库上下文
        ***_ 定义对应的数据库表DbSet<Author>Author
        **[#79bbff] 定义实体类型
        **[#79bbff] 安装 -ef 工具
        ***_ dotnet tool install --global dotnet-ef
        **[#79bbff] 生成迁移文件
        ***_ dotnet ef migrations add InitialCreate
        **[#79bbff] 更新数据库
        ***_ dotnet ef database update
@endmindmap
```
- 效果如图

 ![](../晏天林/img/list.png)

