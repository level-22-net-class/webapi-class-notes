@startuml

start
:安装EFCore核心包
Microsoft.EntityFrameworkCore;
:安装数据库驱动包
Microsoft.EntityFrameworkCore.SqlServer;
repeat :定义实体类型;
  :定义数据库上下文
需要定义对应的数据库表 DbSet Users;
  :生成迁移文件
依赖工具包：Microsft.EntityFrameworkCore.Design;
backward:可循环流程;
repeat while (将迁移文件更新到数据库)

stop

@enduml