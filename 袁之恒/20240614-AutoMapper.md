# AutoMapper 简介

AutoMapper 是一个开源的 .NET 库，它专门用于简化对象之间的属性映射过程。通过自动化这一任务，AutoMapper 显著减少了代码冗余，并大幅提升了开发效率。

## 主要特性

- **复杂映射配置**：支持在对象之间建立复杂的映射关系。
- **自动发现**：能够自动识别和配置映射关系，降低手动设置的需要。
- **Fluent 接口**：提供直观的 Fluent 接口，使映射配置过程更加清晰。
- **LINQ 集成**：可以在 LINQ 查询中直接使用投影映射。
- **批量映射支持**：允许同时映射多个对象。
- **DI 容器集成**：可以无缝集成到依赖注入（DI）容器中。
- **性能优异**：在保持易用性的同时，也注重性能优化。

### 安装与配置

在 Visual Studio 中，可以通过 NuGet 包管理器安装 AutoMapper。使用以下命令快速安装：

```powershell
Install-Package AutoMapper
```

安装后，在你的代码文件中引入 AutoMapper 命名空间：

```csharp
using AutoMapper;
```

### 示例：对象映射

这里提供一个基本的使用示例，演示如何将 `Person` 实体映射到 `PersonDto` 实体：

```csharp
public class Person
{
    public string Name { get; set; }
    public int Age { get; set; }
}

public class PersonDto
{
    public string Name { get; set; }
    public int Age { get; set; }
}

// 初始化 AutoMapper 并配置映射关系
Mapper.Initialize(cfg => cfg.CreateMap<Person, PersonDto>());

// 创建 Person 对象的实例
var person = new Person { Name = "张三", Age = 18 };

// 使用 AutoMapper 将 Person 对象映射到 PersonDto 对象
var personDto = Mapper.Map<PersonDto>(person);
```

在上述示例中，`Mapper.Initialize` 方法用于配置对象间的映射规则。通过 `CreateMap<TSource, TDestination>` 方法定义源对象类型 `TSource` 和目标对象类型 `TDestination` 之间的映射关系。

一旦映射配置完成，就可以使用 `Mapper.Map<TDestination>(TSource source)` 方法将源对象 `source` 映射到目标类型的新实例。