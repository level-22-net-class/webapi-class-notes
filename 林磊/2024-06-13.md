# AutoMapper 对象映射库
下载：`dotnet add package AutoMapper.Extensions.Microsoft.DependencyInjection`

作用：
1. 对象到对象的映射：简化了从一个对象类型到另一个对象类型的转换。
2. 集合的映射：可以自动映射集合中的对象，减少了手动迭代和映射的工作。
3. 可配置的映射规则：开发人员可以定义自定义的映射规则，以满足特定的需求。
4. 灵活的映射选项：AutoMapper提供了许多选项和配置，以满足各种映射需求。
   
使用命名空间：`using AutoMapper;`

将AutoMapper服务添加到依赖注入容器：`services.AddAutoMapper(typeof(Startup));`
  1. 会扫描Profile的派生类，将结果生成映射规则
  2. 将IMapper接口【用于完成映射操作的接口】添加到依赖注入容器
  3. typeof(Startup)：代表 Startup 类的类型
  4. AddAutoMapper 方法使用 Startup 类的类型来扫描应用程序的程序集，并自动发现和注册映射配置

## Helpers
创建Helpers文件夹，添加 XXXMappingProfile.cs

1. Profile派生类：定义映射对象，映射规则
2. `CreateMap<源,目标>()`：创建对象映射关系 
    1. 从数据库获取数据 <实体类为源，DTO为目标>
    2. 发送GET等请求  <DTO为源，实体类为目标>

**XXXMappingProfile.cs:**
```cs
using AutoMapper;
using LibraryAPI.Domain;
using LibraryAPI.Dto;

// Profile位于AutoMapper命名空间下
public class LibraryMappingProfile : Profile
{
  public LibraryMappingProfile()
  {
    // 将 Author 实体映射到 AuthorDto 数据传输对象。
    CreateMap<Author, AuthorDto>();
    CreateMap<Book, BookDto>();
    CreateMap<AuthorForCreationDto, Author>();
    CreateMap<BookForCreationDto, Book>();

    // ForMember为特定的属性自定义映射规则
    // 将调用Author对象的BirthDate属性的GetCurrentAge()方法来获取Author的年龄，并将其映射到AuthorDto对象的Age属性上。
    // CreateMap<Author,AuthorDto>().ForMember(x => x.Age, config => config.MapFrom(src => src.BirthDate.GetCurrentAge()));
  }
}
```
## Controller文件夹
1. 通过构造函数注入IMapper映射器，使用对象映射
2. 转换：`Map<目标>(源);`
```cs
public class AuthorController : ControllerBase
{
  // 
  public IMapper _mapper { get; }
  public AuthorController(IMapper mapper)
  {
    // 映射器：转化实体对象与DTO对象
    _mapper = mapper;
  }
  
  [HttpGet]
  public async Task<ActionResult<IEnumerable<AuthorDto>>> GetAuthorsAsync()
  {
    // 从数据库获取数据，实体对象
    var authors = await _repositoryWrapper.Author.GetAllAsync();
    
    // 进行对象映射，将获取到的作者实体列表 authors 映射为作者DTO
    var authorDtoList = _mapper.Map<IEnumerable<AuthorDto>>(authors);

    return authorDtoList.ToList();
  }
}
```
# C# XML Document插件
/// =>生成XML注释