什么是 Web API？
API 指的是应用程序编程接口（Application Programming Interface）。

Web API 是 Web 的应用程序编程接口。

浏览器 API 可以扩展 Web 浏览器的功能。

服务器 API 可以扩展 Web 服务器的功能。

浏览器 API
所有浏览器都有一组内置的 Web API 来支持复杂的操作，并帮助访问数据。

例如，Geolocation API 可以返回浏览器所在位置的坐标。

实例
获取用户所在位置的经纬度：

const myElement = document.getElementById("demo");

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else {
    myElement.innerHTML = "Geolocation is not supported by this browser.";
  }
}

function showPosition(position) {
  myElement.innerHTML = "Latitude: " + position.coords.latitude +
  "<br>Longitude: " + position.coords.longitude;
}