分层的优点：

1、层次分明，各司其职：每层都有各自的责任，各层级相互独立，上层不需要知道下层的内部实现，上层的改动不会影响下一层。

2、易于开发，便于调试：分层结构使得开发人员可以专注于谋一层进行开发，进行调试时，可以针对每一层进行单独调试。

3、促进标准，移植复用：可替换任意一层，如当前数据层读数据库，可替换为读文件数据，取网络数据。亦可移植到其他项目。

4、。。。。。。

分层的缺点：

1、降低性能：一个简单的数据呈现，需逐层返回且中间的数据转换等都耗费时间。

2、级联修改：在呈现层中增加一个功能，为保证其设计符合分层式结构，可能需要在相应的业务逻辑层和数据访问层中都增加相应的代码。 

3、。。。。。。