## vue

<script setup lang="ts">
// npm run dev 运行
// npm add axios
import axios from "axios";
import { onMounted, reactive, ref } from "vue";

let blogData = reactive([]);
onMounted(async () => {
  rednerData();
})
async function btnDel(id) {
  await axios.delete(`http://localhost:5194/api/blog/${id}`);
  rednerData();
}

async function rednerData() {
  let blog = await axios.get("http://localhost:1234/api/blog");
  blogData.splice(0, blogData.length);
  Object.assign(blogData, blog.data);
}
</script>

<template>
  <table>
    <thead>
      <tr>
        <th>序号</th>
        <th>标题</th>
        <th>作者</th>
        <th>标签</th>
        <th>操作</th>
      </tr>
    </thead>
    <tbody>
      <tr v-for="(item, index) in blogData" :key="item.id">
        <td>{{ index + 1 }}</td>
        <td>{{ item.title }}</td>
        <td>{{ item.author }}</td>
        <td>{{ item.label }}</td>
        <td>
          <button class="btn" @click="btnDel(item.id)">删除</button>
        </td>
      </tr>
    </tbody>
  </table>

</template>

<style scoped>
.logo {
  height: 6em;
  padding: 1.5em;
  will-change: filter;
  transition: filter 300ms;
}

.logo:hover {
  filter: drop-shadow(0 0 2em #646cffaa);
}

.logo.vue:hover {
  filter: drop-shadow(0 0 2em #42b883aa);
}
</style>
