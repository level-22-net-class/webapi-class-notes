### 一、NET Core简介
.NET Core是由微软开发的跨平台、开源的开发框架，适用于构建现代、高性能的应用程序
- 跨平台：.NET Core支持在Windows、macOS和Linux上运行，使得开发者可以在不同的操作系统上进行开发和部署
- 开源：.NET Core是完全开源的，代码托管在GitHub上。任何人都可以查看、贡献代码并帮助改进该框架
- 高性能：.NET Core被设计为高性能的，特别适用于需要高吞吐量和低延迟的应用，如微服务和云计算应用
- 模块化：.NET Core采用模块化设计，开发者可以按需引入所需的库和功能，从而减小应用程序的大小并提高性能
- CLI工具：.NET Core提供了强大的命令行工具（CLI），支持创建、构建、运行和发布应用程序
- 统一编程模型：.NET Core提供了一个统一的编程模型，支持多种类型的应用程序，包括控制台应用、Web应用（通过ASP.NET Core）、桌面应用（通过Windows Forms和WPF）、微服务、云服务等
- 兼容性：.NET Core可以与其他.NET框架（如.NET Framework和Mono）兼容，并且提供了迁移工具，帮助开发者从旧版本迁移到.NET Core。
- 依赖注入：.NET Core内置依赖注入支持，简化了对象的创建和管理，提升了代码的可测试性和可维护性
另外一个最常用的.NET平台是.NETFramework，两者是不同的.NET平台，主要的区别是：前者跨平台，后者仅支持Windows
### 二、.NET Standard简介
.NET Standard是一个旨在提供一组统一API规范的标准，确保不同.NET实现（如 .NET Framework、.NET Core 和 Xamarin）之间的代码兼容性和共享性。具有以下特点：
- 统一API规范
- 代码共享：开发者可以编写能够在多个.NET平台上运行的库和组件，从而提高代码重用性
- 版本管理：.NET Standard有多个版本，每个版本包含一组特定的API
- 跨平台支持
### 三、ASP.NET Core简介
ASP.NET Core是一个开源、跨平台的高性能Web框架，用于构建现代、云端优化的Web应用和服务。具有以下特点：
- 跨平台
- 开源
- 高性能
- 模块化和灵活性：采用模块化设计，开发者可以根据需要选择和配置中间件组件，构建高度定制化的请求处理管道- 统一的编程模型
- 依赖注入：内置了依赖注入（DI）框架，简化了应用的配置和管理，提高了代码的可维护性和可测试性
- 现代开发工具
- 云优化
- 兼容性和迁移
### 四、设置开发环境
使用.NETCoreCLI可以创建一个.NETCore应用
~~~js
dotnet new console -o HelloConsole
dotnet new api -o HelloApi
dotnet new classlib -o HelloClassLibrary
// -o 表示要创建的位置
~~~
除此之外，.NETCoreCLI还有其他命令：

### 五、创建第一个API项目
~~~js
// 恢复项目中的NuGet包依赖项：
dotnet restore

// 编译项目：
dotnet build

// 运行项目：
dotnet run

// 测试项目：
dotnet test

// 发布项目：
dotnet publish -c Release -o <output-folder>
-c Release指定发布配置为Release模式，-o指定输出文件夹

// 添加包：
dotnet add package <package-name>

// 添加项目引用：
dotnet add reference <project-path>

// 移除引用：
dotnet remove

// 清理项目：
dotnet clean
~~~
### 六、创建第一个API项目
#### 1.使用vsCode
在终端输入命令创建API项目
~~~js
dotnet new webapi -n HelloApi
~~~
#### 2.使用VisualStudio
1. 选择“ 文件 ”→“ 新建 ”→“ 项目 ”
2. 在弹出的新建项目对话框中选择左侧的“VisualC#”，继续选择“.NET Core”；在右侧选择“ASP.NET Core Web应用程序”，输入新项目名称
3. 选择“API”模板，并且选择“不进行身份验证”
#### 3.运行项目
使用命令【dotnet run】运行项目后，打开浏览器会返回404，需要将/weatherforecast添加到URL中，就会返回示例JSON代码
### 七、关于Swagger
将/swagger附加到URL中就会显示Swagger页面，该页面会显示：
- 用于测试 WeatherForecast API 的 Curl 命令
- 用于测试 WeatherForecast API 的 URL
- 响应代码、正文和标头
- 包含媒体类型、示例值和架构的下拉列表框
Swagger 用于为 Web API 生成有用的文档和帮助页面

