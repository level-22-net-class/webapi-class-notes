## 创建分页列表类，获取分页相关的属性和方法
~~~c#
public class PagedList<T> : List<T>
{
    public int PageIndex { get; private set; } //页码数
    public int PageSize { get; private set; } //每页条数
    public int TotalCount { get; private set; } //总条数
    public int TotalPages { get; private set; } //总页数

    public PagedList(List<T> items, int count, int pageIndex, int pageSize)
    {
        /*
          items: 当前页包含的记录列表。
          count: 总记录数。
          pageIndex: 当前页的索引。
          pageSize: 每页记录数。
        */
        TotalCount = count;
        PageSize = pageSize;
        PageIndex = pageIndex;
        TotalPages = (int)Math.Ceiling(count / (double)pageSize);
        // 使用 AddRange 方法将 items 添加到当前 PagedList 实例中
        AddRange(items);
    }
    // 判断是否有上一页
    public bool HasPreviousPage => PageIndex > 1;
    // 判断是否有下一页
    public bool HasNextPage => PageIndex < TotalPages;

    public static PagedList<T> Create(IQueryable<T> source, int pageIndex, int pageSize)
    {
        /*
          source: 数据源，类型为 IQueryable<T>
          count：总条数
        */
        var count = source.Count();
        // 使用 Skip 和 Take 方法从数据源获取当前页的记录，并转换为列表 items
        var items = source.Skip((pageIndex - 1) * pageSize)
                          .Take(pageSize).ToList();
        // 创建并返回一个新的 PagedList<T> 实例
        return new PagedList<T>(items, count, pageIndex, pageSize);
    }
}
~~~
## 创建静态辅助类，用于创建分页相关的响应数据
~~~c#
using Microsoft.AspNetCore.Mvc;
namespace Blog.Api.Helpers;
public static class PaginationHelper
{
    public static object CreatePaginationResponse<T>(PagedList<T> pagedList, IUrlHelper urlHelper, string routeName)
    {
        return new
        {
            pageIndex = pagedList.PageIndex,
            pageSize = pagedList.PageSize,
            totalPages = pagedList.TotalPages,
            totalCount = pagedList.TotalCount,
            // 上一页的 URL，如果存在上一页则使用 urlHelper 生成对应的链接，否则为 null
            previousPageUrl = pagedList.HasPreviousPage ? urlHelper.Link(routeName,
                new { pageIndex = pagedList.PageIndex - 1, pageSize = pagedList.PageSize }) : null,
            // ：下一页的 URL，如果存在下一页则使用 urlHelper 生成对应的链接，否则为 null
            nextPageUrl = pagedList.HasNextPage ? urlHelper.Link(routeName,
                new { pageIndex = pagedList.PageIndex + 1, pageSize = pagedList.PageSize }) : null
        };
    }
}
~~~
## get请求处理
~~~c#
[HttpGet(Name = "GetAllBlogs")]
 public IActionResult GetAll([FromQuery] BaseParameters baseParameters)
{
  // 从 _blogRep 中获取所有博客文章，并将其转换为 IQueryable 对象
    var blog = _blogRep.GetAll().AsQueryable();
  // 调用 Create 方法，使用获取到的博客文章数据源和传入的分页参数 baseParameters.PageIndex 和 baseParameters.PageSize 创建一个分页列表 pagedBlogs
    var pagedBlogs = PagedList<Blogs>.Create(blog, baseParameters.PageIndex, baseParameters.PageSize);
  // 创建分页相关的响应数据，并将其添加到响应头中 
    var pagination = PaginationHelper.CreatePaginationResponse(pagedBlogs, Url, "GetAllBlogs");
    Response.Headers.Append("X-Pagination", JsonSerializer.Serialize(pagination));

    var blogDtoList = _mapper.Map<List<BlogDto>>(pagedBlogs);
    return Ok(blogDtoList);
}
~~~