# ABP（ASP .NET Boilerplate Project）概述

ABP是基于领域驱动设计（DDD）的经典分层架构思想的项目框架。它包含了领域驱动设计中的多个概念，并提供了一套完整的技术栈，用于构建现代化的Web应用程序。

## 领域驱动设计概念
ABP基于领域驱动设计思想，包含以下概念：
- 领域
- 子域
- 聚合
- 聚合根
- 领域模型
- 值对象
- 通用仓储接口
- 领域模型仓储接口
- 领域服务接口
- 应用服务接口
- API服务接口
- 领域事件
- 大脑风暴
- 事件风暴

## 具体层分析
ABP的架构包含以下具体层次：
1. 通用仓储接口：提供通用的仓储接口，其实现放在基础设施层或独立的ORM工具层。
2. 领域模型仓储接口：定义领域独有的业务，其实现放在领域层。
3. 领域服务接口：处理涉及多个领域实体的业务和操作，放在领域服务层。
4. 应用服务接口：对领域模型业务和领域服务业务进行编排的薄层。
5. API服务接口：接受参数，进行数据验证等操作。
6. 单元测试层：提供应用层对象的模拟测试，使用Entity Framework的内存数据库进行测试。

## 采用的技术
### 服务端
- ASP.NET MVC5、Web API2、C#
- DDD领域驱动设计
- Castle Windsor（依赖注入容器）
- Entity Framework6/Entity Framework Core/NHibernate、数据迁移
- Log4Net（日志记录）
- AutoMapper（实现DTO类与实体类的双向自动转换）

### 客户端
- Bootstrap
- Less
- Angular
- Vue
- jQuery
- Modernizr
- 其他JS库

以上是关于ABP项目的概述和采用的技术。