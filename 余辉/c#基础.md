### c#基础
1. dotnet new console -n MyConsoleApp  // 创建一个控制台
2. dotnet run  // 运行c#
```c#
namespace c1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
        } 
    }
}
// stativ 表示一个静态修饰符，表示这个是静态方法，Main() 是程序的入口，方法必须是静态方法
// void 表示没有返回值
```

### 注释
1. // 单行
2. /**
* 
* 文档注释，多行的时候使用
*/
3. /// <summary>
/// 对类属性方法经行说明
/// </summary>

### 快捷键
1. 自动对齐格式 ： Ctrl +K+D
2. 撤销： Ctrl +Z, 复制： Ctrl +C, 粘贴： Ctrl +V
3. 保存： Ctrl +S
4. 弹出智能提示： Ctrl +J
5. 注释所选代码 ： Ctrl +K+C
6. 取消所选注释： Ctrl +K +U
7. 查看帮助文档： F1
8. 调试运行： F5
9. 运行（不调试）: Ctrl + F5
10. 折叠代码：#region #EndRegion

### 变量-数据类型
1. string int double float bool

### 变量-声明
1. 显示声明变量
```c#
int a = 10;
string b = "nihao";
bool c = true
```
2. 使用var自动推断变量类型
```c#
var a = 10;
```

### 创建数组-对象-二维数组
1. 创建数组
```c#
//提前定义好数组的数量
int[] numbers = new int[5];
//创建一个新数组并初始化
int[] numbers = new int[]{1,2,3};
//省略new关键字（仅在声明时候可用）
int[] numbers = {1,2,3,3};
```
2. 创建对象
1. 定义一个类并实现构造函数
2. 使用new关键字调用构造函数创建对象
```c#
// 定义了一个对象并存入值
namespace c1
{
    //定义了一个类
    public class Student
    {
        public string? Name {get;set;}
        public int Grade {get;set;}

        // 构造函数
        public Student(string name,int grade)
        {
            Name = name;
            Grade = grade;
        }
    }
    public class Program
    {
        public static void Main(string[] args)
        {
           // 创建初始化学生对象数组
           Student[] students = new Student[]
           {
            new Student("aa",1),
            new Student("bb",2)
           };
           // 遍历输出学生信息
           foreach (Student student in students)
           {
            System.Console.WriteLine($"Name:{student.Name},Grade:{student.Grade}");
           }
        } 
    }
}

```
3. 二位数组
```c#
namespace c1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // 声明和初始化一个数组
            int[,] matrix = new int[,]{{1,2,3},{4,5,6}};

            System.Console.WriteLine(matrix[1,0]+matrix[0,1]);
        }
       
    }
}
```

### 运算符
1. + - == -- * /(除法) %(求余) = 
### 关系运算符,返回的是bool类型
1. ==  !=  >  <  >=  <=
### 逻辑运算符
1. &&(都要满足)  ||(只要满足一个)  !(加在前面结果取反)
### 位运算  - 偏难而且有替代

### 赋值运算符
1. ==  += -=  *=  /=  %=  $=  ^=  |=

### Console 类 控制台
1. Console.Write  控制台输出不换行
2. .WriteLine  像控制台输出后换行(常用)
3. .Read  从控制台读取一个字符
4. .ReadLine  从控制台读取一行字符(常用)

### 转义字符  一般配合Console
1. \n 换行  \r 回车 \t 制表符  \f 换页符  \b 退格  \a  响铃  
\e escape（ASCII中的escape 字符）
\007 任何八进制值（这里是，007=bell(响铃)）
\x7f 任何十六进制值（这里是，007=bell）
\cC 一个控制符（这里是：Ctrl+c）
\ 反斜线
" 双引号
\l 下个字符小写
\L 接着的字符均小写直到\E
\u 下个字符大写
\U 接着的字符均大写直到\E
\Q 在 non-word 字符前加上(自动加转义符号)，直到\E
\E 结束\L,\E和\Q
\0 空格

### 类型转换
1. 方法参考
Convert.ToInt32() : 转换为int
Convert.ToInt64() : 转换为long
Convert.ToString() : 转换为String
Convert.ToByte() : 转换为byte
Convert.ToChar() : 转换为Char字符
```c#
namespace c1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // 1， 隐式转换
          int a = 10;
          long bigNum = a; // 隐式转化为long

          double str = a;  // 隐式转换为double
          // 数字进行隐式转换时，注意不要溢出
          string stringrstr = "10" + a; // 隐式转化为string
          // 2. 强制转换，会丢失精度(最常用的是显示转换)
          double d2 = 123.45;
          int i2 = (int)d2;

          object obj = "nihao";
          string stras = obj as string;

          // 3. 自定义转换
        } 
    }
}
```

### 函数
1. 基本定义
[访问修饰符] [返回类型] 方法名([参数列表])
{
    // 方法体
}
```c#
// See https://aka.ms/new-console-template for more information
namespace c1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // 调用
           Greet();
           Greet("aa");
           int sum = Greet(3);
           // 这种写法就是指定名字，这样可以不按照默认顺序
           int sum1 = Greet(id:3);
           // ref 让栈里面的内容便形成像引用类型一样的新式，从而使方法可以改变外面定义的值
           int a = 5;
           Double(ref a);
           // out 参数用于返回多个值

            // 用于数量可变的参数
            int sub = sum(1,2,3);
        } 
        // 定义了一个基本防方法
        public static void Greet()
        {
            System.Console.WriteLine("hello world");
        }
        public static void Greet(string name)
        {
            System.Console.WriteLine($"我是重构的hello word{name}");
        }
        // 如果形参里面有提前赋值，代表默认值的意思
        public static int Greet(int id = 5)
        {
            System.Console.WriteLine($"我是重构的hello word{id}");
            return id;
        }
        public static void Double(ref int a)
        {
            a++;
        }
        public static void sum(params int[] num)
        {

        }
    }
}
```

### 选择结构 - 三元运算符
```c#
namespace c1
{
    public class Program
    {
        public static void Main(string[] args)
        {
           int num = 7;
           if (true)
           {
            
           }else
           {

           }
            // 从上往下，判断case值是否相等，相等执行里面的代码，如果都没有匹配的值，就执行default函数里面的内容
           switch(num)
           {
            case 1:
                System.Console.WriteLine();
                break;
            case 2:
                System.Console.WriteLine();
                break;
            default:
                System.Console.WriteLine();
                break;
           }
        } 
        // 三元运算符
       bool aa = num == 7 ? true:false;
    }
}
```

### 循环
```c#
// See https://aka.ms/new-console-template for more information
namespace c1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // break 用于立即退出循环  continue 用于跳过本次循环
            // 最全能，效率也高
          for (int i = 0; i < 10; i++)
          {
            System.Console.WriteLine("hello word");
          }
            // 遍历可迭代对象
          int[] numbers = {1,2,3,4};
          foreach (var item in numbers)
          {
            System.Console.WriteLine(item);
          }
            // while
          int i= 0;
          while (i<5)
          {
            System.Console.WriteLine(i);
            i++;
          }
          // do-while  区别就是，可以至少执行一次
          do
          {
            System.Console.WriteLine(i);
            i++;
          } while (i<5);
        } 
       
    }
}
```

### 异常捕获和处理
1. 在初学阶段用的少，了解就好
```c#
using System;

public class Program
{
    public static void Main()
    {
        try
        {
            int[] numbers = { 1, 2, 3 };
            Console.WriteLine(numbers[4]); // 访问数组越界，会抛出 IndexOutOfRangeException 异常
        }
        catch (IndexOutOfRangeException ex)
        {
            Console.WriteLine($"Caught IndexOutOfRangeException: {ex.Message}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Caught Exception: {ex.Message}");
        }
        finally
        {
            Console.WriteLine("Finally block is always executed.");
        }
    }
}
```
### 定义类对类的一个说明
```c#
namespace c1
{
    public class class1
    {
        // 字段  字段是私有的
        private string name;
        // 构造函数
        public class1 (string name)
        {
            // 这个this指向本地，也可以说class1
            this.name =name;
        }
        // 方法
        public void text()
        {
            System.Console.WriteLine($"{name}");
        }
        // 属性
        public string Name {get{return name;}set{name=value;}}
        public class1 ()
        {
            // 利用类模板创建一个对象
            class1 class11 = new class1("aa");

            // 调用里面的方法
            class11.text();

            // 使用属性设置获取指端值,赋值就是设置，直接写就是获取class11.Name
            class11.Name = "hello word";

        }
    }
}
```