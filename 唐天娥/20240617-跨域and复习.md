### 跨域
- 解决跨域其中的一种方法，采用后端解决的办法
```js
services.AddDefaultPolicy(policy=>{
    policy.AllowAnyOrigin();
});
```

### 新建一个webApi,梳理步骤
- 1.新建一个项目——webapi;
    - dotnet new webapi - Blog.Api

- 2.打碎重组，转换为控制器玩法

- 3.注意写上Startup

- 4.编写通用仓储接口:EFCore.Dapper.Freesql(使用sql数据库)

- 5.编写相应的接口实现类，此处采用的ORM是EFCore
    - dotnet add package Microsoft.EntityFrameworkCore
    - dotnet add package Microsoft.EntityFrameworkCore.sqlServer

- 6.根据依赖注入的要求，在Startup,注册通用仓储接口的容器(含实体类)

- 7.根据业务需要和设计，定义实体类型(可以配置实体类型),定义对应数据库上下文，生成迁移文件到数据库
    - dotnet add package Microsoft.EntityFrameworkCore.Design
    
- 8.编写对应的控制器，实现Restfull风格的api
