## 关于项目设计
### 一.主体结构
    - Restfull风格的webapi
    - Asp.net core 8.0
    - 采用传统控制器模式

### 二.模型设计
- 作者 Authors
    - 姓名 AthorName
    - 性别 Gender
    - 出生年月 Birthday
- 图书 BookName
    - 出版社 Publiser
    - 作者 AuthorId
    - 价格 Price

### 三.内存型数据-自定义的一个集合类

### 四.创储系统-准备用来接入数据库的一套接口
- 通用型仓储接口
- 个性化的仓储接口
    - 作者的 增删改查
    - 图书的 增删改查

### 利用第一步主体结构，具体实现Restfull风格关于作者，关于图书的CRUD
- 作者
    - 获取作者列表 Get /api/authors
    - 获取指定作者 Get /api/authors/88
    - 新增作者列表 post /api/authors
    - 修改作者列表 put /api/authors/4545
    - 删除作者列表 deete /api/authors/78
    
- 图书
    - 获取书本列表 Get /api/authors
    - 获取指定书本 Get /api/authors/88
    - 新增书本列表 post /api/authors
    - 修改书本列表 put /api/authors/4545
    - 删除书本列表 deete /api/authors/78