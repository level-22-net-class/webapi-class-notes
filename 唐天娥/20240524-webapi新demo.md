## program.cs
```js
namespace Admin2024.Api;

public static class Program
{
    public static void Main(string[] args)
    {
        CreateWebHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateWebHostBuilder(string[] args)
    {
        return Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(builder=>{
            builder.UseStartup<Startup>();
        });
    }
}
```
## Startup.cs
```js

namespace Admin2024.Api;

public class Startup
{
    public void Configure(IApplicationBuilder app)
        {
            app.UseRouting();
            app.UseEndpoints(enPoints=>{
                enPoints.MapControllers();
                });
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
        }

}
    
```

## BlogsControllers.cs
```js
using Microsoft.AspNetCore.Mvc;

namespace Admin2024.Api;

[Route("[controller]")]
public class BlogsController : ControllerBase
{
    public IActionResult Index()
    {
        return Ok("呵呵哈哈哈或");
    }
}

```
