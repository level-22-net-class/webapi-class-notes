### 分页
- 一.考虑如何从前端传递分页数据到接口
- 二.后面到底是如何完成分页这个操作的
#### BlogsController.cs
```js
[HttpGet]
    public ActionResult<BlogDto> GetById(int pageIndex,int pageSize)
    {
        var count=_blogDb.Table.Count();
        var list=_blogDb.GetAll().Skip((pageIndex-1)*pageSize).Take(pageSize).ToList;

    // 返回给请求端的信息如下(猜测)
    // 总共多少数据.
    // 第几页数据
    // 每页条目数
    // 共多少页

    // 分页的数据信息
        var pagination = new
        {
            pageIndex = baseParams.PageIndex,
            pageSize = baseParams.PageSize,
            totalPages,
            totalCount = count,
            previousPageUrl = baseParams.PageIndex == 1 ? null : Url.Link("abc",
            new { pageIndex = baseParams.PageIndex - 1, pageSize = baseParams.PageSize }),
            nextPageUrl = baseParams.PageIndex < totalPages ? Url.Link("abc",
            new { pageIndex = baseParams.PageIndex + 1, pageSize = baseParams.PageSize }) : null
        };

        // 将分页数据信息附加到响应头
        Response.Headers.Append("X-Pagination", JsonSerializer.Serialize(pagination));
        return Ok(new { baseParams, list, abc = 11 * 1.0 / 10, xyz = Math.Ceiling(11 / (10 * 1.0)) });
    }


```
#### BaseParams.cs
```js
namespace Blog.Api.Params;
public class Params{
    private int _PageIndex;
    private int _PageSize;
    private int pageIndex{
        get{
            return _pageIndex<0?1:_pageIndex;
        }
        private set{
            _PageIndex=value<=0?1:value;
        }
    }

    public int pageSize{
        get{
            return _PageSize;
        }
        private set{
            _pageSize=value<=0?(value>50?50:value);
        }
    }
}
```