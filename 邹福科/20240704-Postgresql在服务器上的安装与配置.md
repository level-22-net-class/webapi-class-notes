# Postgresql在服务器上的安装与配置
## 1. 域名解析
- 阿里云中点控制台，选择域名
- 在域名列表中选择需要解析的域名，点击解析
- 添加记录，然后填入服务器公网IP，公网IP在云服务器ECS->实例

## 2. 开启SSH服务(可选)
1. 安装SSH
   ```
   sudo apt-get install ssh
   ```
2. 配置允许root用户远程登录
   - 修改/etc/ssh/sshd_config文件
  ```
  vim /etc/ssh/sshd_config

  <!-- 找到该行，按i进入编辑，修改如下，完成后esc退出编辑模式，输入:wq保存并退出 -->
  #PermitRootLogin prohibit-password
  PermitRootLogin yes
  ```

3. 重启服务
   ```
   sudo service ssh restart
   ```

## 3. 远程连接服务器

**可通过阿里云直接远程连接，也可以在任意终端工具连接(例如Tabby、Windows PowerShell等)**
**通过终端工具连接需要输入命令`ssh root@域名`，然后输入密码进入**

## 4. 安装和启动PostgreSQL

- 使用 apt update 命令获取包的最新版本
  ```
  $ sudo apt update
  ```
- 安装PostgreSQL
  ```
  $ sudo apt install postgresql -y
  ```
- 启动PostgreSQL
  ```
  <!-- 启动 -->
  $ sudo systemctl start postgresql

  <!-- 验证 PostgreSQL 服务是否启动并运行 -->
  $ sudo systemctl status postgresql
  ```

## 5. 配置 PostgreSQL 允许远程连接
- 编辑postgresql.conf文件
  ```
  vim /etc/postgresql/(版本号)/main/postgresql.conf
  ```
  **找到listen_adresses行，去掉前面#注释，将localhost改为*,表示 PostgreSQL 将监听所有可用的网络接口。具体来说，这意味着 PostgreSQL 将会监听服务器上所有网络接口的连接请求，需要注意的是，开放所有网络接口可能会增加安全风险，因此在配置时需要注意网络安全性，确保采取适当的安全措施（例如防火墙、访问控制等）以保护数据库的安全。**

- 编辑 pg hba.conf 文件的 IPv4 本地连接部分
  ```
  vim /etc/postgresql/(版本号)/main/pg_hba.conf
  ```

  **在IPv4中添加一行`host       all     all     0.0.0.0/0       md5`**
  **表示允许来自任意 IP 地址（0.0.0.0/0）的所有用户（all）连接到所有的数据库（all），并通过md5验证方式进行身份验证**

- 重启PostgreSQL服务
  ```
  sudo systemctl restart postgresql
  ```

- 防火墙中添加允许5432端口
  ```
  sudo ufw allow 5432/tcp
  ```

## 6. 阿里云中安全组添加入口访问规则
- 阿里云中点控制台，选择云服务器ECS
- 网络与安全中选择安全组，创建安全组
- 可快速添加也可以手动添加，端口范围选择PostgreSQL(5432)