## Startup类与依赖注入

### Startup类

IWebHostBuilder接口有多个扩展方法，其中有一个很重要的是UseStarup方法，它主要向应用程序提供用于配置启动的类，而指定的这个类应具有以下两个方法：

ConfigureServices：用于向ASP.NET Core的依赖注入容器添加服务

Configure：用于添加中间件，配置请求管道

这两个方法都会在运行时被调用，且在应用程序的整个生命周期内，只执行一次。其中ConfigureServices方法是可选的，而Configure方法则是必选的。在程序启动时，它会执行ConfigureServices 方法(如果有)，将指定的服务放入应用程序的依赖注入容器中，然后再执行Configure方法，向请求管道中添加中间件

ConfigureServices方法有一个IServiceCollection类型的参数，使用它能够将应用程序级别的服务注册到ASP.NET Core默认的依赖注入容器中。Configure方法默认包含一个IApplicationBuilder类型的参数，通过它可以添加一个或多个中间件，所有添加的中间件将会对传入的HTTP请求进行处理，并将处理后的结果返回为发起请求的客户端

当在ConfigureServices方法中向依赖注入容器添加了服务后，在后面的Configure 方法中就可以通过参数将需要的服务注入进来，如上面的Configure方法中INoteRepository 类型的参数

在配置启动时，除了使用IWebHostBuilder的UseStartup方法引用Startup类外，还可以接使用IWebHostBuilder的ConfigureService和 Configure 两个扩展方法以内联的方式分别实Startup类中对应的两个方法

### 依赖注入

ASP.NET Core框架内部集成了自身的依赖注入容器。相比第三方依赖注入容器，它自带的依赖注入容器并不具备第三方容器所具有的高级功能,但它仍然是功能强大、简单，而且容易使用，此外，它的效率要更高

在ASP.NET Core中，所有被放入依赖注入容器的类型或组件称为服务。容器中的服务有两种类型:第一种是框架服务,它们是 ASP.NET Core框架的组成部分,如IApplicationBuilder、IHostingEnvironment和ILoggerFactory等;另一种是应用服务，所有由用户放到容器中的服务都属于这一类

为了能够在程序中使用服务，首先需要向容器添加服务,然后通过构造函数以注入的方式注入所需要的类中。若要添加服务，则需要使用Startup类的ConfigureServices方法，该方法有一个IServiceCollection类型的参数，它位于Microsoft.Extensions.DependencyInjection 命名空间下

ServiceDescriptor类描述一个服务和它的实现，以及其生命周期，正如上例中的构造函数所表明的，前两个参数分别是接口及其实现的类型，而第3个参数则是指明它的生命周期

在ASP.NET Core内置的依赖注入容器中，服务的生命周期有如下3种类型：

Singleton:容器会创建并共享服务的单例，且一直会存在于应用程序的整个生命周期内

Transient:每次服务被请求时，总会创建新实例

Scoped：在每一次请求时会创建服务的新实例，并在这个请求内一直共享这个实例当每次在容器中添加服务时，都需要指明其生命周期类型。当服务的生命周期结束时，它就会被销毁

### 小练习

#### 抽离

在Program.cs里写：

```
var builder = WebApplication.CreateBuilder(args);

var app = builder.Build();

App.UserRoute(app);

app.Run();
```

在Program.cs的同级层的文件App.cs文件里写：

```
public static class App
{
    public static void UserRoute(WebApplication app)
    {
        
        app.MapGet("/api/user1",async(ctx)=>
        {
            await ctx.Response.WriteAsync("user1");
        });
        app.MapGet("/api/user2",async(ctx)=>
        {
            await ctx.Response.WriteAsync("user2");
        });
        app.MapGet("/api/user3",async(ctx)=>
        {
            await ctx.Response.WriteAsync("user3");
        });
        app.MapGet("/api/user4",async(ctx)=>
        {
            await ctx.Response.WriteAsync("user4");
        });
        app.MapGet("/api/user5",async(ctx)=>
        {
            await ctx.Response.WriteAsync("user5");
        });
    }
};
```

在api.http中测试：

```
### 
GET http://localhost:5081/api/user5 HTTP/1.1
```