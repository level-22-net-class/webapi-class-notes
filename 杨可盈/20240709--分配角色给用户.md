#### 分配角色给用户
```
using Admin2024.Domain.Entity.System;
using Admin2024.Domain.Interface;

namespace Admin2024.Domain.DomainService;

public class AppUserDomainService : IAppUserDomainService
{
    private readonly IRepository<AppUser> _appUserRepository;
    private readonly IRepository<AppRole> _appRoleRepository;
    private readonly IRepository<AppUserRole> _appUserRoleRepository;

    public AppUserDomainService(IRepository<AppUser> appUserRepository,
    IRepository<AppUserRole> appUserRoleRepository, IRepository<AppRole> appRoleRepository)
    {
        _appUserRepository = appUserRepository;
        _appRoleRepository = appRoleRepository;
        _appUserRoleRepository = appUserRoleRepository;
    }

    // 给用户分配角色
    public async Task<AppUserRole?> AssigmRole(Guid appUserId, Guid appRoleId)
    {
        // 1.需要对id对应的实例存在不存在做验证
        // 2.如果都存在，需要对id对应的领域对象做进一步验证，如状态验证和业务验证
        var user = await _appUserRepository.GetByIdAsync(appUserId);
        var role = await _appRoleRepository.GetByIdAsync(appRoleId);
        // id对应的用户和角色都存在的情况下
        if (user != null && role != null)
        {
            // 业务要求：用户没有被删除，也没有被禁用
            // 业务要求：角色没有被删除，也没有被禁用
            if (user.IsDeleted != true && user.IsActived == true
            && role.IsDeleted != true && role.IsActived == true)
            {
                var userRole = new AppUserRole { AppUserId = appUserId, AppRoleId = appRoleId };
                var res = await _appUserRoleRepository.CreateAsync(userRole);
                return res;
            }

            return null;
        }

        return null;

    }

    public async Task<AppUser?> Create(string username, string password, string confirmPassword)
    {
        // 先用用户名去查找数据库有无相当用户名的记录，如果找到，说明用户名重复了
        var user = _appUserRepository.Table.FirstOrDefault(x => x.Username == username);

        if (user == null && password == confirmPassword)
        {
            var appUser = await _appUserRepository.CreateAsync(new AppUser
            {
                Username = username,
                Password = password,
                Salt = "可盐可甜"
            });
            return appUser;
        }
        return null;
    }

    public void HasPemission(AppPermission appPermission)
    {
        throw new NotImplementedException();
    }

    public string? Login(string username, string password)
    {
        var user = _appUserRepository.Table.FirstOrDefault(x => x.Username == username);

        if (user != null && user.Password == password)
        {
            return "我是token";
        }

        return null;
    }

    // 修改密码，修改密码的大部分操作在领域类型中完成的，如，md5加密，或者base64加密
    // 领域服务中，仅仅只是完成持久化
    public async void ModifyPassword(Guid id, string password)
    {
        var user = await _appUserRepository.GetByIdAsync(id);
        if (user != null)
        {
            // 真正修改密码，其实是在领域模型中完成
            user.ModifyPassword(password);
            await _appUserRepository.UpdateAsync(id, user);
        }

    }
}
```