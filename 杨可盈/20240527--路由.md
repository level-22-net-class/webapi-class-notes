```
在控制器上写两个特性
[ApiController]
[Router("/api/..")]
若不符合restfull风格的路由的话，在action上单独写路由
```

```js
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;

namespace Admin2024.Api.Controllers;

[ApiController]
[Route("/api/[controller]")]
public class UsersController : ControllerBase
{
    [HttpGet]
    public string Index()
    {
        dynamic obj = new
        {
            Username = "admin",
            Password = "admin",
        };
        return JsonSerializer.Serialize(obj);
    }
    [HttpGet("{id}")]
    public string Index(int id, int pageIndex = 1, int pageSize = 10)
    {

        return JsonSerializer.Serialize(new { id, pageIndex, pageSize });
    }

    [HttpPost]
    public ActionResult<UserDto> Post(UserDto userDto)
    {
        return userDto;
    }


    [HttpPut("{id}")]
    public ActionResult<dynamic> Put(int id, UserDto userDto)
    {
        return new { id, userDto };
    }

    
    [HttpDelete("{id}")]
    public ActionResult<dynamic> Delete(int id)
    {
        return new { id };
    }
}
```