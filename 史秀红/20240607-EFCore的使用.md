## 一、EF Core的两种使用方法
### 1.代码优先（推荐）
- 根据先创建好的实体类来创建数据库和表
- EF Core会将对实体类的修改同步到数据库中，都是对数据库手工修改将会在EF Core同步数据后丢失
- 用于同步代码到数据库的方法是迁移（Migration），就是提供以增量的方式来修改数据库和表结构，使实体类和数据库保持一致
### 2.数据库优先
根据先创建好的数据库生成相应的代码

## 前提：添加NuGet包到项目 ———— 先下载三包一工具
- ``dotnet add package Microsoft.EntityFrameWorkCore`` —— 核心库，提供了用于数据访问和操作的API
- ``dotnet add package Microsoft.EntityFrameWorkCore.Sqlserver`` —— 允许与数据库进行交互
- ``dotnet add package Microsoft.EntityFrameWorkCore.Design`` —— 设计时工具，用于支持数据库迁移和模型生成
- ``dotnet tool install -g dotnet-ef`` —— 用于使用 Entity Framework Core 进行数据库迁移操作

## 数据库迁移
- ``dotnet ef migrations add <anyname>`` —— 对本次修改生成迁移说明文件
- ``dotnet ef database update`` —— 对其数据库进行同步

### 配置数据库连接
```c#
// appsetting.json
"ConnectionString":{
    Mssql:"server=.;database=BookStore;uid=sa;pwd=123456;TrustServerCertificate=true;"
}
```

### 注入服务到容器
```c#
    // 将数据库配置注入容器
    services.AddDbContext<BookStoreContext>(options => options.UseSqlServer("Mssql"));
```

### 配置数据库上下文DbContext