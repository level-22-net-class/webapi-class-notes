### 通用型仓储
```
通用型仓储接口
namespace Library.Interface;
// 通用性仓储接口
public interface IRepository<T> where T : class
{
   // 获取所有作者/书籍
   List<T> GetAll();
   // 根据id获取作者/书籍
   T? GetById(Guid Id);
   // 新增作者/书籍
   T Insert(T entity);
   // 修改作者/书籍
   T Update(T entity);
   // 删除作者/书籍
   List<T>? Delete(Guid Id);
   // 定义此属性提供了一个访问底层数据集的途径
   DbSet<T> Table {get;}
}
```
###### 通用型仓储接口实现类
 ```  
using Library.Db;
using Library.Interface;
using Microsoft.EntityFrameworkCore;

namespace Library.Services;

public class RepositoryBase<T> : IRepository<T> where T : class
{
    // 声明了一个名为_db的私有只读字段,用于表示数据库上下文
    private readonly BookStoreContext _db;
    // 用于表示数据库表的一个集合的类型，其中T是数据库表对应的实体类型
    private readonly DbSet<T> _tb;
    
    public RepositoryBase(BookStoreContext db)
    {
        _db = db;
        // 这行代码调用了BookStoreContext的Set<T>()方法，它返回一个DbSet<T>对象，代表数据库中的一个表，并将这个对象赋值给_tb字段
        _tb = db.Set<T>();
    }
    // 这是一个公共属性，提供了对_tb字段的只读访问。这个属性允许派生类或使用这个基类的代码访问和操作数据库表
 public DbSet<T> Table { get { return _tb;} }

    public List<T>? Delete(Guid Id)
    {
        var temp = _tb.Find(Id);
        if(temp == null){
            return null;
        }
        _tb.Remove(temp);
        _db.SaveChanges();
        var list = _tb.ToList();
        return list;
    }

    public List<T> GetAll()
    {
        var list = _tb.ToList();
        return list;
    }

    public T? GetById(Guid Id)
    {
        var result = _tb.Find(Id);
        return result;
    }

    public T Insert(T entity)
    {
        _tb.Add(entity);
        _db.SaveChanges();
        return entity;
    }

    public T Update(T entity)
    {
        _tb.Update(entity);
        _db.SaveChanges();
        return entity;
    }
}
```