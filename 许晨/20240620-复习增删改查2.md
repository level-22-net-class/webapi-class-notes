## 博客增删改查前端

### Vue.app

```
<script setup>
import { onMounted,ref } from 'vue';
import axios from 'axios';
let list=ref([]);
let search=ref('');
let isShow=ref(false);
let BlogInfo=ref({})
onMounted(()=>{
    getData();
})
async function getData(){
let res=await axios.get('http://localhost:5087/blog')
console.log(res);
list.value=res.data;
}

async function find(){
let res=await axios.get('http://localhost:5087/blog/'+search.value)
console.log(res);
list.value=[res.data]
console.log(list.value);

}


async function del(id){
    let res=await axios.delete('http://localhost:5087/blog/'+id)
    console.log(res);
    location.reload();
}

async function add(){
    isShow.value=!isShow.value;
}

async function cancel(){
    isShow.value=!isShow.value;
}

async function edit(id){
    isShow.value=!isShow.value;
    let res=await axios.get('http://localhost:5087/blog/'+id)
    console.log(res);
    BlogInfo.value=res.data;

}


async function save(){
    if(BlogInfo.value.id){
        let res=await axios.put('http://localhost:5087/blog/'+BlogInfo.value.id,BlogInfo.value)
        console.log(res);
            getData();
            isShow.value=!isShow.value;
    }else{
        let res=await axios.post('http://localhost:5087/blog',BlogInfo.value)
        console.log(res);
        
            getData();
            isShow.value=!isShow.value;

        
        BlogInfo.value=[];
    }
}

</script>

<template>
<div class="top">
<input type="text" placeholder="请输入关键字" v-model="search">
<button @click="find">查找</button>
<button @click="add">新增</button>
</div>
<div class="content">
    <table class="tb">
        <tr>
            <th>id</th>
            <th>标题</th>
            <th>作者</th>
            <th>标签</th>
            <th>操作</th>

        </tr>
        <tr v-for="item in list" :key="item.id">
            <th>{{ item.id }}</th>
            <th>{{ item.title }}</th>
            <th>{{ item.author }}</th>
            <th>{{ item.flag }}</th>
            <th>
                <button @click="edit(item.id)">编辑</button>
<button @click="del(item.id)">删除</button>

            </th>

        </tr>
    </table>
</div>

<div class="modal" v-if="isShow">
    <div class="middle">
        <tr>
            <td>标题:</td>
            <td><input type="text" v-model="BlogInfo.title"></td>
        </tr>
        <tr>
            <td>作者：</td>
            <td><input type="text" v-model="BlogInfo.author"></td>
        </tr>
        <tr>
            <td>标签:</td>
            <td><input type="text" v-model="BlogInfo.flag"></td>
        </tr>
        <tr>
            <td><button @click="cancel">取消</button></td>
            <td><button @click="save">保存</button></td>
        </tr>
    </div>
</div>

</template>

<style scoped>
.content .tb,tr,th,td{
    border: 1px solid;
    border-collapse: collapse;
}

.modal{
    background-color: rgba(0, 0, 0, .2);
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    position: fixed;
}

.modal .middle{
    background-color: #fff;
    width: 300px;
    height: 150px;
border-radius: 20px;
padding-left: 30px;
padding-top: 30px;
}
</style>
```