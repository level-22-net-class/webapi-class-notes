## API和REST

### API

Application Programming Interface（应用程序接口）是它的全称。简单的理解就是，API是一个接口

### REST

HTTP总共包含八种方法：

```
GET
POST
PUT
DELETE
OPTIONS
HEAD
TRACE
CONNECT
```

● 2xx = Success（成功）

● 3xx = Redirect（重定向）

● 4xx = User error（客户端错误）

● 5xx = Server error（服务器端错误）

我们常见的是200（请求成功）、404（未找到）、401（未授权）、500（服务器错误）...