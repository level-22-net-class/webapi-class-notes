- AuthorController
```csharp
[HttpDelete("{id}")]
public IActionResult Del(Guid id)
{
    var list = _authorRepository.Delete(id); // 使用注入的接口删除指定作者信息
    return Ok(list); // 返回删除的作者信息
}
```

- IAuthorRepository
```csharp
using Admin8.Domain;
using Admin8.Dto;

namespace Admin8.Interface;

public interface IAuthorRepository
{
    // 获取所有作者信息
    ICollection<AuthorDto>? GetAllAuthors(Guid id);

    // 创建作者
    AuthorDto Create(AuthorCreateDto authorCreateDto);

    // 编辑作者信息
    AuthorDto? Edit(Guid id, AuthorEditDto authorEditDto);

    // 删除作者
    AuthorDto? Delete(Guid id);
}
```

- AuthorRepository
```csharp
public AuthorDto? Delete(Guid id)
{
    var obj = AuthorStoreDb.Index.Authors.FirstOrDefault(item => item.Id == id); // 在内存数据库中查找要删除的作者信息
    if (obj == null)
    {
        return null; // 如果作者信息不存在，返回空
    }

    AuthorStoreDb.Index.Authors.Remove(obj); // 从内存数据库中删除作者信息

    var result = new AuthorDto
    {
        Id = obj.Id,
        AuthorName = obj.AuthorName,
        Gender = obj.Gender
    };
    
    return result; // 返回被删除的作者信息
}
```