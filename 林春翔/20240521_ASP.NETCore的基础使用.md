### 一、解决方案和项目之间的关系
1. 编译或打包解决方案时，会同时编译或打包其下所有项目
2. 如果运行解决方案，会按编排的（指定的启动项目），分别编译该项目
3. 在根目录下编译项目会一起编译解决方案，而在项目目录下编译却只会编译该项目

### 二、在项目中引用另一个项目
在MyApi项目中引用ApiDemo项目，需要在终端输入以下命令：
```c#
dotnet add .\MyApi\ reference .\ApiDemo\
```
这会在MyApi.csproj文件中添加以下代码：
```xml
<ItemGroup>
    <ProjectReference Include="..\ApiDemo\ApiDemo.csproj" />
</ItemGroup>
```

### 三、添加依赖包
要添加依赖包到MyApi项目中，可以使用以下命令：
```c#
dotnet add .\MyApi\ package package_name
```
这会在MyApi.csproj文件中添加类似以下代码：
```xml
<PackageReference Include="****" Version="****" />
```
你也可以根据MyApi.csproj文件还原包：
```
dotnet restore
```

### 四、编译一个简单的API请求
1. 在program.cs文件中编写了一个简单的API请求，返回了一组用户信息列表。
2. 启动项目可以使用以下命令：
```c#
dotnet run --project .\MyApi\
```
或者在项目目录下直接运行：
```c#
dotnet run
```
也可以使用 `dotnet watch` 命令进行监控修改，无需每次修改后重新运行。

### 五、中间件
1. 添加中间件的示例代码如下：
```c#
app.Use(async (ctx, next) => {
    var forecast = Enumerable.Range(1, 10);
    await next();
    ctx.Response.WriteAsync("C929星球");
});
```
2. 添加中间件后的效果可以在相应的页面上看到。