```c#
using Admin.Api.Interfaces;
using Admin.Api.Db;
using Admin.Api.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Admin.Api.Services
{
    /// <summary>
    /// Generic repository implementation for CRUD operations.
    /// </summary>
    /// <typeparam name="T">The entity type that the repository operates on.</typeparam>
    public class AuthorRepository<T> : IRepositoryBase<T> where T : class
    {
        private readonly StoreDbContext _db;

        /// <summary>
        /// Constructor that initializes the repository with a database context.
        /// </summary>
        /// <param name="db">The database context.</param>
        public AuthorRepository(StoreDbContext db)
        {
            _db = db ?? throw new ArgumentNullException(nameof(db));
        }

        /// <summary>
        /// Retrieves all entities of type T from the database.
        /// </summary>
        /// <returns>A list of entities.</returns>
        public List<T>? GetAll()
        {
            return _db.Set<T>().ToList();
        }

        /// <summary>
        /// Retrieves a single entity of type T by its unique identifier.
        /// </summary>
        /// <param name="id">The identifier of the entity to retrieve.</param>
        /// <returns>The entity, if found; otherwise, null.</returns>
        public T? GetById(Guid id)
        {
            return _db.Set<T>().Find(id);
        }

        /// <summary>
        /// Adds a new entity of type T to the database.
        /// </summary>
        /// <param name="entity">The entity to add.</param>
        /// <returns>The added entity.</returns>
        public T Post(T entity)
        {
            _db.Set<T>().Add(entity);
            _db.SaveChanges();
            return entity;
        }

        /// <summary>
        /// Updates an existing entity of type T in the database.
        /// </summary>
        /// <param name="id">The identifier of the entity to update.</param>
        /// <param name="entity">The updated entity data.</param>
        /// <returns>The entity before update, if found; otherwise, null.</returns>
        public T? Put(Guid id, T entity)
        {
            var existingEntity = _db.Set<T>().Find(id);
            if (existingEntity != null)
            {
                _db.Entry(existingEntity).CurrentValues.SetValues(entity);
                _db.SaveChanges();
            }
            return existingEntity;
        }

        /// <summary>
        /// Deletes an entity of type T from the database.
        /// </summary>
        /// <param name="id">The identifier of the entity to delete.</param>
        /// <returns>The deleted entity, if found; otherwise, null.</returns>
        public T? Del(Guid id)
        {
            var entity = _db.Set<T>().Find(id);
            if (entity != null)
            {
                _db.Set<T>().Remove(entity);
                _db.SaveChanges();
            }
            return entity;
        }
    }
}

```