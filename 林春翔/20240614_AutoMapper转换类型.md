1. AutoMapper 配置
在你的项目中，确保按照以下方式配置 AutoMapper：
AutoMapperHelper 类定义:

```c#
using AutoMapper;

public class AutoMapperHelper : Profile
{
    public AutoMapperHelper()
    {
        CreateMap&lt;AuthorDto, Authors&gt;();
        // 如果有其他需要的映射关系，可以在这里添加
    }
}
```

2. Startup.cs 中配置 AutoMapper
确保在 Startup.cs 的 ConfigureServices 方法中添加 AutoMapper 的配置：


```c#
public void ConfigureServices(IServiceCollection services)
{
    // 其他服务配置

    // 添加 AutoMapper
    services.AddAutoMapper(typeof(AutoMapperHelper)); // 确保将 AutoMapperHelper 类型传递进去
}

```
3. 控制器中使用 AutoMapper
在你的控制器中使用注入的 IMapper 对象进行对象转换：

```c#
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

public class AuthorsController : ControllerBase
{
    private readonly IMapper _mapper;

    public AuthorsController(IMapper mapper)
    {
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    // 示例方法，演示如何在控制器中使用 AutoMapper 进行对象映射
    [HttpPost("api/authors")]
    public IActionResult CreateAuthor([FromBody] AuthorDto authorDto)
    {
        if (authorDto == null)
        {
            return BadRequest("AuthorDto cannot be null");
        }

        // 使用 AutoMapper 将 AuthorDto 映射为 Authors 对象
        var authorEntity = _mapper.Map&lt;Authors&gt;(authorDto);

        // 这里可以继续处理 authorEntity，例如将其存储到数据库中等操作

        // 返回创建的 Author 对象
        return Ok(authorEntity);
    }
}
```