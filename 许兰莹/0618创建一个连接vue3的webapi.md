创建一个连接Vue3的WebAPI项目并连接数据库，可以分为以下几个步骤：

### 步骤 1：创建WebAPI项目

假设使用的是.NET Core，可以使用Visual Studio或者命令行来创建WebAPI项目。

#### 使用命令行创建：

    dotnet new webapi -n MyWebApi
    cd MyWebApi
    

### 步骤 2：安装数据库连接所需的NuGet包

例如，如果使用的是SQL Server，需要安装`Microsoft.EntityFrameworkCore.SqlServer`。

    dotnet add package Microsoft.EntityFrameworkCore.SqlServer
    dotnet add package Microsoft.EntityFrameworkCore.Tools
    

### 步骤 3：配置数据库连接字符串

在`appsettings.json`文件中，添加数据库连接字符串：

    {
      "ConnectionStrings": {
        "DefaultConnection": "Server=your_server;Database=your_database;User Id=your_user;Password=your_password;"
      }
    }
    

### 步骤 4：创建数据库上下文

在项目中创建一个新的C#文件，比如`AppDbContext.cs`，并定义数据库上下文。

    using Microsoft.EntityFrameworkCore;
    
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }
    
        // 定义DbSet，代表数据库表
        public DbSet<YourEntity> YourEntities { get; set; }
    }
    

### 步骤 5：配置数据库上下文

在`Startup.cs`的`ConfigureServices`方法中，添加数据库上下文的配置。

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddDbContext<AppDbContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
    
        services.AddControllers();
    }
    

### 步骤 6：创建Vue3项目

在WebAPI项目旁边，创建一个Vue3项目。

    vue create my-vue-app
    cd my-vue-app
    

### 步骤 7：在Vue项目中请求WebAPI

安装axios用于发送HTTP请求。

    npm install axios
    

在Vue组件中，你可以使用axios来请求WebAPI。

    import axios from 'axios';
    
    export default {
      name: 'MyComponent',
      methods: {
        async fetchData() {
          try {
            const response = await axios.get('http://localhost:5000/api/yourentity');
            console.log(response.data);
          } catch (error) {
            console.error(error);
          }
        }
      },
      mounted() {
        this.fetchData();
      }
    }
    

### 步骤 8：运行项目

首先运行WebAPI项目：

    dotnet run
    

然后在Vue项目中运行：

    npm run serve
    

现在，Vue3前端应该可以请求你的WebAPI，并且WebAPI能够连接到数据库。