Entity Framework Core (EF Core) 
一、入门基础
------

### 1.1 简介

Entity Framework Core（EF Core）是微软推出的一种轻量级、可扩展、开源且跨平台的对象关系映射（ORM）框架。它允许.NET开发人员使用.NET对象来操作数据库，而无需编写大量数据访问代码。

### 1.2 环境搭建

在开始使用EF Core之前，需要确保安装了.NET Core SDK和Visual Studio或者Visual Studio Code。

二、使用方法
------

### 2.1 创建项目

以.NET Core控制台项目为例，创建一个新的项目。

    // 使用.NET CLI命令创建一个新的.NET Core控制台项目
    dotnet new console -n EFCoreDemo
    

### 2.2 安装EF Core NuGet包

在创建的项目中安装EF Core的NuGet包。

    // 使用.NET CLI命令安装EF Core SQL Server包
    dotnet add package Microsoft.EntityFrameworkCore.SqlServer
    

### 2.3 创建实体类
```cs
创建一个实体类，用于表示数据库中的表。

    // 实体类
    public class Student
    {
        // 主键
        public int Id { get; set; }
    
        // 学生姓名
        public string Name { get; set; }
    }
``` 

### 2.4 创建数据库上下文

创建一个继承自`DbContext`的数据库上下文类，用于管理数据库连接和实体类。
```cs
    // 数据库上下文类
    public class MyDbContext : DbContext
    {
        // 实体集
        public DbSet<Student> Students { get; set; }
    
        // 配置数据库连接
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // 使用SQLite数据库文件
            optionsBuilder.UseSqlite("Data Source=EFCoreDemo.db");
        }
    
        // 配置模型
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // 可以在这里配置实体类的映射关系
        }
    }
 ```   

### 2.5 数据库迁移

使用迁移命令来创建和更新数据库。

    // 使用.NET CLI命令添加迁移
    dotnet ef migrations add Init
    
    // 使用.NET CLI命令更新数据库
    dotnet ef database update
    

### 2.6 使用EF Core操作数据库

以下是如何使用EF Core来添加、查询、更新和删除数据的示例。
```cs
    class Program
    {
        static void Main(string[] args)
        {
            // 创建数据库上下文实例
            using (var context = new MyDbContext())
            {
                // 添加数据
                var student = new Student { Name = "张三" };
                context.Students.Add(student);
                context.SaveChanges();
    
                // 查询数据
                var students = context.Students.ToList();
                foreach (var s in students)
                {
                    Console.WriteLine($"学生ID: {s.Id}, 姓名: {s.Name}");
                }
    
                // 更新数据
                student.Name = "李四";
                context.SaveChanges();
    
                // 删除数据
                context.Students.Remove(student);
                context.SaveChanges();
            }
        }
    }
 ```   

三、代码案例
### 3.1 添加数据
```cs
    // 添加数据
    var student = new Student { Name = "张三" };
    context.Students.Add(student);
    context.SaveChanges();
```  

### 3.2 查询数据
```cs
    // 查询数据
    var students = context.Students.ToList();
    foreach (var s in students)
    {
        Console.WriteLine($"学生ID: {s.Id}, 姓名: {s.Name}");
    }
```   

### 3.3 更新数据
```cs
    // 更新数据
    student.Name = "李四";
    context.SaveChanges();
```   

### 3.4 删除数据
```cs
    // 删除数据
    context.Students.Remove(student);
    context.SaveChanges();
```    