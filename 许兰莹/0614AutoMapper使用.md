当然可以，以下是关于 AutoMapper 的基本用法和常见场景的笔记：

### 1\. 引入和初始化

首先，确保在项目中安装了 AutoMapper。在 .NET 项目中，通过 NuGet 包管理器安装：

    Install-Package AutoMapper
    

然后，在项目中引入 AutoMapper 的命名空间：

    using AutoMapper;
    

初始化 AutoMapper，通常在应用程序启动时执行：

    Mapper.Initialize(config => {
        // 配置映射规则
    });
    

### 2\. 映射规则配置

*   **创建映射**：使用 `CreateMap` 方法定义源类型和目标类型之间的映射关系：

    Mapper.CreateMap<SourceType, DestinationType>();
    

*   **属性映射**：如果源对象和目标对象的属性名不同，可以使用 `.ForMember` 方法指定映射规则：

    Mapper.CreateMap<Source, Destination>()
        .ForMember(dest => dest.DestinationProperty, opt => opt.MapFrom(src => src.SourceProperty));
    

*   **条件映射**：可以设置条件，只在满足条件时进行映射：

    Mapper.CreateMap<Source, Destination>()
        .ForMember(dest => dest.DestinationProperty, opt => opt.Condition(src => src.SourceProperty != null));
    

### 3\. 映射实例

*   **一对一映射**：使用 `Map` 方法将一个源对象转换为目标对象：

    Destination destination = Mapper.Map<Destination>(source);
    

*   **映射列表**：对于集合映射，可以映射整个列表：

    List<Destination> destinations = Mapper.Map<List<Destination>>(sourceList);
    

### 4\. 自动跟踪和忽略属性

*   **自动跟踪**：可以启用 AutoMapper 对对象状态的跟踪，以便在对象改变时自动更新映射结果：

    Mapper.CreateMap<Source, Destination>().AutoMap();
    

*   **忽略属性**：如果不想在映射时包含某些属性，可以使用 `.ForMember` 的 `.Ignore` 方法：

    Mapper.CreateMap<Source, Destination>()
        .ForMember(dest => dest.SensitiveProperty, opt => opt.Ignore());
    

### 5\. 性能优化

*   **配置缓存**：为了提高性能，可以启用映射配置的缓存，减少初始化时间：

    Mapper.Initialize(config => config.CacheMapLookups = true);
    

### 6\. 总结

AutoMapper 是一个强大的工具，它简化了对象之间的映射过程，提高代码的可读性和可维护性。通过合理配置映射规则，可以适应各种复杂的对象映射场景。记得在使用时关注性能优化，特别是处理大型数据集时。