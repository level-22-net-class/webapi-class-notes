### 1\. 创建接口

首先，定义一个接口，其中包含需要实现的方法签名。

    public interface IMyService
    {
        Task DoSomethingAsync();
    }
    

### 2\. 创建实现类

然后，创建一个实现类，实现接口中定义的方法。

    public class MyService : IMyService
    {
        public async Task DoSomethingAsync()
        {
            // 实现具体的业务逻辑
            await Task.Delay(1000);
            Console.WriteLine("Doing something...");
        }
    }
    

### 3\. 使用依赖注入框架（如 ASP.NET Core）

在 ASP.NET Core 中，通常使用 `Microsoft.Extensions.DependencyInjection` 框架来注入依赖。

#### 3.1 配置服务

在 `Startup.cs` 文件中，使用 `ConfigureServices` 方法注册服务。

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddTransient<IMyService, MyService>();
    }
    

#### 3.2 创建控制器

在控制器中注入 `IMyService` 接口。

    public class MyController : ControllerBase
    {
        private readonly IMyService _myService;
    
        public MyController(IMyService myService)
        {
            _myService = myService;
        }
    
        [HttpGet]
        public async Task<IActionResult> GetSomethingAsync()
        {
            await _myService.DoSomethingAsync();
            return Ok();
        }
    }
    

### 4\. 使用控制器

在应用程序中，创建一个 `Program.cs` 文件，启动服务。

    public static async Task Main(string[] args)
    {
        var host = CreateHostBuilder(args).Build();
        await host.RunAsync();
    }
    
    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });
    

### 5\. 测试

使用 `HttpClient` 来测试 API 的行为。

    using System;
    using System.Net.Http;
    using System.Threading.Tasks;
    
    public static async Task TestMyController()
    {
        var client = new HttpClient();
        var response = await client.GetAsync("http://localhost:5000/api/mycontroller");
        response.EnsureSuccessStatusCode();
        Console.WriteLine("API response status: " + response.StatusCode);
    }
