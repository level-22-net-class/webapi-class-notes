Automapper 笔记
=============

1\. 简介
------

Automapper 是一个对象映射器，用于简化不同对象之间的映射过程。它通过配置文件或代码定义映射规则，自动将一个对象的属性值复制到另一个对象的对应属性上，大大简化了数据传输对象（DTO）和实体对象之间的转换工作。

2\. 安装
------

在.NET项目中，可以通过NuGet安装Automapper：

    Install-Package AutoMapper
    

3\. 基本使用
--------

### 3.1 定义映射

首先，需要定义源类型和目标类型之间的映射关系。这可以通过静态方法`CreateMap`来完成：

    Mapper.Initialize(cfg => {
        cfg.CreateMap<Source, Destination>();
    });
    

### 3.2 执行映射

定义好映射后，可以使用`Map`方法执行映射：

    Source source = new Source { /* 属性赋值 */ };
    Destination destination = Mapper.Map<Destination>(source);
    

4\. 高级用法
--------

### 4.1 自定义映射规则

有时候，源对象和目标对象的属性名不完全对应，或者需要进行一些额外的转换逻辑。Automapper允许通过`ForMember`方法自定义这些规则：

    Mapper.Initialize(cfg => {
        cfg.CreateMap<Source, Destination>()
            .ForMember(dest => dest.PropertyName, opt => opt.MapFrom(src => src.AnotherPropertyName));
    });
    

### 4.2 集合映射

Automapper也支持集合类型的映射：

    List<Source> sourceList = new List<Source> { /* 元素赋值 */ };
    List<Destination> destinationList = Mapper.Map<List<Destination>>(sourceList);
    

### 4.3 条件映射

可以使用`Condition`属性设置映射条件，只有满足条件的属性才会被映射：

    Mapper.Initialize(cfg => {
        cfg.CreateMap<Source, Destination>()
            .ForMember(dest => dest.SomeProperty, opt => opt.Condition(src => src.SomeProperty != null));
    });
    

5\. 性能优化
--------

为了提高性能，Automapper提供了缓存机制。在初始化映射配置时，可以启用映射配置的缓存：

    Mapper.Initialize(cfg => {
        cfg.CreateMap<Source, Destination>();
    }, MemberList.Source);
    

6\. 总结
------

Automapper是一个强大的对象映射工具，它简化了对象之间的转换过程，提高了开发效率。通过合理配置映射规则，可以处理大多数复杂的映射需求。使用时应注意性能优化，合理利用缓存机制。