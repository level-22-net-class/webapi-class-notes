## 分页

BlogsConreoller.cs

```
[HttpGet(Name ="abc")]
    public ActionResult<BlogsDto> GetAll([FromQuery]BaseParams baseParams)
    {
        // 1.考虑如何从前端传递分页数据到接口
        // 2.后面到底是如何完成分页这个操作的
        var blog=_blogDb.GetAll().Skip((baseParams.PageIndex-1)*baseParams.PageSize).Take(baseParams.PageSize).ToList();
        // 返回给请求端的页信息如下（猜测）：
        // 总共多少条数据
        // 第几页数据
        // 每页条目数
        // 共多少页

        var count=_blogDb.Table.Count();
        var totalPages=Math.Ceiling(count*1.0/baseParams.PageSize);
        
        // 分页的数据信息
        var pagination=new
        {
            pageIndex=baseParams.PageIndex,
            pageSize=baseParams.PageSize,
            totalPages,
            totalCount=count,
            previousPageUrl=baseParams.PageIndex==1?null:Url.Link("abc",new{pageIndex=baseParams.PageIndex-1,pageSize=baseParams.PageSize}),
            nextPageUrl=baseParams.PageIndex<totalPages?Url.Link("abc",new{pageIndex=baseParams.PageIndex+1,pageSize=baseParams.PageSize}):null
        }; 
        // 将分页数据信息附加到响应头
        Response.Headers.Append("X-Pagination",JsonSerializer.Serialize(pagination));
        return Ok(new{code=1000,msg="获取所有博客成功",data=blog,baseParams,count,totalPages});
    }
```

Blog.Api.http

```
### 获取页码数及该页面显示的数据个数
GET {{url}}/api/blogs?pageIndex=1&pageSize=12
```