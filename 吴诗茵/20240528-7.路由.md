## 路由

在Program.cs中写：

```
namespace mm.Api;

public static class Program
{
    public static void Main(string[] args)
    {
        CreateHostBuilder(args).Build().Run();
    }
    public static IHostBuilder CreateHostBuilder(string[] args)
    {
        return Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(static builder =>
        {
            builder.UseStartup<Startup>();
        });
    }
}
```

在Startup.cs中写：

```
using Microsoft.AspNetCore.Mvc;
using mm.Api.Filters;

namespace mm.Api;

public class Startup
{
    public void Configure(IApplicationBuilder app)
    {
        app.UseRouting();
        app.UseEndpoints(endpoints =>endpoints.MapControllers());
    }
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllers(Options=>{Options.Filters.Add<ApiResultFilter>();});
        // services.AddScoped<IAsyncResultFilter,ApiResultFilter>();
        services.Configure<MvcOptions>(options =>{options.Filters.Add<ApiAcitonFilter>();});
    }
}
```

在BlogsController.cs中写：

```
using Microsoft.AspNetCore.Mvc;

namespace mm.Api;

[ApiController]
[Route("[controller]")]
public class BlogsController:ControllerBase
{
    // /blogs/:id
    [Route("{id?}")]
    public IActionResult Details(int id)
    {
        return Ok(id);
    }

    [HttpPost]
    public ActionResult<BlogDto> Post(BlogDto blogDto)
    {
        return blogDto;
    }

    [HttpPut("{id}")]
    public ActionResult<dynamic> Put(int id, BlogDto blogDto)
    {
        return new{id,blogDto};
    }

    public IActionResult Delete(int id)
    {
        return Ok(new{Code=1000,Msg="请求成功aa",Data="135"});
    }
}
```

在ApiResult.cs中写：

```
namespace mm.Api;

public class ApiResult
{
    public int Code{ get; set; }
    public string? Msg{ get; set; }
    public object? Data{ get; set; }
}
```

在BlogDto.cs中写：

```
namespace mm.Api;

public class BlogDto
{
    public string Title { get; set;}=null!;
    public string Author{ get; set;}=null!;
    public string? Flag{ get; set;}
}
```

在ApiResultFilter.cs中写：

```
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace mm.Api.Filters;

public class ApiResultFilter : IAsyncResultFilter
{
    public async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
    {
        // 判断返回结果是否是内容，如果是，我们就给context.Result赋一个新的实例对象ApiResult
        if (context.Result is ObjectResult objectResult)
        {
            context.Result = new ObjectResult(new ApiResult
            {
                Code = 1000,
                Msg = "请求成功",
                Data = objectResult.Value
            });
        }
        else
        {
            context.Result = new ObjectResult(new ApiResult { Code = 1001 });
        }
        await next();
    }
}
```