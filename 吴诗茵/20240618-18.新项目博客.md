## 新项目-博客

在Program.cs中写：

```
namespace Blog.Api;

public class Program
{
    public static void Main(string[] args)
    {
        CreateBuilder(args).Build().Run();
    }
    public static IHostBuilder CreateBuilder(string[] args)
    {
        return Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webBuilder=>
        {
            webBuilder.UseStartup<Startup>();
        });
    }
}
```

在Startup.cs中写：

```
using Blog.Api.Db;
using Blog.Api.Impl;
using Blog.Api.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Blog.Api;

public class Startup
{
    private readonly IConfiguration _configuration;
    public Startup(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllers();

        // 注入数据库上下文到容器
        services.AddDbContext<BlogsDbContext>(options=>
        {
            options.UseSqlServer(_configuration.GetConnectionString("SqlServer"));
        });

        // 注入通用仓储接口到容器
        services.AddScoped(typeof(IRepositoryBase<>),typeof(RepositoryBase<>));
    }
    public void Configure(IApplicationBuilder app)
    {
        app.UseRouting();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}
```

在BlogsController.cs中写：

```
using Blog.Api.Domain;
using Blog.Api.Dto;
using Blog.Api.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Blog.Api.Controllers;

[ApiController]
[Route("/api/[controller]")]
public class BlogsController:ControllerBase
{
    private readonly IRepositoryBase<Blogs> _blogDb;
    public BlogsController(IRepositoryBase<Blogs> blogDb)
    {
        _blogDb = blogDb;
    }

    [HttpGet]
    public ActionResult<BlogDto> GetAll()
    {
        var blog=_blogDb.GetAll();
        return Ok(blog);
    }

    [HttpGet("{id}")]
    public ActionResult<BlogDto> GetById(int id)
    {
        var blog= _blogDb.GetById(id);
        return Ok(blog);
    }

    [HttpPost]
    public IActionResult Insert(BlogsCreateDto blogsCreateDto)
    {
        var blog=new Blogs
        {
            Title=blogsCreateDto.Title,
            Author=blogsCreateDto.Author,
            Description=blogsCreateDto.Description
        };
        _blogDb.Insert(blog);
        return Ok("新增博客成功");
    }

    [HttpPut("{id}")]
    public IActionResult Update(int id,BlogsUpdateDto blogsUpdateDto)
    {
        var blog=_blogDb.GetById(id);
        if(blog!=null)
        {
            blog.Title = blogsUpdateDto.Title;
            blog.Author = blogsUpdateDto.Author;
            blog.Description = blogsUpdateDto.Description;
            _blogDb.Update(blog);
            return Ok("更新博客成功");
        }
        return Ok("更新失败，请重新试试吧");
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        _blogDb.Delete(id);
        return Ok("删除博客成功");
    }

}
```

在BlogsDbContext.cs中写：

```
using Blog.Api.Domain;
using Microsoft.EntityFrameworkCore;

namespace Blog.Api.Db;

public class BlogsDbContext: DbContext
{
    public BlogsDbContext(DbContextOptions<BlogsDbContext>options): base(options)
    {

    }
    public DbSet<Blogs>Blogs{ get; set; }
}
```

在Blogs.cs中写：

```
namespace Blog.Api.Domain;

public class Blogs
{
    public int Id { get; set;}
    public string Title { get; set;}=null!;
    public string Author { get; set;}=null!;
    public string? Description { get; set;}
}
```

在BlogsDto.cs中写：

```
namespace Blog.Api.Dto;

public class BlogsDto
{
    public int Id { get; set; }
    public string Title { get; set; }=null!;
}
```

在BlogsCreateDto.cs中写：

```
namespace Blog.Api.Dto;

public class BlogsCreateDto
{
    public string Title { get; set; }=null!;
    public string Author { get; set; }=null!;
    public string? Description { get; set; }
}
```

在BlogsUpdateDto.cs中写：

```
namespace Blog.Api.Dto;

public class BlogsUpdateDto
{
    public string Title { get; set; }=null!;
    public string Author { get; set; }=null!;
    public string? Description { get; set; }
}
```

在RepositoryBase.cs里写：

```
using Blog.Api.Db;
using Blog.Api.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Blog.Api.Impl;

public class RepositoryBase<T> : IRepositoryBase<T>where T : class
{
    private readonly BlogsDbContext _db;
    private readonly DbSet<T> _tb;
    public RepositoryBase(BlogsDbContext db)
    {
        _db = db;
        _tb = _db.Set<T>();
    }

    public ICollection<T> GetAll()
    {
        return _tb.ToList();
    }

    public T? GetById(int id)
    {
        var entity = _tb.Find(id);
        return entity;
    }

    public void Insert(T entity)
    {
        _tb.Add(entity);
        _db.SaveChanges();
    }

    public void Update(T entity)
    {
        _tb.Update(entity);
        _db.SaveChanges();
    }

    public void Delete(int id)
    {
        var entity = GetById(id);
        if(entity != null)
        {
            Delete(entity);
        }
    }

    public void Delete(T entity)
    {
        _tb.Remove(entity);
        _db.SaveChanges();
    }
}
```

在IRepositoryBase.cs里写：

```
namespace Blog.Api.Interfaces;

public interface IRepositoryBase<T>where T : class
{
    /// <summary>
    /// 获取所有实体对象
    /// </summary>
    /// <returns></returns>
    ICollection<T> GetAll();

    /// <summary>
    /// 获取指定Id的实体对象
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    T? GetById(int id);

    /// <summary>
    /// 新增记录
    /// </summary>
    /// <param name="entity"></param>
    void Insert(T entity);

    /// <summary>
    /// 修改指定Id的记录
    /// </summary>
    /// <param name="entity"></param>
    void Update(T entity);

    /// <summary>
    /// 删除指定Id的记录
    /// </summary>
    /// <param name="id"></param>
    void Delete(int id);
    
    /// <summary>
    /// 删除指定实体对象的记录
    /// </summary>
    /// <param name="entity"></param> <summary>
    void Delete(T entity);
}
```

在appsettings.json里写：

```
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft.AspNetCore": "Warning"
    }
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "SqlServer":"server=服务器名称;database=Blog;uid=sa;pwd=密码;TrustServerCertificate=true"
  }
}

```

在BooksStore.Api.http中写：

```
@url=http://localhost:5140

### 获取所有博客
GET {{url}}/api/blogs

### 获取指定Id博客
GET {{url}}/api/blogs/3

### 新增博客
POST {{url}}/api/blogs HTTP/1.1
Content-Type: application/json

{
    "title":"标题5",
    "author":"作者5",
    "description":"描述5"
}

### 修改博客
PUT {{url}}/api/blogs/4 HTTP/1.1
Content-Type: application/json

{
    "title":"标题44",
    "author":"作者44",
    "description":"描述44"
}

### 删除博客
DELETE {{url}}/api/blogs/4 HTTP/1.1
```