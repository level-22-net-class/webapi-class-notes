在Program.cs中写：

```
namespace mm.Api;

public static class Program
{
    public static void Main(string[] args)
    {
        CreateWebHostBuilder(args).Build().Run();
    }
    public static IHostBuilder CreateWebHostBuilder(string[] args)
    {
        return Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(CreateWebHostBuilder=>
        {
            CreateWebHostBuilder.UseStartup<Startup>();
        });
    }
}
```

在Startup.cs中写：

```
namespace mm.Api;

public class Startup
{
    // 注册或者添加中间件
    public void Configure(IApplicationBuilder app)
    {
        app.UseRouting();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
    // 准备用于依赖注入的服务
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllers();
    }
}
```

在BooksController.cs中写：

```
using Microsoft.AspNetCore.Mvc;

namespace mm.Api;
[Route("[controller]")]
public class BooksController:ControllerBase
{
    public IActionResult Index()
    {
        return Ok("8888");
    }
}
```