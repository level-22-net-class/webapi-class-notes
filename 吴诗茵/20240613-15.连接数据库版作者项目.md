## 连接数据库版作者项目

在Program.cs中写：

```
namespace BookStore.Api;

public static class Program
{
    public static void Main(string[] args)
    {
        CreateWebHostBuilder(args).Build().Run();
    }
    public static IHostBuilder CreateWebHostBuilder(string[] args)
    {
        return Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webBuilder=>
        {
            webBuilder.UseStartup<Startup>();
        });
    }
}
```

在Startup.cs中写：

```
using BookStore.Api.Db;
using BookStore.Api.Interface;
using BookStore.Api.Services;
using Microsoft.EntityFrameworkCore;
namespace BookStore.Api;
public class Startup
{
    private   readonly IConfiguration _configuration;
    public Startup(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    public void Configure(IApplicationBuilder app)
    {
        app.UseRouting();
        app.UseEndpoints(endpoints=>
        {
            endpoints.MapControllers();
        });
    }
    public void ConfigureServices(IServiceCollection services)
    {
        // 将swagger端点服务器和界面服务注册到容器
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen();

        // 将控制器服务注册到容器
        services.AddControllers();

        // 将仓储服务注册到容器
        services.AddScoped<IAuthorRepository,AuthorRepository>();
        services.AddScoped<IBookRepository,BookRepository>();

        // 注册通用仓储接口
        services.AddScoped(typeof(IRepositoryBase<>),typeof(RepositoryBase<>));

        // 注册数据库上下文到容器
        services.AddDbContext<BookStoreDbContext>(option=>option.UseSqlServer(_configuration.GetConnectionString("Mssql")));
    }
}
```

在AuthorsController.cs中写：

```
using BookStore.Api.Domain;
using BookStore.Api.Dto;
using BookStore.Api.Interface;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.Api.Controller;

[ApiController]
[Route("api/[controller]")]
public class AuthorsController : ControllerBase
{
    private readonly IAuthorRepository _authorRepository;
    private readonly IRepositoryBase<Authors> _authorsRep;

    public AuthorsController(IAuthorRepository authorRepository,IRepositoryBase<Authors> authorsRep)
    {
        _authorRepository = authorRepository;
        _authorsRep = authorsRep;
    }
    [HttpGet("{id?}")]
    public IActionResult Get(Guid id)
    {
        if(id.ToString()=="00000000-0000-0000-0000-000000000000")
        {
            return Ok(_authorsRep.GetAll());
        }
        else
        {
            var item=_authorsRep.GetById(id);
            return Ok(item);
        }
    }
    [HttpPost("")]
    public IActionResult Post(AuthorCreateDto authorCreateDto)
    {
        // 1.拿到AuhtorCreateDto类型的实例化数据-模型绑定
        // 2.将相关数据保存到数据库
        // ① 转换AuthorCreateDto类型的数据为Authors
        // ② 调用数据库上下文，将数据插入
        var author=new Authors{Id=Guid.NewGuid(),AuthorName=authorCreateDto.AuthorName,Gender=authorCreateDto.Gender,Birth=authorCreateDto.Birth};
        var result=_authorsRep.Insert(author);
        // var result=_authorRepository.Insert(authorCreateDto);
        return Ok(result);
    }
    [HttpPut("{id}")]
    public IActionResult Put(Guid id,AuthorUpdateDto authorUpdateDto)
    {
        var author=_authorsRep.GetById(id);
        if(author==null)
        {
            return NotFound();
        }
        author.AuthorName = authorUpdateDto.AuthorName;
        author.Gender = authorUpdateDto.Gender;
        author.Birth = authorUpdateDto.Birth;
        var result=_authorsRep.Update(author);
        return Ok(result);
    }
    [HttpDelete("{id}")]
    public IActionResult Delete(Guid id)
    {
        var author=_authorsRep.Delete(id);
        return Ok(author);
    }
}
```

在BookStoreDbContext.cs中写：

```
using BookStore.Api.Domain;
using Microsoft.EntityFrameworkCore;

namespace BookStore.Api.Db;

public class BookStoreDbContext: DbContext
{
    public BookStoreDbContext(DbContextOptions<BookStoreDbContext>options): base(options)
    {
        
    }
    public DbSet<Authors>Authors { get; set; }
    public DbSet<Books> Books { get; set;}
}
```

在Authors.cs中写：

```
namespace BookStore.Api.Domain;

public class Authors
{
    public Guid Id{ get; set; }
    public string AuthorName { get; set;}=null!;
    public int Gender { get; set;}
    public DateTime Birth { get; set;}
}
```

在AuthorDto.cs中写：

```
namespace BookStore.Api.Dto;

public class AuthorDto
{
    public Guid Id { get; set; }
    // name 一个字符串 如果为null，表示不指向任何一个值
    // name 其值为""，就是空字符串，其实是有指向一个值，这个值是空字符串而已
    public string AuthorName { get; set; }=null!;
    public int Gender { get; set; }
}
```

在AuthorCreateDto.cs中写：

```
namespace BookStore.Api.Dto;

public class AuthorCreateDto
{
    public string AuthorName{ get; set; }=null!;
    public int Gender{ get; set; }
    public DateTime Birth{ get; set; }
}
```

在AuthorUpdateDto.cs中写：

```
namespace BookStore.Api.Dto;

// 专为作者更新传输而设
public class AuthorUpdateDto
{
    public string AuthorName{ get; set; }=null!;
    public int Gender{ get; set; }
    public DateTime Birth{ get; set; }
}
```

在IRepositoryBase.cs里写：

```
namespace BookStore.Api.Interface;

/// <summary>
/// 基础仓储接口，定义通用的CURD功能
/// </summary>
public interface IRepositoryBase<T>
{
    // 通过id获取指定模型（对应数据库中的表，模型中的公共属性对应数据表的字段或者叫做列）
    // 获取所有的模型
    // 获取所有实体
    // 新增插入
    // 删除
    // 更新

    /// <summary>
    /// 根据提供的主键id，尝试获取对应记录，以实体的形式返回
    /// </summary>
    /// <param name="id">主键</param>
    /// <returns></returns>
    T? GetById(Guid id);

    /// <summary>
    ///  获取所有的记录，以集合形式返回，集合中的类型是实体类型
    /// </summary>
    /// <returns>包含实体类型的集合</returns>
    List<T>GetAll();

    /// <summary>
    /// 插入数据到数据库
    /// </summary>
    /// <param name="entity">实体对象</param>
    /// <returns>带Id值的实体对象</returns>
    T Insert(T entity);

    /// <summary>
    /// 更新实体
    /// </summary>
    /// <param name="entity">已经被赋值新数据的实体</param>
    /// <returns></returns>
    T Update(T entity);

    /// <summary>
    /// 根据实体对象删除对应记录
    /// </summary>
    /// <param name="entity">准备删除的实体对象</param>
    /// <returns>已经删除的实体对象</returns>
    T Delete(T entity);

    /// <summary>
    /// 根据主键Id删除对应记录
    /// </summary>
    /// <param name="id">主键</param>
    /// <returns>已经删除的实体对象</returns>
    T? Delete(Guid id);
}
```

在RepositoryBase.cs里写：

```
using BookStore.Api.Db;
using BookStore.Api.Interface;
using Microsoft.EntityFrameworkCore;

namespace BookStore.Api.Services;

public class RepositoryBase<T> : IRepositoryBase<T>where T : class
{
    private readonly BookStoreDbContext _db;
    private readonly DbSet<T> _tb;
    public RepositoryBase(BookStoreDbContext db)
    {
        _db = db;
        _tb=db.Set<T>(); 
    }
    public T Delete(T entity)
    {
        _tb.Remove(entity);
        _db.SaveChanges();
        return entity;
    }
    public T? Delete(Guid id)
    {
        var entity = _tb.Find(id);
        if (entity == null)
        {
            return null;
        }
        _tb.Remove(entity);
        _db.SaveChanges();
        return entity;
    }
    public List<T> GetAll()
    {
        // // 第一种写法
        // var list=_db.Set<T>().ToList();
        // return list;

        // // 第二种写法
        var list=_tb.ToList();
        return list;
    }
    public T? GetById(Guid id)
    {
        var entity=_tb.Find(id);
        return entity;
    }
    public T Insert(T entity)
    {
        _tb.Add(entity);
        _db.SaveChanges();
        return entity;
    }
    public T Update(T entity)
    {
        _tb.Update(entity);
        _db.SaveChanges();
        return entity;
    }
}

```

在appsettings.json里写：

```
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft.AspNetCore": "Warning"
    }
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "Mssql":"server=服务器名称;database=BookStore;uid=sa;pwd=密码;TrustServerCertificate=true"
  }
}

```

在BooksStore.Api.http中写：

```
@url = http://localhost:5000

### 获取作者列表
GET {{url}}/api/authors HTTP/1.1

### 获取指定id作者
GET {{url}}/api/authors/fd2a48e5-ebe5-4a0b-a702-0131c022be51 HTTP/1.1

### 新增作者
POST {{url}}/api/authors HTTP/1.1
Content-Type: application/json
  
{
    "authorName":"司丝",
    "gender":1,
    "birth":"2000-08-07"
}

### 修改作者
PUT {{url}}/api/authors/fd2a48e5-ebe5-4a0b-a702-0131c022be51 HTTP/1.1
Content-Type: application/json

{
    "authorName":"绾绾",
    "gender":2,
    "birth":"1985-05-07"
}

### 删除作者
DELETE {{url}}/api/authors/4600cbd9-c475-4775-abd5-c69c153c7821 HTTP/1.1
```