### PlantUML
PlantUML是一个开源工具，允许用户通过简单的文本描述来创建UML图，他的优点在于能够快速绘制出结构图，如序列图、活动图等
### PlantUML使用
1. 安装PlantUML插件： 在扩展中安装PlantUML插件

2. 创建一个PlantUML文件：

- 在VS Code中，创建一个新文件，并将其保存为.puml扩展名（或其他PlantUML支持的文件格式，如.plantuml）。

3. Alt+D 浏览流程图
### 思维导图
```
@startmindmap //开始
* 中心
** 主题1
***: 主题1.1
未登录页进入,登录页进行登录;
***: 主题1.2
** 主题2
** 主题3
@endmindmap //结束
```

### 活动图
```
@startuml 开始
@enduml 结束
: 语句开始
：语句结束
```

### EFCore流程图
生成代码：

```
@startuml
<style>

</style>
start
#lightblue:      下载efCore核心包 
(Micresoft.EntityFrameCore);
#lightblue:安装数据库驱动包，支持主流数据库
(Microsoft.EntityFrameCore.SqlServer);
:创建实体类型;
:         创建数据库上下文(DbCpntext)
定义对应数据库表DbSet<Users>Users;
#lightblue:          安装迁移依赖包
Microsoft.EntityFrameCore.Design;
:          生成迁移文件
(dotnet ef migrations add <迁移名称>);


stop
@enduml
```