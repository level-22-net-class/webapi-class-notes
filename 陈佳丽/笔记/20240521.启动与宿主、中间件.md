### 解决方案与项目的区别
1. 变异或者打包解决方案的时候，会同时编译或打包其下所有的项目
2. 如果运行解决方案的话，则会按照编排的（指定的启动项目），分别调用指定项目

## 启动与宿主
  ASP.NET Core应用程序启动时，他首先会配置并运行其宿主(Host),宿主主要用来启动、初始化应用程序，并管理其生命周期。

ASP.NET Core应用程序的启动主要包含三个步骤：
- CreateDefaultBuilder()：创建IWebHostBuilder
- Build()：IWebHostBuilder负责创建IWebHost
- Run()：启动IWebHost

#### 宿主构造器
在启动IWebHost宿主之前，需要完成对IWebHost的创建和配置。这需要IWebHostBuild对象完成。而IWebHostBulider是由CreateDefaultBuilder方法创建的。

CreateDefaultBuilder()方法主要干了六件大事：
- UseKestrel：使用Kestrel作为Web server。
- UseContentRoot：指定Web host使用的content。  root（内容根目录），比如Views。默认为当前应用程序根目录。
- ConfigureAppConfiguration：设置当前应用程序配置。主要是读取 appsettinggs.json  配置文件、开发环境中配置的UserSecrets、添加环境变量和命令行参数 。
- ConfigureLogging：读取配置文件中的Logging节点，配置日志系统。
- UseIISIntegration：使用IISIntegration 中间件。
- UseDefaultServiceProvider：设置默认的依赖注入容器。

创建完毕WebHostBuilder后，通过调用UseStartup()来指定启动类，来为后续服务的注册及中间件的注册提供入口 

#### 宿主
在ASP.Net Core中定义了IWebHost用来表示Web应用的宿主，并提供了一个默认实现WebHost。宿主的创建是通过调用IWebHostBuilder的Build()方法来完成的

## 中间件
中间件就是处理HTTP请求和响应的组件，它本质上是一段用来处理请求与响应的代码。

#### 创建中间件
Startup类的Configure方法就是添加中间件的地方，在Configure方法中，通过调用IApplicationBuilder接口以Use开头的扩展方法，就可以添加系统内置的中间件。
```c#
public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
{
    if (env.IsDevelopment())
    {
        // 异常中间件
        app.UseDeveloperExceptionPage();
    }

    // 路由中间件
    app.UseRouting();
    // 授权中间件
    app.UseAuthorization();
    // 终结点中间件
    app.UseEndpoints(endpoints =>
    {
        endpoints.MapControllers();
    });
}
```

#### Run() 方法  
Run()方法中只有一个RequestDelegate委托类型的参数，没有Next参数，所以Run()方法也叫终端中间件，不会将请求传递给下一个中间件，也就是发生了“短路”。

```c#
// Run方法向应用程序的请求管道中添加一个RequestDelegate委托
// 放在管道最后面，终端中间件
app.Run(handler: async context => 
{
    await context.Response.WriteAsync(text: "Hello World1\r\n");
});
app.Run(handler: async context =>
{
    await context.Response.WriteAsync(text: "Hello World2\r\n");
});
```

#### Map()
Map方法有两个参数：第一个参数是匹配规则，第二个参数是Action泛型委托，泛型委托参数是IApplicationBuilder类型，和Configure方法的第一个参数类型相同 。这就表示可以把实现了Action泛型委托的方法添加到中间件管道中执行。
```c#
// Map可以根据匹配的URL来选择执行，简单来说就是根据URL进行分支选择执行
// 有点类似于MVC中的路由
// 匹配的URL：http://localhost:5000/Map1
app.Map(pathMatch: "/Map1", configuration: HandleMap1);
// 匹配的URL：http://localhost:5000/Map2
app.Map(pathMatch: "/Map2", configuration: HandleMap2);
```