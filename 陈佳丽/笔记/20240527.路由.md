### IActionResult 和 ActionResult<T>
IActionResult 和 ActionResult<T> 都用于表示控制器方法的返回结果，但它们有一些区别：

1. IActionResult: 是 ASP.NET Core 中所有控制器方法返回类型的基本接口。
它代表一个可以执行的操作结果，可以是任何类型的结果，例如视图、重定向、JSON 数据等。
使用 IActionResult 作为返回类型可以使控制器方法更加灵活，可以根据需要返回不同类型的结果。

2. ActionResult<T>: 是 IActionResult 的泛型版本，其中 T 表示返回结果的类型。
ActionResult<T> 是一个带有数据的特定类型的操作结果，通常用于返回包含数据的结果，如 JSON 数据、视图模型等。

使用 ActionResult<T> 可以更方便地返回带有数据的结果，并且在编码时提供了更好的类型安全性。

3. 区别总结：

- IActionResult 是通用的操作结果接口，可以返回任何类型的结果。
- ActionResult<T> 是带有数据的特定类型的操作结果，提供更好的类型安全性和便利性。

在实际开发中，你可以根据需要选择使用 IActionResult 或 ActionResult<T>。如果你的方法需要返回不同类型的结果，你可以使用 IActionResult；如果方法需要返回特定类型的数据结果，你可以使用 ActionResult<T>。

### 模型绑定
Action特性：

- [FromQuery]：从HTTP请求的查询字符串中获取参数的值
- [FromForm]：从表单中获取参数的值
- [FromHeader]：从HTTP 请求的头部信息中获取参数值
- [FromBody]：从请求的内容体获取参数值
- [FromServices]：从依赖注入容器中获取参数值
- [FromRoute]：从路由中获取参数值
- [BindRequiredAttribute]：如果没有值绑定到此参数，或绑定不成功，这个特性将添加一个ModelState错误

另外，看可以添加[ApiController]位于命名空间下方，会尝试自动获取参数值，不再需要为参数指定上述其他特性

### 模型验证
使用Required特性为属性添加需要的数据验证
```
[Required(ErrorMessage)="标题属性不能为空"]
[MinLength(20,ReeroMessage="标题不能少于20个字符")]
public string Title{get;set;}
```

### 实现一个简易的CRUD
```
using Microsoft.AspNetCore.Mvc;

namespace Admin2024.api;
[ApiController]
//定义路由，[controller]代指`/blog`
[Route("[controller]")]
public class BlogController : ControllerBase
{
    //获取到所有或者指定id的数据
    [HttpGet("{id?}")]
    public IActionResult Index(int id)
    {
        return Ok(new {Data=id});
    }
    //保存数据，BlogDto为定义的实体类对象，注意返回类型为ActionResult<实体类对象>
    [HttpPost]
    public ActionResult<BlogDto> Edit(BlogDto blogDto)
    {
        return blogDto;
    }
    //删除数据
    [HttpDelete]
    public IActionResult Delete()
    {
        return Ok(new {Code=3000,Msg="删除成功"});
    }
    //修改数据，BlogDto为定义的实体类对象，注意返回类型为ActionResult<实体类对象>
    [HttpPut("{id}")]
    public ActionResult<dynamic> Put(int id,BlogDto blogDto)
    {
        return Ok(new {Code=3000,Msg="修改成功",id=id,Data=blogDto});
    }
}
```