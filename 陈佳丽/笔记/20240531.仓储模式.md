### 集合接口
- ICollection<T> :ICollection<T>接口由泛型集合类实现。使用这个接口可以获得集合中的元素个数(Conunt())，还可以从集合中添加和删除元素(Add()、Remove()、Clear())、把集合复制到数组中(CopyTo()方法)
- IList<T> :IList<T>接口由于可通过位置访问其中的元素列表，这个接口定义了一个索引器，可以在集合的指定位置插入或者删除某些项(Insert()和RemovcAt()方法)。ILst<T>接口派生自ICollection<T>接口

### 仓储模式
仓储模式通常指的是使用Entity Framework Core实现数据持久化


### 接口
I实例名Repository
```c#
    //通过Id获取所有作者的实现
    实例对象? GetAuthorById(Guid guid);

    //获取所有作者的方法
    //函数三要素（函数名，形参，返回值）
    ICollection<实例对象> GetAllAuthors();
```

实例名Repository
```c#
   public class 实例名Repository:I实例名Repository
    {
       public ICollection<实例名> GetAllAuthors()
       {
            //从持久化数据库中获取
            return 名Db.Instance.实例名.ToList();
            //return [.. 名Db.Instance.实例名];
       }
       public 实例名? GetAuthorById(Guid guid)
       {
            return 名Db.Instance.实例名.SingleOrDefault(item=>item.Id==guid);
       }
    }
```

控制器
```c#
    private readonly I实例名Repository _实例名Repository;
    public 实例名Controller(I实例名Repository 实例名Repository)
    {
        _实例名Repository=实例名Repository;
    }
    //获取get的函数中
    [HttpGet("{id?}")]
    public IActionResult Get(int id)
    {
        if(id.ToString==""){
            return Ok(_实例名Repository.GetAllAuthors());
        }else{
            return Ok(_实例名Repository.GetAuthorById(id));
        }
    }
```
Startup.cs
```c#
  public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllers();
        //新加
        services.AddScoped<I实例名Repository,实例名Repository>();
    }
```