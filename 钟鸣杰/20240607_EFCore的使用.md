一、EF Core的两种使用方法
1.代码优先（推荐）
根据先创建好的实体类来创建数据库和表
EF Core会将对实体类的修改同步到数据库中，都是对数据库手工修改将会在EF Core同步数据后丢失
用于同步代码到数据库的方法是迁移（Migration），就是提供以增量的方式来修改数据库和表结构，使实体类和数据库保持一致
2.数据库优先
根据先创建好的数据库生成相应的代码

二、EF Core的添加步骤（代码优先）
1.创建数据模型
创建数据模型类，这些类将映射到数据库中的表

2. 配置数据库连接
// appsetting.json
"ConnectionString":{
    Mssql:"server=.;database=BookStore;uid=sa;pwd=123456;TrustServerCertificate=true;"
}
3.配置数据库上下文DbContext
DbContext是非常重要的类，代表程序与数据库之间的会话或数据上下文，能够完成查询和保存数据等操作
它的派生类中有若干个DbSet类型的公告属性，表示相应实体的集合，对它们的操作最后会反映到数据表
// Db/BookStoreDbContext.cs
using Microsoft.EntityFrameworkCore;
namespace BookStore.Api.Db;
public class BookStoreDbContext:DbContext
{
    public DbSet<Author> Authors {get;set;}
    public DbSet<Book> Books {get;set;}
    // 构造函数接受一个 DbContextOptions<BookStoreDbContext> 类型的参数
    // 将该参数传递给基类的构造函数 base(options)
    // 这个构造函数用于配置数据库上下文的选项，例如数据库提供程序、连接字符串等
    public BookStoreDbContext(DbContextOptions<BookStoreDbContext> options):base(options)
    {

    }
}
注意点：

提供正确的连接字符串
必须构造函数选项配置
4.注入服务，引入配置文件，实现连接数据库
// Startup.cs
servuces.AddDbContext<BookStoreDbContext>(config => 
    config.UseSqlServer(Configuration.GetConnectionString("Mssql"));
    // 也可以直接在GetConnectionString()里写连接字符串内容
)
注意: 这里UseSqlServer只有在Microsoft.EntityFrameworkcore.SqlServer包安装后才可以使用

5.生成迁移与创建数据库
// 安装dotnet-ef工具
dotnet tool install -g dotnet-ef
// 生成迁移
dotnet ef migrations add <迁移名称>
// 如果迁移成功就会出现Migrations文件夹

// 创建数据库（同步）

dotnet ef database update
// 创建成功就会在数据库出现表，其中会自带一个名为_EFMigrationsHistory的表，这是存储迁移的历史记录，删除就会导致程序运行失败
注意：

迁移名称最好是英文且首字母大写
使用迁移前需要安装Microsoft.EntityFrameworkcore
需要全局安装dotnet-ef工具
代码不能有错误，否则迁移中会报错
6.添加种子数据
方法一--为种子数据建模
// OnModelCreating.cs
protected override void OnModelCreating(ModelBuilder modelBuider)
{
    base.OnModelCreating(modelBuider);
    modelBuilder.Entity<Author>().HasData(new Author{
        Id = new Guid,
        AuthorName = "小米"
    },
    new Author{
        ......
    }
    );
    modelBuilder.Entity<Book>().HasData(new Book{......});
}
// HasData方法可添加一个或多个相同的实体类型
注意：

通常情况下数据库会自动生成主键值，但在种子数据中，您仍然需要为主键字段指定值
如果以任何方式更改主键，将删除之前设定种子的数据
方法二--手动迁移自定义
解决 HasData 的某些限制的方法之一是改为手动向迁移添加这些调用或自定义操作

// 在Migrations目录下以_SeeData.cs结尾的文件中，包含两个方法Up和Down
//在Up方法中调用迁移生成器对象的InsertData方法
migrationBuilder.InsertData( 
    table: "Authors", //目标表的名称
    columns: new[] { "Url" }, //目标表的名称
    values: new object[] { "http://generated.com" } //要插入列的值
);
// 如果要给全部列插入数据，则直接省略columns 参数
// 直接提供 values 参数中与表中列顺序相对应的数值
方法三--自定义初始化逻辑
在主应用程序逻辑开始执行之前使用 DbContext.SaveChanges()执行数据种子设定

//program.cs
public static void Main(string[] args)
{
    var app = CreateHostBuilder(args).Build();

    // 在应用程序启动时执行数据种子逻辑
    using (var context = new DataSeedingContext())
    {
       context.Database.EnsureCreated();
       // 检查是否存在该数据信息，若不存在则添加该数据
       var testBlog = context.Authors.FirstOrDefault(a => a.AuthorName == "小明");
       if (testBlog == null)
       {
           context.Authors.Add(new Author { AuthorName == "小明" });
       }
   
       context.SaveChanges();
    }
    app.Run();
}