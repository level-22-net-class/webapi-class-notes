## AutoMapper
AutoMapper是一个对象-对象映射器，可以将一个对象映射到另一个对象 

### 注册
1. 安装
```
dotnet add package AutoMapper
```

2. 注入
```
services.AddAutoMapper(type of (Startup));
```

3. Profile
Profile是组织映射的另一种方式，说明要映的射对象以及映射规则
```
// 新建一个名为BookStoreProfile类，继承于Profile
public class BookStoreProfile : Profile
{
    public BookStoreProfile()
    {
        CreateMap<Authors,AuthorDto>();
        CreateMap<Books,BookDto>();
        CreateMap<AuthorCreateDto,Authors>();
        ......
    }
}
```

CreateMap方法的两个泛型参数分别指明对象映射中的源和目标,可以理解第一个参数为源，第二个参数为目标
- 从数据库中获取数据时，实体类为源，DTO为目标
- 增改时，DTO为源，实体类为目标