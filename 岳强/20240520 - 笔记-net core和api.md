# .net core 和 api .net core
## .net core
.NET Core是一个跨平台的开源框架，用于构建现代化、高性能和可扩展的应用程序。它允许开发人员在不同的操作系统上进行开发和部署，包括Windows、macOS和Linux。



## api .net core

API .NET Core是基于.NET Core框架开发的应用程序接口。它提供了一种独立于操作系统的方式来创建和连接不同的软件应用程序，使它们可以相互通信和共享数据。API .NET Core通常用于构建Web服务和微服务，以便其他应用程序可以使用HTTP协议进行通信。

总的来说，.NET Core是一个框架，用于构建各种类型的应用程序，而API .NET Core则是一种特定类型的应用程序接口，用于连接不同的软件应用程序。它们都可以利用.NET Core框架的功能和特性来实现高效、灵活和可扩展的应用程序开发。