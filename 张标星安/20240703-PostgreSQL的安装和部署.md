# Linux下PostgreSql安装部署
### 一、安装PostgreSQl
1. 运行以下命令更新软件包列表
~~~js
sudo apt-get update
~~~
2. 运行以下命令安装PostgreSQ
~~~js
sudo apt install postgresql
~~~
3. 安装完成后，PostgreSQL服务将自动启动，使用以下命令检查它是否正在运行
~~~js
sudo systemctl status postgresql
// 如果PostgreSQL正在运行，将看到“Active: active (running)”的消息
~~~
4. 停止PostgreSQL服务
~~~js
sudo systemctl stop postgresql
~~~

### 二、关于用户和数据库
1. 创建新的PostgreSQL用户，默认情况下，PostgreSQL使用名为“postgres”的超级用户
~~~js
sudo -u postgres createuser --interactive
// 根据提示输入新用户的名称和是否为超级用户
~~~
2. 删除用户
~~~js
DROP ROLE username;
// 如果要删除用户并强制断开连接
DROP ROLE username FORCE;
~~~
3. 创建新的数据库
~~~js
sudo -u postgres createdb dbname
~~~
4. 删除数据库
~~~js
DROP DATABASE dbname;
// 删除数据库并强制断开连接
DROP DATABASE dbname FORCE;
~~~

### 三、登录和访问
#### 1.使用以下命令以超级用户身份登录到PostgreSQL：
~~~js
sudo -u postgres psql
// 登录成功将看到一个以“postgres=#”开头的命令行提示符
// 注：也可以先切换到su postgres，再通过命令：psql
~~~
#### 2.常用pg命令行
1. 在PostgreSQL中创建一个新用户并授予其对新数据库的访问权限
~~~js
CREATE USER username WITH PASSWORD 'password';
// 分号不能少
~~~
2. 查看所有库、查看帮助
~~~js
// 查看库
\l
// 查看帮助
\l?
~~~
3. 退出PostgreSQL
~~~js
\q
~~~
4. 修改密码
~~~js
ALTER USER postgres WITH PASSWORD '新密码';
~~~
5. 切换数据库
~~~js
\c dbname
// 一般数据库名需要加上引号
~~~
6. 显示所有表
~~~js
\dt
~~~
7. 帮助信息：help
8. \h 显示 SQL脚本，\h + 命令，可以查看脚本语法
~~~js
// 例如
\h SELECT
// 会显示以下信息
命令：       SELECT
描述：       从数据表或视图中读取数据
语法：
[ WITH [ RECURSIVE ] with查询语句(with_query) [, ...] ]
SELECT [ ALL | DISTINCT [ ON ( 表达式 [, ...] ) ] ]
    [ * | 表达式 [ [ AS ] 输出名称 ] [, ...] ]
    [ FROM from列表中项 [, ...] ]
    [ WHERE 条件 ]
    [ GROUP BY grouping_element [, ...] ]
    [ HAVING 条件 ]
......省略......
~~~
9. 列出表、视图、序列
~~~js
// 显示出了表、视图等信息
\dS
// 显示了更多信息，包括表存储空间
\dS+
// 描述表，视图，序列，或索引
\dS 表名
~~~

### 四、远程连接配置
#### 1.修改pg_hba.conf文件
~~~js
// 1.进入到所在目录
cd /etc/postgresql/16/main
// 2.查看是否配置远程连接
cat pg_hba.conf
// 3.打开并修改
vim pg_hba.conf
// 4.并添加以下一行
host    all             all             0.0.0.0/0               md5
  /*
     0.0.0.0/0表示允许所有IP访问，md5表示使用密码验证方式
  */
~~~
#### 2.修改postgresql.conf文件
~~~js
// 修改配置项
listen_addresses = '*'
// 该配置项表示监听所有IP地址。
~~~
#### 3.重启PostgreSQL服务
~~~js
sudo systemctl restart postgresql
~~~

### 五、在服务器上安装PostgreSQL的优势
1. 平台稳定性和可靠性： Linux 操作系统通常被认为比 Windows 更稳定和可靠，尤其是在服务器环境中
2. 开源：ostgreSQL 是一个开源数据库管理系统，它在Linux社区中得到广泛支持和使用。Linux和开源软件有着紧密的关系
3. 许多服务器应用程序和工具更倾向于在Linux环境下运行，因此在同一平台上安装 PostgreSQL 可以更容易地实现集成和兼容性

### 六、PgAdmin——管理pg数据库通用工具

[pgadmin4工具安装及使用-CSDN博客](https://blog.csdn.net/tangzongwu/article/details/122165362)
