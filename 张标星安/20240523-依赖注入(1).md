### 依赖注入
依赖注入是一种新的设计模式，通过正确使用依赖注入的相关技术，可以降低系统耦合度，增加系统的可扩展性。

### 实例的生命周期
.NET Core DI 为我们提供的实例生命周其包括三种：
- Transient： 每一次GetService都会创建一个新的实例
- Scoped：  在同一个Scope内只初始化一个实例 ，可以理解为（ 每一个 request级别只创建一个实例，同一个http request会在一个 scope内）
- Singleton ：整个应用程序生命周期以内只创建一个实例 
### 方法
- `Main` 方法：这是应用程序的入口点。它调用 `CreateHostBuilder(args).Build().Run()` 来初始化和启动 Web 主机。
- `CreateHostBuilder` 方法：配置 Web 主机构建器。它使用 `WebHost.CreateDefaultBuilder(args)` 创建默认的构建器，该构建器加载配置（如 `appsettings.json`）并设置日志记录等。然后，它调用 `.UseStartup<Startup>()` 来指定应用程序启动时使用的启动类。
- `Startup` 类：定义了 ASP.NET Core 应用程序的启动逻辑。
- `ConfigureServices` 方法：在服务容器中注册应用程序所需的服务和依赖项。`services.AddControllers();` 添加了 MVC 控制器服务，这是 ASP.NET Core MVC 框架的核心部分。
- `Configure` 方法：配置应用程序的请求处理管道。`app.UseRouting()` 启用路由中间件，它是处理请求 URL 并将其与应用程序中的路由匹配的关键组件。`app.UseEndpoints(endpoints => { endpoints.MapControllers(); })` 设置终结点路由中间件，它负责将请求映射到 MVC 控制器。
- `MapControllers` 方法：一个扩展方法，它告诉终结点路由中间件如何将请求映射到应用程序中的控制器。