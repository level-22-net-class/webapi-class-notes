## 重构控制器（以Aithors为例）
~~~c#
// 注入依赖
private readonly IMapper _mapper;
public AuthorsController(IRepositoryBase<Authors> authorRep,IMapper mapper)
{
    _mapper = mapper;
}
// 获取读者信息(分离版)
// 获取所有作者
[HttpGet]
    public async Task<IActionResult> GetAll()
    {
        var list = await _authorRep.GetAllAsync();
        var authorDtos = _mapper.Map<List<AuthorDto>>(list);
        return Ok(authorDtos);
    }
// 通过id获取单个作者
[HttpGet("{id}")]
    public async Task<IActionResult> Get(Guid id)
    {
        var noId = Guid.Empty;
        if ( id == noId)
        {
            return BadRequest("找不到id");
            // var list = await _authorRep.GetAllAsync();
            // var authorDtos = _mapper.Map<List<AuthorDto>>(list);
            // return Ok(authorDtos);
        }
        else
        {
            var item = await _authorRep.GetByIdAsync(id);
            if (item == null)
            {
                return NotFound();
            }
            var authorDto = _mapper.Map<AuthorDto>(item);
            return Ok(authorDto);
        }
    }
// 添加读者 
[HttpPost]
    public async Task<IActionResult> Post(AuthorCreateDto authorCreateDto)
    {
        // 使用AutoMapper将AuthorCreateDto对象映射为Authors实体
        var author = _mapper.Map<Authors>(authorCreateDto);
        // 为新的作者对象生成一个新的唯一标识符，可以尝试在设置字段时设置自动生成Guid类型的id
        author.Id = Guid.NewGuid();
        var createAuthor = await _authorRep.CreateAsync(author);
        if(createAuthor == null)
        {
            return NotFound();
        }
        // 使用AutoMapper将创建后的Authors实体映射为AuthorDto
        var authorDto = _mapper.Map<AuthorDto>(createAuthor);
        return Ok(authorDto);
    }
// 修改读者
[HttpPut("{id}")]
    public async Task<IActionResult> Put(Guid id, AuthorUpdateDto authorUpdateDto)
    {
        var author = await _authorRep.GetByIdAsync(id);
        if(author == null)
        {
            return NotFound();
        }
        // 使用AutoMapper将authorUpdateDto的属性映射到author对象上
        // 注意：他们俩的属性名要一致
        _mapper.Map(authorUpdateDto,author);
        var updateAuthor = await _authorRep.UpdateAsync(author);
        if(updateAuthor == null){
            return NotFound();
        }
        // 将更新后的作者对象映射为AuthorDto
        var authorDto = _mapper.Map<AuthorDto>(updateAuthor);
        return Ok(authorDto);
    }