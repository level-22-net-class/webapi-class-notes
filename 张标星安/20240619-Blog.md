## 博客表考前梳理
### 后端webapi
1. 配置入口文件Program.cs
2. 配置启动类，注册依赖Startup.cs
3. 创建封装表格Domain/Blog.cs
4. 创建表格数据实体类Dto：BlogCreateDto、BlogDto、BlogUpdateDto
5. 在配置文件中编写数据库连接字符串
~~~js
 "ConnectionStrings": {
    "SqlServer": "server=.;database=BlogDemo;uid=sa;pwd=123456;TrustServerCertificate=true;"
  }
~~~
6. 创建数据库上下文，连接数据库Db/BlogsDbContext
7. 创建通用仓储接口Interfaces/IRepositoryBase.cs
8. 实现接口Services/RepositoryBase.cs
9. 编写控制器
10. 生成迁移和同步数据库
~~~js
// 全局安装dotnet-ef工具
dotnet tool install --global dotnet-ef
// 生成迁移
dotnet ef migrations add <Name>
// 同步数据库
dotnet ef batadase update
~~~