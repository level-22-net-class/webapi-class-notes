```bash
Uncaught (in promise) RangeError: Invalid array length
    at renderList (vue.js?v=a122b8ba:3373:11)
    at Proxy._sfc_render (App.vue:1:1)
    at renderComponentRoot (vue.js?v=a122b8ba:2366:17)
    at ReactiveEffect.componentUpdateFn [as fn] (vue.js?v=a122b8ba:6683:26)
    at ReactiveEffect.run (vue.js?v=a122b8ba:438:19)
    at instance.update (vue.js?v=a122b8ba:6736:17)
    at callWithErrorHandling (vue.js?v=a122b8ba:1675:33)
    at flushJobs (vue.js?v=a122b8ba:1888:9)
renderList @ vue.js?v=a122b8ba:3373
_sfc_render @ App.vue:1
renderComponentRoot @ vue.js?v=a122b8ba:2366
componentUpdateFn @ vue.js?v=a122b8ba:6683
run @ vue.js?v=a122b8ba:438
instance.update @ vue.js?v=a122b8ba:6736
callWithErrorHandling @ vue.js?v=a122b8ba:1675
flushJobs @ vue.js?v=a122b8ba:1888
Promise.then（异步）
queueFlush @ vue.js?v=a122b8ba:1798
queueJob @ vue.js?v=a122b8ba:1792
（匿名） @ vue.js?v=a122b8ba:6730
resetScheduling @ vue.js?v=a122b8ba:519
trigger @ vue.js?v=a122b8ba:666
set @ vue.js?v=a122b8ba:787
set @ vue.js?v=a122b8ba:3737
fetchBlogs @ App.vue:68
await in fetchBlogs（异步）
mounted @ App.vue:62
（匿名） @ vue.js?v=a122b8ba:3285
callWithErrorHandling @ vue.js?v=a122b8ba:1675
callWithAsyncErrorHandling @ vue.js?v=a122b8ba:1682
hook.__weh.hook.__weh @ vue.js?v=a122b8ba:3265
flushPostFlushCbs @ vue.js?v=a122b8ba:1858
render2 @ vue.js?v=a122b8ba:7275
mount @ vue.js?v=a122b8ba:4524
app.mount @ vue.js?v=a122b8ba:11142
（匿名） @ main.js:5
显示另外 22 个框架
收起
```
这个错误表明在渲染列表时，totalPages 的计算可能出错，导致 v-for 指令生成的数组长度无效。我们需要仔细检查 totalPages 的计算方式，并确保它是一个有效的整数。