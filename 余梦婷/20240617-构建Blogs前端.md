## 准备工作和基础设置
- 安装 Vue CLI
  - Vue CLI是一个官方的脚手架工具，由于快速搭建Vue.js项目。
- ```bash
        npm install -g @vue/cli
       ```
- 创建项目
  - 使用 Vue CLI 创建一个新的 Vue.js 项目。
  - ```bash
    yarn create vite
    cd [项目名]
    yarn 
    yarn dev
  ```
  
## 主要文件和目录结构
```plaintext
vue-blog-frontend/
├── public/
│   └── index.html
├── src/
│   ├── assets/
│   ├── components/
│   ├── views/
│   ├── App.vue
│   └── main.js
├── package.json
└── README.md
```
- public/：包含 index.html 和其他静态资源。
- src/：主要的开发目录。
  - assets/：存放静态资源如图片、样式表等。
  - components/：Vue 组件。
  - views/：视图组件，通常与路由对应。
  - App.vue：根组件。
  - main.js：入口文件，初始化 Vue 应用。
## 使用 Vue.js 和 Axios 进行 API 调用
### 安装 Axios
Axios 是一个常用的 HTTP 客户端库，用于在 Vue 中进行 API 调用。

```bash
  npm install axios
```

### 在 Vue 组件中使用 Axios
```vue
<script>
import axios from 'axios';

export default {
  data() {
    return {
      blogs: []
    };
  },
  mounted() {
    axios.get('http://localhost:5000/api/blogs')
      .then(response => {
        this.blogs = response.data;
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });
  }
};
</script>
```

## 路由和导航
### 安装 Vue Router
Vue Router 是 Vue.js 的官方路由管理器。
```bash
yarn add vue-router
```
### 配置和使用路由
```js
// main.js
import Vue from 'vue';
import App from './App.vue';
import router from './router';

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');

// router/index.js
import Vue from 'vue';
import VueRouter from 'vue-router';
import BlogList from '../views/BlogList.vue';
import BlogDetail from '../views/BlogDetail.vue';

Vue.use(VueRouter);

const routes = [
  { path: '/', component: BlogList },
  { path: '/blog/:id', component: BlogDetail, props: true }
];

const router = new VueRouter({
  routes
});

export default router;
```
## 解决跨域问题
## 1. 后端配置 CORS
在 ASP.NET Core 中，可以通过配置 CORS（跨域资源共享）来允许特定的前端域名访问后端 API。
```C#
// Startup.cs

public void ConfigureServices(IServiceCollection services)
{
    services.AddCors(options =>
    {
        options.AddPolicy("VueCorsPolicy", builder =>
        {
            builder.WithOrigins("http://localhost:8080") // Vue.js 的开发服务器地址
                   .AllowAnyMethod()
                   .AllowAnyHeader();
        });
    });

    // 其他配置服务代码
}

public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
{
    // 其他中间件配置

    app.UseCors("VueCorsPolicy");

    // 其他配置代码
}
```
上面的示例中，我们定义了一个名为 "VueCorsPolicy" 的 CORS 策略，允许来自 http://localhost:8080 的请求，并允许任意 HTTP 方法和请求头。在 Configure 方法中使用 `app.UseCors("VueCorsPolicy")` 将 CORS 中间件添加到请求处理管道中。
### 2. Vue CLI 代理
在开发阶段，通常使用 Vue CLI 提供的代理功能来解决跨域问题。通过配置 `vue.config.js` 文件，可以将前端开发服务器代理到后端 API 服务器，从而避免浏览器的跨域限制。
```js
// vue.config.js

module.exports = {
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:5000', // 后端 API 的地址
        changeOrigin: true,
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  }
};
```
### 3. 使用跨域 HTTP 请求库
如果以上方法仍然遇到问题，可以考虑使用支持跨域请求的 HTTP 库，如 `axios`。在请求中添加 `withCredentials: true` 可以处理一些复杂的跨域情况。
```js
axios.get('http://localhost:5000/api/blogs', { withCredentials: true })
  .then(response => {
    console.log(response.data);
  })
  .catch(error => {
    console.error('Error fetching data:', error);
  });
```