## Entity Framework Core 简介

**Entity Framework Core** 是一个面向对象的关系映射（ORM）框架，提供了开发人员使用 .NET 对象与数据库交互的能力。EF Core 支持多种数据库提供程序，如 SQL Server、SQLite、MySQL 等。

**特点**：

- **跨平台**：支持 Windows、macOS 和 Linux。
- **高性能**：比 EF6 更轻量和高效。
- **灵活**：支持代码优先和数据库优先的开发模式。

## 安装与配置

### 安装 EF Core 包

在项目中安装 EF Core 及其数据库提供程序（以 SQL Server 为例）：

```sh
dotnet add package Microsoft.EntityFrameworkCore
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
```

### 配置数据库上下文

在 `Startup.cs` 中配置数据库上下文：

```csharp
public void ConfigureServices(IServiceCollection services)
{
    services.AddDbContext<ApplicationDbContext>(options =>
        options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
    services.AddControllers();
}
```

在 `appsettings.json` 中添加连接字符串：

```json
{
  "ConnectionStrings": {
    "DefaultConnection": "Server=(localdb)\\mssqllocaldb;Database=MyDatabase;Trusted_Connection=True;"
  }
}
```

## 数据模型

定义实体类来表示数据库表。以下是一个简单的 `Product` 实体类示例：

```csharp
public class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    public decimal Price { get; set; }
}
```

## 数据库上下文

数据库上下文类继承自 `DbContext`，并包含 `DbSet<T>` 属性来表示数据库表。

```csharp
public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    public DbSet<Product> Products { get; set; }
}
```

## 基本 CRUD 操作

### 创建（Create）

```csharp
public async Task<IActionResult> Create(Product product)
{
    _context.Products.Add(product);
    await _context.SaveChangesAsync();
    return CreatedAtAction(nameof(GetById), new { id = product.Id }, product);
}
```

### 读取（Read）

#### 获取所有记录

```csharp
public async Task<ActionResult<IEnumerable<Product>>> GetAll()
{
    return await _context.Products.ToListAsync();
}
```

#### 根据 ID 获取记录

```csharp
public async Task<ActionResult<Product>> GetById(int id)
{
    var product = await _context.Products.FindAsync(id);
    if (product == null)
    {
        return NotFound();
    }
    return product;
}
```

### 更新（Update）

```csharp
public async Task<IActionResult> Update(int id, Product product)
{
    if (id != product.Id)
    {
        return BadRequest();
    }

    _context.Entry(product).State = EntityState.Modified;
    await _context.SaveChangesAsync();

    return NoContent();
}
```

### 删除（Delete）

```csharp
public async Task<IActionResult> Delete(int id)
{
    var product = await _context.Products.FindAsync(id);
    if (product == null)
    {
        return NotFound();
    }

    _context.Products.Remove(product);
    await _context.SaveChangesAsync();

    return NoContent();
}
```

## 数据迁移

EF Core 提供数据迁移功能，用于创建和更新数据库结构。

### 添加迁移

```sh
dotnet ef migrations add InitialCreate
```

### 更新数据库

```sh
dotnet ef database update
```

## 高级主题

### 关系和导航属性

EF Core 支持多种关系类型：一对多、多对多和一对一。

#### 一对多关系示例

```csharp
public class Category
{
    public int Id { get; set; }
    public string Name { get; set; }
    public ICollection<Product> Products { get; set; }
}

public class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    public decimal Price { get; set; }
    public int CategoryId { get; set; }
    public Category Category { get; set; }
}
```

### LINQ 查询

EF Core 支持使用 LINQ 进行查询。

#### 示例查询

```csharp
var expensiveProducts = await _context.Products
    .Where(p => p.Price > 100)
    .ToListAsync();
```

### 延迟加载与急切加载

#### 延迟加载

延迟加载（Lazy Loading）在访问导航属性时才加载相关数据。

```csharp
public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
        this.ChangeTracker.LazyLoadingEnabled = true;
    }

    public DbSet<Product> Products { get; set; }
    public DbSet<Category> Categories { get; set; }
}
```

#### 急切加载

急切加载（Eager Loading）在查询时使用 `Include` 方法加载相关数据。

```csharp
var productsWithCategories = await _context.Products
    .Include(p => p.Category)
    .ToListAsync();
```

## 示例项目

### 项目结构

```
├── Controllers
│   └── ProductController.cs
├── Models
│   └── Product.cs
│   └── Category.cs
├── Data
│   └── ApplicationDbContext.cs
├── Program.cs
└── Startup.cs
```

### 创建数据模型

```csharp
public class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    public decimal Price { get; set; }
    public int CategoryId { get; set; }
    public Category Category { get; set; }
}

public class Category
{
    public int Id { get; set; }
    public string Name { get; set; }
    public ICollection<Product> Products { get; set; }
}
```

### 配置数据库上下文

```csharp
public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    public DbSet<Product> Products { get; set; }
    public DbSet<Category> Categories { get; set; }
}
```

### 配置依赖注入

在 `Startup.cs` 中配置依赖注入：

```csharp
public void ConfigureServices(IServiceCollection services)
{
    services.AddDbContext<ApplicationDbContext>(options =>
        options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
    services.AddControllers();
}
```

### 创建控制器

```csharp
[Route("api/[controller]")]
[ApiController]
public class ProductController : ControllerBase
{
    private readonly ApplicationDbContext _context;

    public ProductController(ApplicationDbContext context)
    {
        _context = context;
    }

    [HttpPost]
    public async Task<IActionResult> Create(Product product)
    {
        _context.Products.Add(product);
        await _context.SaveChangesAsync();
        return CreatedAtAction(nameof(GetById), new { id = product.Id }, product);
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<Product>>> GetAll()
    {
        return await _context.Products.Include(p => p.Category).ToListAsync();
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<Product>> GetById(int id)
    {
        var product = await _context.Products.Include(p => p.Category).FirstOrDefaultAsync(p => p.Id == id);
        if (product == null)
        {
            return NotFound();
        }
        return product;
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> Update(int id, Product product)
    {
        if (id != product.Id)
        {
            return BadRequest();
        }

        _context.Entry(product).State = EntityState.Modified;
        await _context.SaveChangesAsync();

        return NoContent();
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(int id)
    {
        var product = await _context.Products.FindAsync(id);
        if (product == null)
        {
            return NotFound();
        }

        _context.Products.Remove

(product);
        await _context.SaveChangesAsync();

        return NoContent();
    }
}
```

### 配置路由和中间件

在 `Startup.cs` 中配置中间件和路由：

```csharp
public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
{
    if (env.IsDevelopment())
    {
        app.UseDeveloperExceptionPage();
    }

    app.UseHttpsRedirection();

    app.UseRouting();

    app.UseAuthorization();

    app.UseEndpoints(endpoints =>
    {
        endpoints.MapControllers();
    });
}
```

## 补充说明和注解

1. **连接字符串安全性**：在生产环境中，避免将数据库连接字符串明文存储在 `appsettings.json` 文件中。建议使用环境变量或 Azure Key Vault 等安全存储方式。
2. **数据库上下文生命周期**：确保数据库上下文的生命周期与请求的生命周期相同。推荐使用依赖注入的 `AddDbContext` 方法。
3. **性能优化**：在大数据量操作时，考虑使用分页、批量操作和数据库索引来优化性能。
4. **迁移管理**：定期备份数据库，并在应用迁移前测试迁移脚本，以防止数据丢失或损坏。