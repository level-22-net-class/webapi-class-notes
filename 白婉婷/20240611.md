## 一、创建通用的仓储接口，定义通用的CRUD功能
~~~c#
using BookStore.Api.Interface;
// where T:class 是约束，表示 T 必须是引用类型（即类）
// 这里的T类似占位符，代表了使用接口时会传入的任何引用类型的类
public interface IRepositoryBase<T>where T:class
{
    // 获取所有模型
    List<T> GetAll();
    // 根据id获取指定模型（也就是数据库中的表）
    T? GetById(Guid id);
    // 新增
    T Create(T entity);
    // 修改
    T Update(T entity);
    // 删除
    /*这里采用了方法重载，可以根据需求选择不同的方式来删除数据
        一个接受实体对象作为参数
        一个接受id作为参数
    */
    T Delete(T entity);
    T? Delete(Guid id);
}
~~~

## 二、实现通用仓储接口
~~~c#
 public class RepositotyBase<T>:IRepositoryBase<T>where T:class
 {
     private readonly BookStoreDbContext _db;
     private readonly DbSet<T> _tb;
     public RepositotyBase(BookStoreDbContext db)
     {
         _db = db;
         _tb = db.Set<T>();
     }
 //-------------------获取全部信息---------------------
     public List<T> GetAll()
     {
         var list = _tb.ToList();
         return list;
     }
 //-------------------获取指定id信息---------------------
     public T? GetById(Guid id)
     {
         var entity = _tb.Find(id);
          if(entity == null){
             return null;
         }
         return entity;
     }
 //-------------------------新增-------------------------
     public T Create(T entity)
     {
         _tb.Add(entity);
         _db.SaveChanges();
         return entity;
     }
 //-------------------------修改-------------------------
     public T? Update(Guid id,T entity)
     {
         var author = _tb.Find(id);
         if(author == null){
             return null;
         }
         _tb.Update(entity);
         _db.SaveChanges();
         return entity;
     }
 //-------------------------删除-------------------------
     public T Delete(T entity)
     {
         _tb.Remove(entity);
         _db.SaveChanges();
         return entity;
     }
     public T? Delete(Guid id)
     {
         var entity = _tb.Find(id);
         if(entity == null){
             return null;
         }
         _tb.Remove(entity);
         _db.SaveChanges();
         return entity;
     }
 }
~~~
**注意：**
 EFCore对于查询采用延迟执行方法，当在程序中使用LINQ查询时，此时查询命令只是存储在变量中，并未实际操作，只有遇到以下几种操作才会执行查询，并返回给存储的变量
 - 对结果使用for或foreach循环
 - ToList()、ToArray()和ToDictionary()等方法
 - Single()、Count()、Average、First()和Max()等方法

## 三、注入通用仓储接口
~~~c#
// 注册通用仓储接口
services.AddScoped(typeof(IRepositoryBase<>),typeof(RepositotyBase<>));
~~~

## 四、控制器重写
~~~c#
 public class AuthorsController : ControllerBase
 {
 // 注入依赖
   private readonly IRepositoryBase<Authors> _authorRep;
   public AuthorsController(IRepositoryBase<Authors> authorRep)
   {
     _authorRep = authorRep;
   }
 // -----------------获取------------------------------------------
     [HttpGet("{id?}")]
     public IActionResult Get(Guid id)
    {
         var noId = "00000000-0000-0000-0000-000000000000";
         if(id.ToString()==noId){
             var list = _authorRep.GetAll();
             return Ok(list);
         }else{
             var item = _authorRep.GetById(id);
             return Ok(item);
         }
    }
 // -----------------添加------------------------------------------
    [HttpPost]
    public IActionResult Post(AuthorCreateDto authorCreateDto)
    {
         var author = new Authors{
             Id = Guid.NewGuid(),
             AuthorName = authorCreateDto.AuthorName,
             Gender = authorCreateDto.Gender,
             Birthday = authorCreateDto.Birthday
         };
         var res = _authorRep.Create(author);
         return Ok(res);
    }
 // -----------------修改------------------------------------------
    [HttpPut("{id}")]
    public IActionResult Put(Guid id,AuthorUpdateDto authorUpdateDto)
    {
         var author = new Authors{
             AuthorName = authorUpdateDto.AuthorName,
             Gender = authorUpdateDto.Gender,
             Birthday = authorUpdateDto.Birthday
         };
         var res = _authorRep.Update(id,author);
         return Ok(res);
    }
 // -----------------删除------------------------------------------
    [HttpDelete("{id}")]
    public IActionResult Del(Guid id)
    {
        var res = _authorRep.Delete(id);
        return Ok(res);
    }
 }
~~~

## 五、EF Core流程图
~~~js
@startuml
  :安装EF Core核心依赖包;
  note right
    Microsoft.EntityFrameworkCore
  end note
  :安装数据库驱动包;
  note right
    * 例如Microsoft.EntityFrameworkCore.SqlServer
    * 目前支持主流的数据库如MsSql、Mysql、PostgreSQL、Oracle、Sqlite、MongoDB
  end note
  :创建实体类;
  repeat :创建数据库上下文;
   :生成迁移文件;
   note right
    * 安装dotnet-ef工具
    dotnet tool install -g dotnet-ef
    * 生成迁移
    dotnet ef migrations add <迁移名称>
  end note
  repeat while (同步数据库)
@enduml
~~~
![EFCore流程图-2024-6-1220:36:41.png](https://gitee.com/huangxuefang0929/xiu_img/raw/master/EFCore%E6%B5%81%E7%A8%8B%E5%9B%BE-2024-6-1220:36:41.png)