## 添加EF Cpre

        dotnet add package Microsoft.EntityFrameworkCore

## 添加驱动包

        dotnet add package Microsoft.EntityFrameworkCore.SqlServer

## Json文件中添加

        {
            "ConnectionStrings": {
                "AuthorDbConnection": "Server=Author;Database=BookStoreDb;User Id=sa;Password=123456;"
            }
        }

## 注册数据库上下文到容器 链接字符串
- Db-AuthorDbContext.cs
- Startup.cs
  
## DbSet 公共形式

## 数据迁移
- dotnet add package Microsoft.EntityFrameworkCore.Design
- dotnet ef migrations add Firstii
- ef migrations remove 撤销迁移
- dotnet ef database update 数据更新

## 安装更新 .NET EF
- dotnet tool install -g dotnet-ef 安装
- dotnet tool update -g dotnet-ef 更新到最新版本

## ssql查询
- select * from __EFMigrationsHistory 迁移生成记录
- select * from Authors

