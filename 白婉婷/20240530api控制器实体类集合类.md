## api接口层面的控制器
- 完成了第一个阶段的api搭建， 第五阶段Restfull风格的控制器和路由 Controllers目录下的文件
  - AuthorsController.cs

        using Microsoft.AspNetCore.Mvc;
        using BookStoreApi.Db;
        using BookStoreApi.Domain;
        namespace BookStoreApi.AddControllers;
        [ApiController]
        [Route(("api/[controller]"))]
        public class AuthorsController : ControllerBase
        {
            [HttpGet("{id?}")]
            public IActionResult Get(int id)
            {
                var db = new BookStoreDb();
                db.Authors.Add(new Authors{AuthorName="一",Gender=1,Birthday=Convert.ToDateTime("1999-03-01")});
                var list =db.Authors.ToList();
                return Ok(list);
            }
            [HttpPost]
            public IActionResult Post()
            {
                return Ok();
            }
      
            [HttpPut("{id}")]
            public IActionResult Put(int id)
            {
                return Ok(id);
            }
      
            [HttpDelete("{id}")]
            public IActionResult Del(int id)
            {
                return Ok(id);
            }
        }

  - BooksController.cs

        using Microsoft.AspNetCore.Mvc;
        namespace BookStoreApi.AddControllers;
        [ApiController]
        [Route("api/authors/{authorId}/[controller]/")]
        public class BooksController : ControllerBase
        {
            [HttpGet("{bookId?}")]
            public IActionResult Get(int authorId, int bookId)
            {
                return Ok(new { authorId, bookId });
            }
            [HttpPost]
            public IActionResult Post(int authorId)
            {
                return Ok();
            }
            [HttpPut("{id}")]
            public IActionResult Put(int authorId,int id)
            {
                return Ok(id);
            }
            [HttpDelete("{id}")]
            public IActionResult Delete(int authorId,int id)
            {
                return Ok(id);
            }
        }

## Domain 实体类
- 模型设计-图书管理系统
  - Authors.cs

            using System.Data;
            namespace BookStoreApi.Domain;
            public class Authors
            {
                public Guid Id {get; set;}
                public string AuthorName { get; set; } =null!;
                public int Gender { get; set; }
                public DateTime Birthday { get; set; }
            }

  - Books.cs

        namespace BookStoreApi.Domain;
        public class Books
        {
            public Guid Id { get; set; }
            public string BookName { get; set; } =null!;
            public string? Publisher { get; set;} 
            public int Price { get; set;}
        }
## Db内存型数据库 自定义集合类
- bookStoreDb.cs

        using BookStoreApi.Domain;
        namespace BookStoreApi.Db;
        public class BookStoreDb
        {
            public static BookStoreDb Instance { get; set; } = new BookStoreDb();
            public ICollection<Authors> Authors { get; set; } = new List<Authors>{new Authors()};
            public ICollection<Books> Books { get; set; } = new List<Books>{new Books()};
        }
