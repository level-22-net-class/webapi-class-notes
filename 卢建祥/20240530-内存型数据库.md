## 内存型数据库
- 创建一个Db对象
```csharp
    public static BookDtoreDb Instance{get;set;}=new BookStoreDb();
    public Icollection<实例对象1> 名1 {get;set;}=new List<实例对象1>();
    public Icollection<实例对象2> 名2 {get;set;}=new List<实例对象2>();
    //初始化数据
    public BookStoreDb()
    {
        名1.Add(new 实例名{
            字段1=值1,
            字段2=值2
        })
    }
```

### 集合的操作
> 较多使用：ICollection、IList、IComparer

![alt text](./imgs/i..Repository.png)

### 各类文件夹的代指
- `Db`指的是内存型数据库，自己创建的数据库
- `Domain`指的是实体类对象，用来放字段
- `interfaces`指的是接口类对象（`I`实例名`Repository`结尾）
- `Services`是接口的实现类（实例名`Repository`结尾）