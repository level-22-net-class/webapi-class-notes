## api创建控制器
- Program
```cs
namespace Admin.api;

public static class Program
{
    public static void Main(string[] args)
    {
        CreateHostBuilder(args).Build().Run();
    }
    public static IHostBuilder CreateHostBuilder(string[] args)
    {
        return Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(builder=>
        {
            builder.UseStartup<Startup>();
        });
    }
}
```

- Startup
```cs
namespace Admin.api;

public class Startup
{
    public void Configure(IApplicationBuilder app)
    {
        app.UseRouting();
        app.UseEndpoints(endpoints =>{
            endpoints.MapControllers();
        });
    }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllers();
    }
}
```

- BlogController
```cs
using Microsoft.AspNetCore.Mvc;

namespace Admin.api;

[Route("[controller]")]

public class BlogsController:ControllerBase
{
    public IActionResult Index()
    {
        return Ok("会了吗？？？");
    }
}
```