## EF Core数据库

### 创建一个通用的CRUD接口
```csharp
public interface IRepositoryBase<T>
{
    T GetById(Guid id);
    List<T> GetAll();
    T Insert(T entity);
    T Update(T entity);
    T Delete(T entity);
    T Delete(Guid id);
}
```

### EF Core数据库
1. 安装的依赖包
- dotnet add package Mircosoft.EntityFrameworkCore
- dotnet add package Mircosoft.EntityFrameworkCore.SqlServer
- dotnet add package Mircosoft.EntityFrameworkCore.Design
2. 创建一个`名StoreDbContext.cs`
```csharp
using BookStore.Api.Domain;
using Microsoft.EntityFrameworkCore;

namespace BookStore.Api.Db;

public class BookStoreDbContext : DbContext
{
    public BookStoreDbContext(DbContextOptions<BookStoreDbContext> options) : base(options)
    {

    }


    public DbSet<Authors> Authors { get; set; }
    public DbSet<Books> Books { get; set; }
}
```
3. Startup
```csharp
    public void ConfigureServices(IServiceCollection services)
    {
        // 将swagger端点服务和界面服务注册到容器
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen();

        // 将控制器服务注册到容器
        services.AddControllers();

        // 将仓储服务注册到容器
        services.AddScoped<IAuthorRepository, AuthorRepository>();
        services.AddScoped<IBookRepository, BookRepository>();

        // 注册数据库上下文到容器
        services.AddDbContext<BookStoreDbContext>(
            option =>
            option.UseSqlServer(_configuration.GetConnectionString("Mssql"))
            //"server=.;database=BookStore;uid=sa;pwd=123456;TrustServerCertificate=true";
        );
    }
```
4. 更新同步迁移命令
- 更新ef工具包版本：dotnet  tool install -g dotnet-ef
- 生成迁移文件：dotnet ef migrations add FirstMo