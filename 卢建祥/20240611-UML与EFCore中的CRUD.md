## UML与EFCore中的CRUD
### plantUML
![Alt text](imgs/EFCoreplantUML.png)

### EFCore中的CRUD
1. RepositoryBase.cs
```csharp
using Admin.Api.Interfaces;
using Admin.Api.Db;
using Admin.Api.Domain;

namespace Admin.Api.Services;

public class AuthorRepository<T> : IRepositoryBase<T> where T : class
{
    private readonly StoreDbContext _db;
    public AuthorRepository(StoreDbContext db)
    {
        _db = db;
    }
    public List<T>? GetAll()
    {

        var result = _db.Set<T>().ToList();
        return result;
    }
    public T? GetById(Guid id)
    {
        var list = _db.Set<T>().Find(id);
        return list;
    }
    public T Post(T entity)
    {
        _db.Set<T>().Add(entity);
        _db.SaveChanges();
        return entity;
    }
    public T? Put(Guid id, T entity)
    {
        var data = _db.Set<T>().Find(id);
        if (data == null)
        {
            return null;
        }
        _db.Set<T>().Update(entity);
        _db.SaveChanges();
        return data;
    }
    public T? Del(Guid id)
    {
        var data = _db.Set<T>().Find(id);
        if (data != null)
        {
            _db.Set<T>().Remove(data);
            _db.SaveChanges();
        }
        return data;
    }
}
```