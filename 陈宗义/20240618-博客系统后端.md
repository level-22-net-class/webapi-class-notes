## 代码
#### Program.cs
```c#
using Microsoft.AspNetCore;
namespace Blog;
public class Program
{
    // 应用程序主入口
    public static void Main(string[] args)
    {
        // 创建web主机
        CreateWebHostBuilder(args).Build().Run();
    }
    // 配置web主机
    public static IWebHostBuilder CreateWebHostBuilder(string[] args)
    {
        // 创建默认的启动类
        return WebHost.CreateDefaultBuilder(args).UseStartup<Startup>();
    }
}
```
#### Startup.cs
```c#
using Blog.Db;
using Blog.Helper;
using Blog.Interface;
using Blog.Service;
using Microsoft.EntityFrameworkCore;
namespace Blog;
public class Startup
{
   private readonly IConfiguration _configuration;
   public Startup(IConfiguration configuration)
   {
      _configuration = configuration;
   }
   public void Configure(IApplicationBuilder app)
   {
      // 使用路由中间件
      app.UseRouting();
      // 使用跨域中间件
      app.UseCors();
      // 使用请求管道终结点激活控制器
      app.UseEndpoints(endpoint => endpoint.MapControllers());
   }
   public void ConfigureServices(IServiceCollection services)
   {
      // 添加控制器服务
      services.AddControllers();
      // 添加控制器服务
      services.AddCors(options =>
      {
         options.AddDefaultPolicy(policy =>
         {
            policy.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin();
         });
      });
      // 添加数据库服务
      services.AddDbContext<BlogContext>(options => options.UseSqlServer(_configuration.GetConnectionString("Mssql")));
      // 将通用性仓储接口和实现类注入容器
      services.AddScoped(typeof(IRepostory<>),typeof(Repostory<>));
      // 注册AutoMapper
      services.AddAutoMapper(typeof(BlogAutoMapperFilter));
   }
}
```
#### DbContext
```c#
using Blog.Model;
using Microsoft.EntityFrameworkCore;
namespace Blog.Db;
public class BlogContext : DbContext
{
    public BlogContext(DbContextOptions<BlogContext> options) :base(options) { }
    DbSet<Blogs> blogs {get;set;}
}
```
#### Model
```c#
namespace Blog.Model;
public class Blogs
{
    public int Id { get; set; }
    public string Title {get;set;}=null!;
    public string? Author {get;set;}
    public string? Flag {get;set;}
}
```
#### DTO 与模型一样，想得到什么数据，写什么数据
#### 通用型仓储接口
```c#
using Microsoft.EntityFrameworkCore;
namespace Blog.Interface;
public interface IRepostory<T> where T : class
{
   DbSet<T> Table {get;}
   // 查询全部博客
   List<T> GetAll();
   // 根据id查询部分博客
   T? GetById(int Id);
   // 添加博客
   T Post(T entity);
   // 修改博客
   T Put(T entity);
   // 删除博客
   List<T>? Delete(int id);
}
```
#### 接口实现类
```c#
using Blog.Db;
using Blog.Interface;
using Microsoft.EntityFrameworkCore;
namespace Blog.Service;
public class Repostory<T> : IRepostory<T> where T : class
{
    // 声明了一个名为_db的私有只读字段,用于表示数据库上下文
    private readonly BlogContext _db;
    // 用于表示数据库表的一个集合的类型，其中T是数据库表对应的实体类型
    private readonly DbSet<T> _tb;
    public Repostory(BlogContext db)
    {
        _db = db;
        // 这行代码调用了BookStoreContext的Set<T>()方法，它返回一个DbSet<T>对象，代表数据库中的一个表，并将这个对象赋值给_tb字段
        _tb = db.Set<T>();
    }
    public DbSet<T> Table { get { return _tb; } }

    public List<T>? Delete(int id)
    {
        var temp = _tb.Find(id);
        if(temp == null){
            return null;
        }
        _tb.Remove(temp);
        _db.SaveChanges();
        var list = _tb.ToList();
        return list;
    }
    public List<T> GetAll()
    {
        var list = _tb.ToList();
        return list;
    }
    public T? GetById(int Id)
    {
        var temp = _tb.Find(Id);
        if(temp == null){
            return null;
        }
        return temp;
    }
    public T Post(T entity)
    {
        _tb.Add(entity);
        _db.SaveChanges();
        return entity;
    }
    public T Put(T entity)
    {
       _tb.Update(entity);
       _db.SaveChanges();
       return entity;
    }
}
```
#### AutoMapper
```c#
using AutoMapper;
using Blog.Dto;
using Blog.Model;
namespace Blog.Helper;
public class BlogAutoMapperFilter : Profile
{
    public BlogAutoMapperFilter()
    {
        CreateMap<Blogs, BlogGetDto>();
        CreateMap<Blogs, BlogCreateDto>();
        CreateMap<Blogs, BlogPutDto>();
        CreateMap<BlogGetDto,Blogs>();
        CreateMap<BlogCreateDto,Blogs>();
        CreateMap<BlogPutDto,Blogs>();
    }
}
```
#### Controller控制器
```c#
using System.Linq;
using AutoMapper;
using Blog.Dto;
using Blog.Interface;
using Blog.Model;
using Microsoft.AspNetCore.Mvc;
namespace Blog.Controller;
[ApiController]
[Route("/api/[controller]")]
public class BlogController : ControllerBase
{
    // 依赖注入通用性仓储接口
    private readonly IRepostory<Blogs> _blog;
    private readonly IMapper _mapper;
    public BlogController(IRepostory<Blogs> blog, IMapper mapper)
    {
        _blog = blog;
        _mapper = mapper;
    }
    /// <summary>
    /// 查询所有博客
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public ActionResult<IEnumerable<BlogGetDto>> GetAll()
    {
        var list = _blog.GetAll();
        var result = _mapper.Map<IEnumerable<BlogGetDto>>(list);
        return Ok(result);
    }
    /// <summary>
    /// 模糊查询含有关键词的博客
    /// </summary>
    /// <param name="keyword"></param>
    /// <returns></returns>
    [HttpGet("search")]
    public ActionResult<IEnumerable<BlogGetDto>> GetLike(string keyword ="")
    {
        if (keyword == "")
        {
            var list = _blog.GetAll();
            var result = _mapper.Map<IEnumerable<BlogGetDto>>(list);
            return Ok(result);
        }
        var listlinq = _blog.Table.Where(item => item.Title.Contains(keyword) || (item.Author ?? string.Empty).Contains(keyword) || (item.Flag ?? string.Empty).Contains(keyword)).ToList();
        if(listlinq.Count == 0){
            return Ok(new List<BlogGetDto>());
        }
        var res = _mapper.Map<IEnumerable<BlogGetDto>>(listlinq);
        return Ok(res);
    }
    /// <summary>
    /// 根据id查询博客
    /// </summary>
    /// <param name="blogId"></param>
    /// <returns></returns>
    [HttpGet("{blogId}")]
    public ActionResult<BlogGetDto> GetById(int blogId)
    {
        var item = _blog.GetById(blogId);
        var temp = _mapper.Map<BlogGetDto>(item);
        return Ok(temp);
    }
    /// <summary>
    /// 新增博客
    /// </summary>
    /// <param name="blogCreateDto"></param>
    /// <returns></returns>
    [HttpPost]
    public ActionResult<BlogGetDto> Post(BlogCreateDto blogCreateDto)
    {
        var entity = _mapper.Map<Blogs>(blogCreateDto);
        var result = _mapper.Map<BlogGetDto>(_blog.Post(entity));
        return Ok(result);
    }
    /// <summary>
    /// 修改博客
    /// </summary>
    /// <param name="blogId"></param>
    /// <param name="blogPutDto"></param>
    /// <returns></returns>
    [HttpPut("{blogId}")]
    public ActionResult<BlogGetDto> Put(int blogId, BlogPutDto blogPutDto)
    {
        var temp = _blog.GetById(blogId);
        if (temp == null)
        {
            return NotFound();
        }
        _mapper.Map(blogPutDto, temp);
        var item = _blog.Put(temp);
        var result = _mapper.Map<BlogGetDto>(item);
        return Ok(result);
    }
    /// <summary>
    /// 删除博客
    /// </summary>
    /// <param name="blogId"></param>
    /// <returns></returns>
    [HttpDelete("{blogId}")]
    public ActionResult<IEnumerable<BlogGetDto>> Delete(int blogId)
    {
        var list = _blog.Delete(blogId);
        var result = _mapper.Map<IEnumerable<BlogGetDto>>(list);
        return Ok(result);
    }
}
```