### 路由：通过指定路径访问某一个类型而访问到指定的函数。
```
[ApiController].[Route("/api/..")]
```

### 关于匹配到函数的处理：
 1. 入参：函数的参数，模型绑定
2. 出参：返回的响应，1.状态码 2.带对象的状态码  3.重定向  4.内容


```
Dto:BlogDto.cs

namespace Admin2024.Api;

public class BlogDto{
    public string Title {get;set;}=null!;
    public string Author {get;set;}=null!;
    public string? Flag {get;set;}

}
```


```
BlogsController.cs:
using Microsoft.AspNetCore.Mvc;

namespace Admin2024.Api;

[ApiController]
[Route("/api/[controller]")]                                                                         

public class BlogsController:ControllerBase{
//创建
    public IActionResult Index(){
        return Ok("你好，世界");
    }

    [Route("{id}")]
    public IActionResult Details(int id){
        return Ok(id);
    }
//新增
    [HttpPost]
    public ActionResult<BlogDto> Post(BlogDto blogDto){
        return blogDto;
    }
//编辑
    [HttpPut("{id}")]
    public ActionResult<dynamic> PUT(int id,BlogDto blogDto){
        return new {id,blogDto};
    }
//删除                                                                   
    [HttpDelete]
    public IActionResult Delete(int id){
        return Ok(id);
    }


}
```