## 接口及实现

> 任务：1、理解流程 2、继续去完善

- 数据库（Navicat Premium Lite）

1. 设计控制器

- 如果实体有自己的行为，当自己无法单独满足时，则可以将行为放到领域服务层

2. Admin2024.Application.Contracts(契约，约束数据)

- AppUser
  - 命名规则（AppService 结尾）
  -

3. Admin2024.Application（服务，实现类）

- AppUser
  - 命名（AppService 结尾）

4. Admin2024.EntityFrameworkCore

- Repository

  ```csharp

  ```

1. Domain

- 定义实例模型
- 定义行为待继承的类（System 中都继承它）
