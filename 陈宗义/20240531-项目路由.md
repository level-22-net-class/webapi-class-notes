## 接口

- I 实例名 Repository

  ```csharp
      //通过Id获取所有作者的实现
      实例对象? GetAuthorById(Guid guid);

      //获取所有作者的方法
      //函数三要素（函数名，形参，返回值）
      ICollection<实例对象> GetAllAuthors();

  ```

- 实例名 Repository
  ```csharp
      public class 实例名Repository:I实例名Repository
      {
         public ICollection<实例名> GetAllAuthors()
         {
              //从持久化数据库中获取
              return 名Db.Instance.实例名.ToList();
              //return [.. 名Db.Instance.实例名];
         }
         public 实例名? GetAuthorById(Guid guid)
         {
              return 名Db.Instance.实例名.SingleOrDefault(item=>item.Id==guid);
         }
      }
  ```
- 控制器中
  ```csharp
      private readonly I实例名Repository _实例名Repository;
      public 实例名Controller(I实例名Repository 实例名Repository)
      {
          _实例名Repository=实例名Repository;
      }
      //获取get的函数中
      [HttpGet("{id?}")]
      public IActionResult Get(int id)
      {
          if(id.ToString==""){
              return Ok(_实例名Repository.GetAllAuthors());
          }else{
              return Ok(_实例名Repository.GetAuthorById(id));
          }
      }
  ```
- Startup
  ```csharp
      public void ConfigureServices(IServiceCollection services)
      {
          services.AddControllers();
          //新加
          services.AddScoped<I实例名Repository,实例名Repository>();
      }
  ```
