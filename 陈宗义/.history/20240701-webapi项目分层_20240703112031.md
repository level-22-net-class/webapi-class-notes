### ABP.IO

1.  了解代码实现与设计思想

### 项目架构分层

1. \*/api 对下提供接口服务层
2. \*/

### RBAC 模型 Role-Based Access Control :基于角色的访问控制

#### 通过把权限（读取，写入，修改等）分配给角色（管理员、普通用户等），再将角色分配给用户来实现对系统资源的访问控制

1.  RBAC 认为授权实际上是 Who 、What 、How 三元组之间的关系，也就是 Who 对 What 进行 How 的操作，也就是“主体”对“客体”的操作。
    1. who 是权限的拥有者或主体 user role
    2. what 是操作或对象 operation object
    3. how 是具体的权限 privilege，正向授权或反向授权
