Entity Framework Core 提供了几种方式来进行分页查询。以下是一些常见的分页技术：

### 1. 使用 `Skip` 和 `Take` 方法

这是实现分页查询的最简单方式。`Skip` 方法用于跳过指定数量的记录，而 `Take` 方法用于获取指定数量的记录。

```
public async Task<PageResult> GetPaginatedBlogsAsync(int pageNumber, int pageSize)
{
    var blogs = _context.Blogs
        .OrderBy(b => b.Id) // 确保有一个排序顺序
        .Skip((pageNumber - 1) * pageSize)
        .Take(pageSize);

    var result = new PageResult
    {
        Blogs = await blogs.ToListAsync(),
        TotalCount = await _context.Blogs.CountAsync()
    };

    return result;
}
```

### 2. 使用 `PagedEnumerable` 类

Entity Framework Core 6.0 引入了 `PagedEnumerable` 类，它提供了一种更高效的方式来进行分页查询，因为它可以减少数据库的查询次数。

```
public async Task<PagedResult<Blog>> GetPaginatedBlogsAsync(int pageNumber, int pageSize)
{
    var pagedEnumerable = _context.Blogs
        .OrderBy(b => b.Id)
        .ToPagedListAsync(pageNumber, pageSize);

    var pagedResult = await pagedEnumerable
        .ToListAsync();

    return new PagedResult<Blog>
    {
        Items = pagedResult,
        TotalCount = pagedEnumerable.TotalCount
    };
}
```

注意：使用 `PagedEnumerable` 需要安装 `Microsoft.EntityFrameworkCore.PagedList` NuGet 包。

### 3. 使用 SQL Server 的 `OFFSET` 和 `FETCH NEXT` 子句

如果你使用的是 SQL Server，你可以利用 SQL Server 的 `OFFSET` 和 `FETCH NEXT` 子句来进行分页查询。

```
public async Task<List<Blog>> GetPaginatedBlogsAsync(int pageNumber, int pageSize)
{
    return await _context.Blogs
        .OrderBy(b => b.Id)
        .Offset((pageNumber - 1) * pageSize)
        .FetchNext(pageSize)
        .ToListAsync();
}
```

### 4. 使用 `AsNoTracking` 方法

在进行分页查询时，使用 `AsNoTracking` 方法可以提高性能，因为它告诉 Entity Framework Core 不需要跟踪这些实体的更改。

```
public async Task<List<Blog>> GetPaginatedBlogsAsync(int pageNumber, int pageSize)
{
    return await _context.Blogs
        .AsNoTracking()
        .OrderBy(b => b.Id)
        .Skip((pageNumber - 1) * pageSize)
        .Take(pageSize)
        .ToListAsync();
}
```

### 注意事项

- 在使用 `Skip` 和 `Take` 方法时，确保你的查询有一个稳定的排序顺序，否则分页的结果可能会不一致。
- 分页查询可能会对数据库性能产生影响，特别是当 `Skip` 的数值较大时。在这种情况下，考虑使用其他分页技术或优化你的查询。
- 使用 `PagedEnumerable` 或 SQL Server 的 `OFFSET` 和 `FETCH NEXT` 子句可以提高分页查询的性能。

根据你的具体需求和数据库类型，你可以选择最适合你情况的分页查询方法。