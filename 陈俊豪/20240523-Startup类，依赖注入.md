#### Startup类
IWebHostBuilder接口有多个扩展方法，其中有一个很重要的是UseStartup方法，他主要向应用程序提供用于配置启动的类，而指定的这个类应具有一下两个方法：
```
ConfigureServices:用于向ASP.NET Core的依赖注入容器添加服务
Configure:用于添加中间件，配置请求管理
```
```
public class Startup{
    public void ConfigureServices(IServiceCollection services){
        ...
        services.AddMvc();
        services.AddScoped<INoteRepository,NoteReposei>
        
        }
}
```

#### 依赖注入
```
//startup.cs
public void ConfigureServices(IServiceCollection services)
{
    services.AddAuthentication(AzureADDefaults.BearerAuthenticationScheme)
            .AddAzureADBearer(options => Configuration.Bind("AzureAd", options));
 
    services.AddHttpContextAccessor();
    services.AddTransient<HttpContextAccessor>();
 
    //Register class for DI
    services.UseCustomServices();
 
    services.AddControllers();
    services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1", new OpenApiInfo { Title = "RestfulApi", Version = "v1" });
    });
}
 
//webcommon注入扩展方法
namespace RestfulApi.WebCommon
{
    public static class CustomServicesExtensions
    {
        public static IServiceCollection UseCustomServices(this IServiceCollection services)
        {
            var assembliesToScan = new[] {
                Assembly.GetExecutingAssembly(),
                Assembly.GetAssembly(typeof(MyNumberService))
            };
 
            services.RegisterAssemblyPublicNonGenericClasses(assembliesToScan)
                .Where(c => c.Name.EndsWith("Service"))
                .AsPublicImplementedInterfaces(ServiceLifetime.Scoped);
 
            services.AddScoped<MyNumberService>();
            return services;
        }
    }
}
```

构造函数依赖注入分为三步：

1、先创建一个接口

2、创建一个类，用于实现接口

3、到配置服务类（Startup）的 public void ConfigureServices(IServiceCollection services){  } 方法中注册，注册方式有 addSingleton() addTransient() addScoped() 三种

例如：services.AddSingleton<接口名