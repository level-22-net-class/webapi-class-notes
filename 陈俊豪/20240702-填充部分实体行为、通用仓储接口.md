### 填充部分领域的实体行为
```cs
public class AppUser  : BaseEntity
{
  private List<AppRole> _appRoles=new List<AppRole>();
  ······
  public void SetPassword(string password)
    {
        this.Password = password;
    }
    public void AllocateRole(AppRole appRole)
    {
        _appRoles.Add(appRole);//分配了角色
    }
    public void RemoveRole(AppRole appRole)
    {
        _appRoles.Remove(appRole);
    }
    //移除
    public void RemoveRole(string roleName)
    {
        var role=_appRoles.FirstOrDefault(x => x.RoleName==roleName);
        if (role!=null)
        {
            _appRoles.Remove(role);
        }
    }
    //权限
    public bool HasPermssion()
    {
        return true;
    }
}
```

```cs
public class AppRole : BaseEntity
{
    private List<AppPermission> _appPermission=new List<AppPermission>();
    public string RoleName { get; set;}=null!;
    public string? Description{ get; set;}

    //一般用于ORM工具初始化对象
    public AppRole()
    {

    }
    public AppRole(string roleName,string description)
    {
        RoleName=roleName;
        Description=description;
    }
    //角色

      //分配权限
      public void AllocatePermission(AppPermission appPermission)
      {
        _appPermission.Add(appPermission);
      }
      //移除权限
      public void RemovePermission(AppPermission appPermission)
      {
        _appPermission.Remove(appPermission) ;
      }
      //判断有无权限
      public bool HasPermssion(AppResource appResource,AppOperation appOperation)
      {
        return _appPermission.Any(x => x.AppResourceId == appResource.Id
         && x.AppOperationId == appOperation.Id);
      }
      //判断有没有权限
      public bool HasPermssion(Guid resourceId,Guid operationId)
      {
        return _appPermission.Any(x => x.AppResourceId == resourceId
         && x.AppOperationId == operationId);
      }
}
```
```cs
namespace Admin2024.Domain.Entity.System;

//权限
public class AppRolePermission : BaseEntity
{
    public Guid AppRoleId { get; set;}
    public Guid  AppPermissionId { get; set;}
}
```


### 完成通用仓储接口

```cs
using Admin2024.Domain.Entity;
namespace Admin2024.Domain.Interface;

public interface IRepository<T> where T : BaseEntity
{
    //根据id获取实体
   Task<T?> GetByIdAsync(Guid id);
   //获取实体列表
   Task<List<T>> GetListAsync();
   Task<T> CreateAsync(T entity);
   Task<T> UpdateAsync(Guid id, T entity);
   Task<T?> DeleteAsync(Guid id);
}
```