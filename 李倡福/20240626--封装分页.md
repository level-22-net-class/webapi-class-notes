### 分页帮助类

首先，我们创建一个通用的分页帮助类，用于处理分页逻辑。

```csharp
public class PagedResult<T>
{
    public int Page { get; set; }
    public int PageSize { get; set; }
    public int TotalCount { get; set; }
    public int TotalPages { get; set; }
    public List<T> Results { get; set; }

    public PagedResult(List<T> items, int totalCount, int page, int pageSize)
    {
        Results = items;
        TotalCount = totalCount;
        Page = page;
        PageSize = pageSize;
        TotalPages = (int)Math.Ceiling(totalCount / (double)pageSize);
    }
}
```

### 使用分页帮助类的Web API 示例

接下来，我们在Web API 控制器中使用这个分页帮助类来处理分页查询。

```csharp
[HttpGet]
public IHttpActionResult GetProducts(int page = 1, int pageSize = 10)
{
    var totalCount = db.Products.Count();
    var products = db.Products.OrderBy(p => p.Id)
                             .Skip((page - 1) * pageSize)
                             .Take(pageSize)
                             .ToList();

    var pagedResult = new PagedResult<Product>(products, totalCount, page, pageSize);

    return Ok(pagedResult);
}
```


