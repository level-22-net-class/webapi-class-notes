# 雪花算法 snowflake
雪花算法使用64位long类型的数据存储id
```md
0 - 0000000000 0000000000 0000000000 0000000000 0 - 0000000000 - 000000000000

符号位                  时间戳                     机器码             序列号
```
1. 最高位表示==符号位==，其中0代表整数，1代表负数，而id一般都是正数，所以最高位为0。
2. ==41位存储毫秒级时间戳==，这个时间截是存储时间截的差值（当前时间截 - 开始时间截) * 得到的值），这里的的开始时间截，一般是我们的ID生成器开始使用的时间，一般为项目创建时间
3. 10位存储机器码，最多支持1024台机器，当并发量非常高，同时有多个请求在同一毫秒到达，可以根据机器码进行第二次生成。
4. 12位存储序列号，当同一毫秒有多个请求访问到了同一台机器后，此时序列号就派上了用场，为这些请求进行第三次创建，最多每毫秒每台机器产生2的12次方也就是4096个id

# 存储方式
1. U表示该常数用无符号整型方式存储，相当于 unsigned int 
   1. 数值后面加“U”和“u”的意义是该数值是unsigned型。
2. 数值后面加“L”和“l”（小写的l）的意义是该数值是long型。   【5L的数据类型为long int。】
    1. L表示该常数用长整型方式存储，相当于 long 
3. F表示该常数用浮点方式存储，相当于 float三、自动类型转换


这些后缀跟是在字面量（literal，代码中的数值、字符、字符串）后面
```md
```
# EFCore中使用雪花算法生成ID
**SnowflakeIdGenerator:**
```cs
namespace Admin.Infrastructure;
public class SnowflakeIdGenerator
{
  // long：64位有符号整数  <==>int64 

  //  twepoch: 时间戳基准值，即起始时间戳，这里设置为2021年1月1日零点的时间戳。
  private static readonly long twepoch = 1609459200000L; // 时间戳基准值，2021-01-01 00:00:00

  // workerIdBits: 机器ID所占位数，这里设置为5位。
  private static readonly long workerIdBits = 5L;

  // datacenterIdBits: 数据中心ID所占位数，这里也设置为5位。
  private static readonly long datacenterIdBits = 5L;

  // sequenceBits: 序列号所占位数，设置为12位。
  private static readonly long sequenceBits = 12L;

  // maxWorkerId、maxDatacenterId、sequenceMask分别为计算出的最大机器ID、最大数据中心ID和序列号的掩码。
  private static readonly long maxWorkerId = -1L ^ (-1L << (int)workerIdBits);
  private static readonly long maxDatacenterId = -1L ^ (-1L << (int)datacenterIdBits);
  private static readonly long sequenceMask = -1L ^ (-1L << (int)sequenceBits);
  // workerId、datacenterId、sequence用于存储机器ID、数据中心ID和序列号。

  private static long workerId;
  private static long datacenterId;
  private static long sequence = 0L;
  // lastTimestamp用于存储上一个ID生成的时间戳。
  private static long lastTimestamp = -1L;

  // 接收机器ID和数据中心ID作为参数
  public SnowflakeIdGenerator(long workerId, long datacenterId)
  {
    if (workerId > maxWorkerId || workerId < 0)
    {
      throw new ArgumentException($"worker Id cannot be greater than {maxWorkerId} or less than 0");
    }
    if (datacenterId > maxDatacenterId || datacenterId < 0)
    {
      throw new ArgumentException($"datacenter Id cannot be greater than {maxDatacenterId} or less than 0");
    }

    SnowflakeIdGenerator.workerId = workerId;
    SnowflakeIdGenerator.datacenterId = datacenterId;
  }
  /* 
  方法NextId()：生成下一个唯一ID。在该方法内部，首先获取当前时间戳，然后进行一系列的判断和操作：

如果当前时间戳小于上一个时间戳（出现了时间回退），则抛出异常。
如果当前时间戳和上一个时间戳相同，则递增序列号，如果序列号达到上限，则等到下一个毫秒。
如果时间戳发生变化，重置序列号为0。
更新最后生成ID的时间戳。
返回计算后的唯一ID。
   */
  public long NextId()
  {
    lock (this)
    {
      // GenerateTimestamp()：生成当前时间戳（以毫秒为单位）
      long timestamp = GenerateTimestamp();

      if (timestamp < lastTimestamp)
      {
        throw new Exception($"Clock moved backwards for {lastTimestamp - timestamp} milliseconds");
      }

      if (lastTimestamp == timestamp)
      {
        sequence = (sequence + 1) & sequenceMask;
        if (sequence == 0)
        {
          // NextMillis(long lastTimestamp)：生成下一个时间戳，确保不小于上一个时间戳。
          // 如果生成的时间戳小于等于上一个时间戳，则循环生成直到获得大于上一个时间戳的时间戳。
          timestamp = NextMillis(lastTimestamp);
        }
      }
      else
      {
        sequence = 0;
      }

      lastTimestamp = timestamp;

      return ((timestamp - twepoch) << 22) | (datacenterId << 17) | (workerId << 12) | sequence;
    }
  }

  private long GenerateTimestamp()
  {
    return (long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds;
  }

  private long NextMillis(long lastTimestamp)
  {
    long timestamp = GenerateTimestamp();
    while (timestamp <= lastTimestamp)
    {
      timestamp = GenerateTimestamp();
    }
    return timestamp;
  }
}
```
**Startup**：在依赖注入容器中注入SnowflakeIdGenerator，以便在程序集中使用该算法生成id
```cs
long workerId = 1; // 定义机器ID
long datacenterId = 1; // 定义数据中心ID
services.AddSingleton(new SnowflakeIdGenerator(workerId, datacenterId));

// 生成的ID如下
// 311928119286042624
// 311928119286042625
```
**使用算法：**
```cs
// 使用构造函数注入类，NextId() 生成唯一id
private readonly SnowflakeIdGenerator _sfg;

public FnName(SnowflakeIdGenerator snowflakeIdGenerator) 
{
  _sfg = snowflakeIdGenerator;
}

Id = _sfg.NextId()
```