# 创建
1. 创建项目：`dotnet new webapi -n 项目名`
2. 创建解决方案`dotnet new sln -n 解决方案名`
3. 将项目添加到解决方案中：`dotnet sln add 项目路径`
4. 项目生成：`dotnet build`
5. 项目生成并运行：`dotnet run`

添加依赖包：
1. `dotnet add package Microsoft.EntityFrameworkCore`
2. `dotnet add package Microsoft.EntityFrameworkCore.SqlServer`
3. `dotnet add package Microsoft.EntityFrameworkCore.Design`
4. `dotnet add package Microsoft.EntityFrameworkCore.Tools`
5. 查看依赖项列表：`dotnet list package`

安装并使用ef工具：
1. 安装：`dotnet tool install --global dotnet-ef`
2. 添加迁移文件：`dotnet ef migrations add 文件名`
3. 将迁移映射到数据库：`dotnet ef database update`

# 基础代码
项目
***Program.cs***：
```cs
namespace DemoName;
public class Program
{
    public static void Main(string[] args)
    {
        // Build() ：创建IHost
        // Run()：启动运行IHost
        CreateHostBuilder(args).Build().Run();
    }
    //  IHostBuilder 对象
    public static IHostBuilder CreateHostBuilder(string[] args)
    {
        // 加载配置
        return Host.CreateDefaultBuilder(args)
        // 将服务器设置为web服务器并配置。 添加主机筛选中间件。
        .ConfigureWebHostDefaults(build=>
        {
            build.UseStartup<Startup>();
        });
    }
}
```

***Startup.cs***：
```cs
namespace DemoName;
public class Startup
{
    // 指定应用响应 HTTP 请求的方式
    public void Configure(IApplicationBUilder app)
    {
        app.UseRouting();
        app.UseEndpoints(ep=>
        {
            ep.MapControllers();
        });
    }
    // 将服务注入到IServiceCollection服务容器中
    public void ConfigureServices(IServiceCollection service)
    {
        service.AddControllers();
    }
}
```