# Application.Contract
定义用于增删改查的实体类，以及业务接口
文件结构：
```cs
Application.Contract{
  *{
    *Dto.cs,
    Create*Dto.cs,
    Update*Dto.cs,
    I*AppService.cs    定义业务接口
  }
}
```
# Application

文件结构：
```cs
Application{
  *{
    *AppService.cs    实现Application.Contract中的IAppUserService.cs接口
  }
}
```