# 过滤 搜索 排序
**AuthorResourceParameters**:在author的路由参数定义类中，添加如下字段
```cs
  // 添加过滤，定义属性Age
  // 过滤：通过给定值对资源的一个或多个属性进行限制
  public int Age { get; set; }
  // 添加搜索功能，搜索全列表信息
  // 搜索：根据指定关键字对所有属性值进行匹配
  public string? SearchQuery { get; set; }
  // 添加排序
  // 当获取资源时，如果没有显示指定排序字段，默认以Name进行排序
  public string SortBy { get; set; } = "Name";
```
**AuthorRepository**:实现仓储接口的类中修改以下方法
```cs
  public async Task<PagedList<Author>> GetByPagedQueryAsync(AuthorResourceParameters parameters)
  {
    IQueryable<Author> queryableAuthors = _dbContext.Set<Author>();
    // 过滤，没有就使用默认数据
    // {{url}}/author?age=99
    if (parameters.Age != 0)
    {
      queryableAuthors = queryableAuthors.Where(x => x.Age == parameters.Age);
    }
    // 搜索 {{url}}/author?searchquery=9
    if (!string.IsNullOrEmpty(parameters.SearchQuery))
    {
      queryableAuthors = queryableAuthors.Where(x => x.Name.Contains(parameters.SearchQuery));
    }
    // 排序
    if (parameters.SortBy == "Name")
    {
      queryableAuthors = queryableAuthors.OrderBy(x => x.Name);
    }
    return await PagedList<Author>.CreateAsync(queryableAuthors, parameters.PageSize, parameters.PageIndex);
  }
```