#### 过滤器
##### Filter的开发和调用
```

在默认的WebApi中，框架提供了三种Filter，他们的功能和运行条件如下表所示：

Filter 类型	实现的接口	描述
Authorization	IAuthorizationFilter	最先运行的Filter，被用作请求权限校验
Action	IActionFilter	在Action运行的前、后运行
Exception	IExceptionFilter	当异常发生的时候运行
```
```
建Filter ApiResultFilter.cs:

namespace Admin2024.Api;

public class ApiResultFilter:IAsyncResultFilter{
    public Task OnResultExecutionAsync(ResultExecutingContent content,ResultExectionDeletegate next){
         //判断返回结果是否是内容，如果是，我们就给content.Result赋一个新的实例对象
         if(content.Result is objectResult){
            var objectResult=(objectResult)content.Result;
            content.Result=new objectResult(new ApiResult{
                Code=1000,
                Msg="请求成功",
                Data=objectResult.Value
            })
         }else{
            content.Result=new objectResult(new ApiResult{
                Code=1001
            })
            await next();
         }
    }
}
```

```
Dto ApiResult.cs

namespace Admin2024.Api;

public class ApiResult{
    public int Code{get;set;}
    public string? Msg{get;set;}
    public object? Data{get;set;}
    
}
```