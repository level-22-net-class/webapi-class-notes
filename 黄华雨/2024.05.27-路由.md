```
路由：1.在控制器上写上两个特性 [ApiController].[Route("/api/..")]

2.如果不符合restfull风格的路由的话，在action上单独写路由
[HttpGet("/api/...")]
```

```
关于匹配到函数的处理：
入参：函数的参数，模型绑定
出参：返回的响应，1.状态码 2.带对象的状态码  3.重定向  4.内容：{Code:1000,data:[],msg:"请求成功"}


```


```
Dto:BlogDto.cs

namespace Admin2024.Api;

public class BlogDto{
    public string Title {get;set;}=null!;
    public string Author {get;set;}=null!;
    public string? Flag {get;set;}

}
```


```
BlogsController.cs:
using Microsoft.AspNetCore.Mvc;

namespace Admin2024.Api;

[ApiController]
[Route("/api/[controller]")]                                                                         

public class BlogsController:ControllerBase{
//创建
    public IActionResult Index(){
        return Ok("哈哈哈哈");
    }

    [Route("{id}")]
    public IActionResult Details(int id){
        return Ok(id);
    }
//新增
    [HttpPost]
    public ActionResult<BlogDto> Post(BlogDto blogDto){
        return blogDto;
    }
//编辑
    [HttpPut("{id}")]
    public ActionResult<dynamic> PUT(int id,BlogDto blogDto){
        return new {id,blogDto};
    }
//删除                                                                   
    [HttpDelete]
    public IActionResult Delete(int id){
        return Ok(id);
    }


}
```


```
http:
@url=http://localhost:5000

###
GET {{url}}/api/books
Accept: application/json 

### 
GET {{url}}/api/blogs/1 HTTP/1.1


###
POST {{url}}/api/blogs HTTP/1.1
Content-Type: application/json

{
    "title":"晚上又和",
    "author":"驾驶证",
    "flag":"绝对是"
}

###
PUT  {{url}}/api/blogs/11 HTTP/1.1
Content-Type: application/json

{
    "title":"jjjjj",
    "author":"驾驶证",
    "flag":"dsz"
}

###
DELETE {{url}}/api/blogs/12 HTTP/1.1

###
GET {{url}}/api/books/77/blogs/88 HTTP/1.1
```