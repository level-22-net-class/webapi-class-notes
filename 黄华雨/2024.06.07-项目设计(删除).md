### 项目设计（删除）
```
Controllers-BooksController.cs

....
 [HttpDelete("{bookId}")]
    public IActionResult Delete(Guid authorId, Guid bookId)
    {
        var result = _bookRepository.Delete(authorId, bookId);
        return Ok(result);
    }

```

```
Serivices-BookRepository.cs

....
 public BookDto? Delete(Guid authorId, Guid bookId)
    {
        /*
            1. 确认要操作的作者是否存在
                存在则继续
                不存在则返回null
            2. 在内存数据库查找对应的图书id
                存在则删除对应图书
                不存在则返回null
        */
        var author = BookStoreDb.Instance.Authors.FirstOrDefault(item => item.Id == authorId);
        if (author == null)
        {
            return null;
        }
        var book = BookStoreDb.Instance.Books.FirstOrDefault(item => item.Id == bookId);
        if (book == null)
        {
            return null;
        }
        BookStoreDb.Instance.Books.Remove(book);
        var result = new BookDto { Id = bookId, AuthorId = authorId, BookName = book.BookName };
        return result;
    }
    ..
```

```
测试：http：

### 删除指定作者下指定的图书
DELETE {{url}}/api/authors/102a86f0-ae9b-439f-9bc4-bb8ed8c2af6d/books/a53434ed-4da3-4dd3-b31d-51920e31f56e HTTP/1.1
```

### 第五章 ：使用Entity Framework Core

```
Interfaces-IRepositoryBase.cs

namespace BookStore.Api.Interface;

/// <summary>
/// 基础仓储接口，定义通用的CRUD功能
/// </summary>
public interface IRepositoryBase<T>
{
    /*
        通过id获取指定模型（对应数据库中的表，模型中的公共属性对应数据表的字段或者叫列）
        获取所有的模型

        获取所有实体

        新增插入

        删除


        更新

    */

    T GetById(Guid id);

    List<T> GetAll();

    T Insert(T entity);
    T Update(T entity);
    T Delete(T entity);
    T Delete(Guid id);
}
```

```
appsetting.json:

{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft.AspNetCore": "Warning"
    }
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "Mssql":"server=.;database=BookStore;uid=sa;pwd=123456;TrustServerCertificate=true;"
  }
}                           \
```

```
Db-BookStoreContext.cs

using BookStore.Api.Domain;
using Microsoft.EntityFrameworkCore;

namespace BookStore.Api.Db;

public class BookStoreDbContext : DbContext
{
    public BookStoreDbContext(DbContextOptions<BookStoreDbContext> options) : base(options)
    {

    }


    public DbSet<Authors> Authors { get; set; }
    public DbSet<Books> Books { get; set; }
    public DbSet<Users> Users { get; set; }

}
```