```
建：Dto-AuthorDto.cs:

namespace BookStore.Api.Dto;

public class AuthorDto
{
    public Guid Id { get; set; }

    public string AuthorName { get; set; } = null!;
    public int Gender { get; set; }

    // name  一个字符串 如果为null，表示不指向任何一个值
    // name 其值为""，就是空字符串,其实是有指向中一个值，这个值是空字符串而已

}
```

```
Services-AuthorRepository.cs

using BookStore.Api.Domain;
using BookStore.Api.Db;
using BookStore.Api.Dto;
using BookStore.Api.Interface;

namespace BookStore.Api.Services;

public class AuthorRepository : IAuthorRepository
{
  ......

    public ICollection<AuthorDto> GetAllAuthors()
    {
        // 这个实现应该从持久化的数据源中获得：数据库、文件、各种异构数据、从各种api中获取的数据
        var list = BookStoreDb.Instance.Authors.ToList();
        var resultList = new List<AuthorDto>();// let resultList=[]
        list.ForEach(item =>
        {
            // 实例化一个对象这里有2种方式，一个是直接调用构造函数,形如：new AuthorDto()
            // 另一个是直接填充其属性,形如：new AuthorDto{}
            var tmp = new AuthorDto { Id = item.Id, AuthorName = item.AuthorName, Gender = item.Gender };
            resultList.Add(tmp);
        });
        return resultList;
    }

    public AuthorDto? GetAuthorById(Guid id)
    {
        var tmp = BookStoreDb.Instance.Authors.SingleOrDefault(item => item.Id == id);
        var tmpResult = tmp != null ? new AuthorDto { Id = tmp.Id, AuthorName = tmp.AuthorName, Gender = tmp.Gender } : null;
        // dynamic xResult=null;
        // if(tmp!=null){
        //     xResult=new AuthorDto { Id = tmp.Id, AuthorName = tmp.AuthorName, Gender = tmp.Gender };
        // }
        return tmpResult;
    }
.....
}
```

```
Controllers-AuthorController.cs
....
 [HttpPost]
    public IActionResult Post(AuthorCreateDto authorCreateDto)
    {
        /*
            1.拿到AuthorCreateDto类型的实例化数据-模型绑定
            2.将相关数据保存到数据库
                1.转换AuthorCreateDto类型的数据为Authors
                2.调用数据库上下文，将数据插入

        */
        var result = _authorRepository.Insert(authorCreateDto);
        return Ok(result);
    }

    [HttpPut("{id}")]
    public IActionResult Put(Guid id, AuthorUpdateDto authorUpdateDto)
    {
        var result = _authorRepository.Update(id, authorUpdateDto);
        return Ok(result);
    }

    ....

```