```plantuml
@startuml
    participant "开始" as Start  
    participant "安装EFCore核心包" as InstallEFCore  
    participant "安装数据库驱动包" as InstallDbDriver  
    participant "定义实体类型" as DefineEntities  
    participant "定义数据库上下文" as DefineDbContext  
    participant "生成迁移文件" as GenerateMigration  
    participant "更新数据库" as UpdateDatabase  
    participant "结束" as End  
  
    Start -> InstallEFCore: 第一步  
    InstallEFCore -> InstallEFCore: 使用 NuGet 安装 \nMicrosoft.EntityFrameworkCore  
    InstallEFCore -> InstallDbDriver: 第二步  
    InstallDbDriver -> InstallDbDriver: 选择并安装数据库驱动包  
    note over InstallDbDriver: 例如: Microsoft.EntityFrameworkCore.SqlServer  
    InstallDbDriver -> DefineEntities: 第三步  
    DefineEntities -> DefineEntities : 定义实体类型
    DefineEntities -> DefineDbContext: 第四步  
    DefineDbContext -> DefineDbContext: 定义: \nDbContext 和 DbSet<Users>  
    DefineDbContext -> GenerateMigration: 第五步  
    GenerateMigration -> GenerateMigration: 使用工具包 \nMicrosoft.EntityFrameworkCore.Design 生成迁移  
    GenerateMigration -> UpdateDatabase: 第六步  
    UpdateDatabase -> End: 结束  
@enduml
```

![alt text](./img/image.png)